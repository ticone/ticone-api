/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 19, 2018
 *
 */
public class CreatePrototypeInstanceFactoryException extends CreateInstanceFactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1636916184111531559L;

	public CreatePrototypeInstanceFactoryException() {
		super();
	}

	public CreatePrototypeInstanceFactoryException(final String message) {
		super(message);
	}

	public CreatePrototypeInstanceFactoryException(final Throwable cause) {
		super(cause);
	}

}
