/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import dk.sdu.imada.ticone.util.FactoryException;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 1, 2018
 *
 */
public class PrototypeFactoryException extends FactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4294652153308281723L;

	public PrototypeFactoryException() {
		super();
	}

	public PrototypeFactoryException(final String message) {
		super(message);
	}

	public PrototypeFactoryException(final Throwable cause) {
		super(cause);
	}

}