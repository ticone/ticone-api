/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 31, 2018
 *
 */
public class IncompatiblePrototypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2702584664558474762L;

	public IncompatiblePrototypeException() {
		super();
	}

	public IncompatiblePrototypeException(final Throwable cause) {
		super(cause);
	}

}
