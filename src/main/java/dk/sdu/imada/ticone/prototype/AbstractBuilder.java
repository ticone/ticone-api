/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import java.io.Serializable;
import java.util.Objects;

import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IBuilder;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 6, 2018
 *
 */
public abstract class AbstractBuilder<T, FE extends FactoryException, IE extends CreateInstanceFactoryException>
		implements IBuilder<T, FE, IE>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1686557378482415467L;

	/**
	 * 
	 * {@inheritDoc}
	 * <p>
	 * Delegates the construction to {@link #doBuild()} and resets fields that are
	 * instance specific by calling {@link #reset()}.
	 * </p>
	 */
	@Override
	public final T build() throws FE, IE, InterruptedException {
		final T result = doBuild();
		reset();
		return result;
	}

	/**
	 * Actually constructs the new instance.
	 * 
	 * @return
	 * @throws FE
	 * @throws IE
	 * @throws InterruptedException
	 */
	protected abstract T doBuild() throws FE, IE, InterruptedException;

	/**
	 * Clears all fields of the builder that are specific to the previously created
	 * instance and will not be needed for creation of further instances.
	 * 
	 * Clearing them allows to prevent memory leakage.
	 */
	protected abstract void reset();

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return Objects.equals(this.getClass(), obj.getClass());
	}
}
