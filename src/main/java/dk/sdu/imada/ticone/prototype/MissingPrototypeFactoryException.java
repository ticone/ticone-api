/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 6, 2018
 *
 */
public class MissingPrototypeFactoryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1791693551535807497L;

	public MissingPrototypeFactoryException() {
		super();
	}

	public MissingPrototypeFactoryException(final String message) {
		super(message);
	}

	public MissingPrototypeFactoryException(final Throwable cause) {
		super(cause);
	}

}
