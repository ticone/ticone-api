/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import dk.sdu.imada.ticone.network.INetworkLocationPrototypeComponentBuilder;
import dk.sdu.imada.ticone.network.INetworkLocationPrototypeComponentBuilder.INetworkLocationPrototypeComponent;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;

public class NetworkLocationPrototypeComponentType extends PrototypeComponentType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2349497634532524433L;

	/**
	 * 
	 */
	NetworkLocationPrototypeComponentType() {
		super();
	}

	@Override
	public String toString() {
		return "NETWORK_LOCATION";
	}

	@Override
	public Class<INetworkLocationPrototypeComponentBuilder> getFactoryType() {
		return INetworkLocationPrototypeComponentBuilder.class;
	}

	@Override
	public Class<ITiconeNetworkNode> getBasicType() {
		return ITiconeNetworkNode.class;
	}

	@Override
	public Class<INetworkLocationPrototypeComponent> getComponentType() {
		return INetworkLocationPrototypeComponent.class;
	}

	@Override
	public INetworkLocationPrototypeComponent getComponent(final IPrototype prototype)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		if (prototype == null)
			throw new MissingPrototypeException();
		return (INetworkLocationPrototypeComponent) prototype.getPrototypeComponent(this);
	}

	@Override
	public INetworkLocationPrototypeComponentBuilder getComponentFactory(final IPrototypeBuilder prototypeFactory)
			throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException {
		if (prototypeFactory == null)
			throw new MissingPrototypeFactoryException();
		return (INetworkLocationPrototypeComponentBuilder) prototypeFactory.getPrototypeComponentFactory(this);
	}
}