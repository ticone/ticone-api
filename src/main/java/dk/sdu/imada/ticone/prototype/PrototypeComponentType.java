/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import java.io.Serializable;

import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder.IPrototypeComponent;

public abstract class PrototypeComponentType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6774766105971202976L;

	public static final TimeSeriesPrototypeComponentType TIME_SERIES = new TimeSeriesPrototypeComponentType();

	public static final NetworkLocationPrototypeComponentType NETWORK_LOCATION = new NetworkLocationPrototypeComponentType();

	/**
	 * 
	 */
	PrototypeComponentType() {
	}

	@Override
	public boolean equals(final Object obj) {
		return this.getClass().equals(obj.getClass());
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	/**
	 * @return the factory
	 */
	public abstract Class<?> getFactoryType();

	/**
	 * @return the component
	 */
	public abstract Class<?> getComponentType();

	/**
	 * @return the basicType
	 */
	public abstract Class<?> getBasicType();

	public abstract IPrototypeComponentBuilder<?, ?> getComponentFactory(final IPrototypeBuilder prototypeFactory)
			throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException;

	public abstract IPrototypeComponent<?> getComponent(final IPrototype prototype)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException;
}