/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import dk.sdu.imada.ticone.util.FactoryException;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 1, 2018
 *
 */
public class PrototypeComponentFactoryException extends FactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4248831465580568115L;

	public PrototypeComponentFactoryException() {
		super();
	}

	public PrototypeComponentFactoryException(final String message) {
		super(message);
	}

	public PrototypeComponentFactoryException(final Throwable cause) {
		super(cause);
	}

}
