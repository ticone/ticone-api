/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import java.io.Serializable;

import dk.sdu.imada.ticone.clustering.aggregate.IAggregateCluster;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder.IPrototypeComponent;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.IBuilder;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 1, 2018
 *
 * @param <C> The type of {@link IPrototypeComponent} this factory creates.
 * @param <V> The wrapped type of the created {@link IPrototypeComponent}.
 */
public interface IPrototypeComponentBuilder<C extends IPrototypeComponent<V>, V>
		extends IBuilder<C, PrototypeComponentFactoryException, CreateInstanceFactoryException>, Serializable {

	IAggregateCluster<V> getAggregationFunction();

	IPrototypeComponentBuilder<C, V> setAggregationFunction(IAggregateCluster<V> aggregationFunction);

	@Override
	IPrototypeComponentBuilder<C, V> copy();

	PrototypeComponentType getType();

	ITimeSeriesObjects getObjects();

	IPrototypeComponentBuilder<C, V> setObjects(ITimeSeriesObjects objects);

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Oct 31, 2018
	 * @param <V> The wrapped type of this component.
	 */
	public interface IPrototypeComponent<V> extends Serializable {
		IPrototypeComponent<V> copy();

		PrototypeComponentType getType();
	}

}
