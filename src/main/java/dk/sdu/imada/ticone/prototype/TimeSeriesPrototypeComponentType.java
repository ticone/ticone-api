/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder.ITimeSeriesPrototypeComponent;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;

public class TimeSeriesPrototypeComponentType extends PrototypeComponentType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2590199279247684849L;

	/**
	 * 
	 */
	TimeSeriesPrototypeComponentType() {
		super();
	}

	@Override
	public String toString() {
		return "TIME_SERIES";
	}

	@Override
	public Class<ITimeSeriesPrototypeComponentBuilder> getFactoryType() {
		return ITimeSeriesPrototypeComponentBuilder.class;
	}

	@Override
	public Class<ITimeSeries> getBasicType() {
		return ITimeSeries.class;
	}

	@Override
	public Class<ITimeSeriesPrototypeComponent> getComponentType() {
		return ITimeSeriesPrototypeComponent.class;
	}

	@Override
	public ITimeSeriesPrototypeComponent getComponent(final IPrototype prototype)
			throws IncompatiblePrototypeComponentException, MissingPrototypeException {
		if (prototype == null)
			throw new MissingPrototypeException();
		return (ITimeSeriesPrototypeComponent) prototype.getPrototypeComponent(this);
	}

	@Override
	public ITimeSeriesPrototypeComponentBuilder getComponentFactory(final IPrototypeBuilder prototypeFactory)
			throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException {
		if (prototypeFactory == null)
			throw new MissingPrototypeFactoryException();
		return (ITimeSeriesPrototypeComponentBuilder) prototypeFactory.getPrototypeComponentFactory(this);
	}
}