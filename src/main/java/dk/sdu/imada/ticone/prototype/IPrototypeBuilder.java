/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.permute.IPermutable;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder.IPrototypeComponent;
import dk.sdu.imada.ticone.util.IBuilder;

/**
 * 1) If {@link #setComponents(IPrototypeComponent, IPrototypeComponent...)} has
 * been invoked, these components are used.
 * 
 * 2) Otherwise, if
 * {@link #setPrototypeComponentBuilders(IPrototypeComponentBuilder...)} has
 * been invoked, components are created using these. If
 * {@link #setOverrideComponents(Map)} has been invoked, these components are
 * not newly created. If {@link #setObjects(ITimeSeriesObjects)} have been
 * invoked, these objects are applied to the component factory prior to
 * component creation.
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 31, 2018
 *
 */
public interface IPrototypeBuilder
		extends IBuilder<IPrototype, PrototypeFactoryException, CreatePrototypeInstanceFactoryException>, Serializable {
	@Override
	IPrototypeBuilder copy();

	IPrototypeBuilder setPrototypeComponentBuilders(
			final IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?>... componentFactories);

	Map<PrototypeComponentType, IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?>> getPrototypeComponentFactories();

	/**
	 * This method does not return the concrete type of builder. If this is needed
	 * use {@link PrototypeComponentType#getComponentFactory(IPrototypeBuilder)} of
	 * the builder subtype.
	 * 
	 * @param type
	 * @return
	 * @throws IncompatiblePrototypeComponentException
	 * @throws MissingPrototypeFactoryException
	 */
	IPrototypeComponentBuilder<? extends IPrototypeComponent<?>, ?> getPrototypeComponentFactory(
			final PrototypeComponentType type)
			throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException;

	IPrototypeBuilder setObjects(final ITimeSeriesObjects objects);

	ITimeSeriesObjects getObjects();

	Map<PrototypeComponentType, IPrototypeComponent<?>> getComponents();

	IPrototypeBuilder setComponents(final IPrototypeComponent<?> component, final IPrototypeComponent<?>... components);

	Map<PrototypeComponentType, IPrototypeComponent<?>> getOverrideComponents();

	/**
	 * These components are used only in combination with
	 * {@link #setObjects(ITimeSeriesObjects)}.
	 * 
	 * @param overrideComponents
	 * @return
	 */
	IPrototypeBuilder setOverrideComponents(Map<PrototypeComponentType, IPrototypeComponent<?>> overrideComponents);

	/**
	 * Creates a deep copy of the passed prototype, i.e. also creating copies of its
	 * prototype components, except for those passed in as overriding components.
	 * 
	 * @param template           The prototype instance to be copied.
	 * @param overrideComponents Components specified here are used instead of
	 *                           creating new ones.
	 * @return
	 * @throws PrototypeFactoryException
	 */
	IPrototype copy(final IPrototype template, final IPrototypeComponent<?>... overrideComponents)
			throws PrototypeFactoryException;

	boolean producesComparablePrototypesWith(final IPrototypeBuilder prototypeFactory);

	boolean producesComparablePrototypesTo(final IPrototype prototype);

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Oct 31, 2018
	 *
	 */
	public interface IPrototype extends IPermutable, Serializable {
		boolean hasPrototypeComponent(final PrototypeComponentType componentType);

		IPrototypeComponent<?> getPrototypeComponent(final PrototypeComponentType componentType)
				throws IncompatiblePrototypeComponentException, MissingPrototypeException;

		Collection<PrototypeComponentType> getPrototypeComponentTypes();

		Collection<? extends IPrototypeComponent<?>> getPrototypeComponents();

		IPrototypeComponent<?> addPrototypeComponent(final IPrototypeComponent<?> component);

		IPrototype copy();
	}

}
