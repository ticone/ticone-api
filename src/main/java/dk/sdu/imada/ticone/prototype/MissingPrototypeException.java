/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 6, 2018
 *
 */
public class MissingPrototypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6037783485988416558L;

	public MissingPrototypeException() {
		super();
	}

	public MissingPrototypeException(final String message) {
		super(message);
	}

	public MissingPrototypeException(final Throwable cause) {
		super(cause);
	}

}
