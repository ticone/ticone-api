/**
 * 
 */
package dk.sdu.imada.ticone.prototype;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 31, 2018
 *
 */
public class IncompatiblePrototypeComponentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8220383289135071068L;

	public IncompatiblePrototypeComponentException() {
		super();
	}

}
