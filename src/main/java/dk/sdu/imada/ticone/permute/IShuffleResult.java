/**
 * 
 */
package dk.sdu.imada.ticone.permute;

import java.io.Serializable;
import java.util.Map;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * @author Christian Wiwie
 * 
 * @since May 7, 2017
 *
 */
public interface IShuffleResult extends Serializable {

	IObjectWithFeatures getOriginal();

	IObjectWithFeatures getShuffled();

	boolean hasShuffledSupport();

	Map<IObjectWithFeatures, IObjectWithFeatures> getShuffledSupport();
}
