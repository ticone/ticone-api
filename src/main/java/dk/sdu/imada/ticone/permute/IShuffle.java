/**
 * 
 */
package dk.sdu.imada.ticone.permute;

import java.io.Serializable;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 24, 2017
 */
public interface IShuffle extends Serializable {

	ObjectType<?> supportedObjectType();

	String getName();

	boolean producesShuffleMappingFor(ObjectType<?> type);

	IShuffleResult shuffle(final IObjectWithFeatures object, final long seed)
			throws ShuffleException, ShuffleNotInitializedException, IncompatibleShuffleException, InterruptedException,
			IncompatibleMappingAndObjectTypeException;

	default boolean canShuffle(final IObjectWithFeatures object) {
		return supportedObjectType().equals(object.getObjectType());
	}

	default void cleanUpShuffle(final IShuffleResult shuffleResult) {

	}

	IShuffle copy();

	IShuffle newInstance();

	boolean validateParameters() throws ShuffleParameterException;

}
