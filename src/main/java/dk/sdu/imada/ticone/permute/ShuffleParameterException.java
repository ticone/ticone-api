/**
 * 
 */
package dk.sdu.imada.ticone.permute;

/**
 * @author Christian Wiwie
 * 
 * @since May 6, 2017
 *
 */
public class ShuffleParameterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3189065806985957152L;

	/**
	 * 
	 */
	public ShuffleParameterException(final String m) {
		super(m);
	}
}
