/**
 * 
 */
package dk.sdu.imada.ticone.permute;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class ShuffleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5143553576383368747L;

	/**
	 * 
	 */
	public ShuffleException(final String message) {
		super(message);
	}

	public ShuffleException(final Throwable e) {
		super(e);
	}
}
