/**
 * 
 */
package dk.sdu.imada.ticone.permute;

import java.util.Collections;
import java.util.Map;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * @author Christian Wiwie
 * 
 * @since May 7, 2017
 *
 */
public class BasicShuffleResult implements IShuffleResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8130861537084034765L;

	protected final IObjectWithFeatures original, shuffled;

	protected final Map<IObjectWithFeatures, IObjectWithFeatures> shuffledSupport;

	/**
	 * 
	 */
	public BasicShuffleResult(final IObjectWithFeatures original, final IObjectWithFeatures shuffled) {
		super();
		this.original = original;
		this.shuffled = shuffled;
		this.shuffledSupport = Collections.emptyMap();
	}

	/**
	 * 
	 */
	public BasicShuffleResult(final IObjectWithFeatures original, final IObjectWithFeatures shuffled,
			final Map<IObjectWithFeatures, IObjectWithFeatures> shuffledSupport) {
		super();
		this.original = original;
		this.shuffled = shuffled;
		this.shuffledSupport = shuffledSupport;
	}

	@Override
	public IObjectWithFeatures getOriginal() {
		return this.original;
	}

	@Override
	public IObjectWithFeatures getShuffled() {
		return this.shuffled;
	}

	@Override
	public boolean hasShuffledSupport() {
		return !this.shuffledSupport.isEmpty();
	}

	/**
	 * @return the supportShuffled
	 */
	@Override
	public Map<IObjectWithFeatures, IObjectWithFeatures> getShuffledSupport() {
		return this.shuffledSupport;
	}
}
