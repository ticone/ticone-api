/**
 * 
 */
package dk.sdu.imada.ticone.permute;

/**
 * @author Christian Wiwie
 * 
 * @since May 8, 2017
 *
 */
public class ShuffleNotInitializedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5427464122029595759L;

	public ShuffleNotInitializedException(final String m) {
		super(String.format("This shuffle method has not been fully initialized: Field '%s' is not set.", m));
	}
}
