/**
 * 
 */
package dk.sdu.imada.ticone.permute;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleObjectTypeException;

/**
 * @author Christian Wiwie
 * 
 * @since May 7, 2017
 *
 */
public class BasicShuffleMapping implements IShuffleMapping {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6473026380911221634L;

	private final Map<ObjectType<?>, Map<IObjectWithFeatures, IObjectWithFeatures>> mapping;

	public BasicShuffleMapping() {
		super();
		this.mapping = new HashMap<>();
	}

	public BasicShuffleMapping(IObjectWithFeatures[] originalObjects, IObjectWithFeatures[] shuffledObjects)
			throws IncompatibleObjectTypeException {
		this();
		if (originalObjects.length != shuffledObjects.length)
			throw new IllegalArgumentException("The number of original and shuffled objects needs to be the same");
		for (int i = 0; i < originalObjects.length; i++) {
			final IObjectWithFeatures originalObject = originalObjects[i];
			final IObjectWithFeatures shuffledObject = shuffledObjects[i];
			final ObjectType<? extends IObjectWithFeatures> objectType = originalObject.getObjectType();
			if (objectType != shuffledObject.getObjectType())
				throw new IncompatibleObjectTypeException(
						"Each shuffled object needs to have the same object type as its original counterpart.");
			else {
				this.mapping.putIfAbsent(objectType, new HashMap<>());
				this.mapping.get(objectType).put(originalObject, shuffledObject);
			}
		}

	}

	@Override
	public Collection<ObjectType<? extends IObjectWithFeatures>> getObjectTypes() {
		return this.mapping.keySet();
	}

	@Override
	public <O extends IObjectWithFeatures> O get(final O key) throws IncompatibleMappingAndObjectTypeException {
		if (!this.mapping.containsKey(key.getObjectType()))
			throw new IncompatibleMappingAndObjectTypeException();
		return (O) this.mapping.get(key.getObjectType()).get(key);
	}

	@Override
	public <O extends IObjectWithFeatures> O put(final O key, final O value) {
		this.mapping.putIfAbsent(key.getObjectType(), new HashMap<>());
		return (O) this.mapping.get(key.getObjectType()).put(key, value);
	}

	@Override
	public <O extends IObjectWithFeatures> Set<O> keySet(final ObjectType<O> objectType)
			throws IncompatibleMappingAndObjectTypeException {
		if (!this.mapping.containsKey(objectType))
			throw new IncompatibleMappingAndObjectTypeException();
		return (Set<O>) this.mapping.get(objectType).keySet();
	}

	@Override
	public <O extends IObjectWithFeatures> Set<O> values(final ObjectType<O> objectType)
			throws IncompatibleMappingAndObjectTypeException {
		if (!this.mapping.containsKey(objectType))
			throw new IncompatibleMappingAndObjectTypeException();
		return new HashSet<>((Collection<? extends O>) this.mapping.get(objectType).values());
	}
}
