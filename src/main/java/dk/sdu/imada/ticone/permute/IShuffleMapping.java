/**
 * 
 */
package dk.sdu.imada.ticone.permute;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleMappingAndObjectTypeException;

/**
 * @author Christian Wiwie
 * 
 * @since May 7, 2017
 *
 */
public interface IShuffleMapping extends Serializable {

	Collection<ObjectType<? extends IObjectWithFeatures>> getObjectTypes();

	<O extends IObjectWithFeatures> O put(O key, O value);

	<O extends IObjectWithFeatures> O get(O key) throws IncompatibleMappingAndObjectTypeException;

	<O extends IObjectWithFeatures> Set<O> keySet(ObjectType<O> objectType)
			throws IncompatibleMappingAndObjectTypeException;

	<O extends IObjectWithFeatures> Set<O> values(ObjectType<O> objectType)
			throws IncompatibleMappingAndObjectTypeException;
}
