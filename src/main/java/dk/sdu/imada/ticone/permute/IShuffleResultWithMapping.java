/**
 * 
 */
package dk.sdu.imada.ticone.permute;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 10, 2018
 *
 * @param <M> The type for which instances of the original object are mapped to
 *        instances of the shuffled object.
 */
public interface IShuffleResultWithMapping extends IShuffleResult {
	IShuffleMapping getShuffleMapping();

	void setShuffleMapping(IShuffleMapping mapping);

	boolean hasShuffleMapping();

}
