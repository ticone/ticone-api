/**
 * 
 */
package dk.sdu.imada.ticone.permute;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 14, 2019
 *
 */
public class IncompatibleShuffleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -784958608120336474L;

	public IncompatibleShuffleException() {
		super();
	}

	public IncompatibleShuffleException(String message) {
		super(message);
	}

	public IncompatibleShuffleException(Throwable cause) {
		super(cause);
	}

}
