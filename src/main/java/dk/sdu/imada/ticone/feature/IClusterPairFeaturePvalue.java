/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.pair.IClusterPairFeature;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 28, 2019
 *
 */
public interface IClusterPairFeaturePvalue extends IClusterPairFeature<Double>, IFeatureWithValueProvider<Double> {

}