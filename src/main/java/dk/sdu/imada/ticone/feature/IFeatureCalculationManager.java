/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.util.Set;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * A manager that knows how to delegate supported types of requested feature
 * calculations.
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 7, 2019
 *
 */
public interface IFeatureCalculationManager {
	boolean requestFeatureCalculation(IFeature<?> feature);

	Set<IFeature<Object>> getRequestedFeatureCalculations(ObjectType<?> type);
}
