/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.io.Serializable;

import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.util.IFeatureValueProviderListener;

/**
 * Manages events and listeners of a single {@link IFeatureValueProvider}.
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public interface IFeatureValueProviderManager extends Serializable {

	void addFeatureValueProviderListener(final IFeatureValueProviderListener listener);

	void removeFeatureValueProviderListener(final IFeatureValueProviderListener listener);

	void fireFeatureValueProviderChanged();

	IFeatureValueProvider getFeatureValueProvider();
}
