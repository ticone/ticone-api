/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Dec 17, 2018
 *
 */
public interface IObjectWithFeaturesProxy {
	IObjectWithFeatures getObjectWithFeatures();
}
