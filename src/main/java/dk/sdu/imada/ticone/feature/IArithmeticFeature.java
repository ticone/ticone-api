/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.fitness.IFitnessScore;

/**
 * A feature with arithmetic values. Such values can be used in {@link IScaler}
 * and {@link IFitnessScore}.
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 11, 2019
 *
 */
public interface IArithmeticFeature<R> extends IFeature<R> {

}
