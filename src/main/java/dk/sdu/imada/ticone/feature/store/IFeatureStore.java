/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.Pair;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 28, 2017
 *
 */
public interface IFeatureStore extends Serializable {

	boolean contains(IObjectWithFeatures key);

	boolean contains(IFeature<?> key);

	Set<IObjectWithFeatures> keySet();

	<O extends IObjectWithFeatures> Set<O> keySet(ObjectType<O> keyType);

	Collection<IObjectWithFeatures> keySet(IFeature<?> feature);

	Set<IFeature<?>> featureSet();

	<O extends IObjectWithFeatures> boolean hasFeatureFor(IFeature<?> feature, O object);

	<O extends IObjectWithFeatures> Set<IFeature<?>> getFeaturesFor(O key);

	<O extends IObjectWithFeatures, V> void setFeatureValue(O object, IFeature<V> feature, IFeatureValue<V> value);

	<O extends IObjectWithFeatures, V> IFeatureValue<V> getFeatureValue(O key, IFeature<V> feature);

	<O extends IObjectWithFeatures, V> void setFeatureValues(List<Pair<O, IFeature<V>>> objectFeaturePairs,
			List<IFeatureValue<V>> values);

	/**
	 * Retrieves a mapping from object to stored values of this feature.
	 * 
	 * @param feature
	 * @return A newly created map containing objects and corresponding feature
	 *         values.
	 */
	<V> Map<IObjectWithFeatures, IFeatureValue<V>> getFeatureValueMap(IFeature<V> feature);

	/**
	 * Retrieves a mapping from object to stored values of this feature for objects
	 * of the specified type.
	 * 
	 * @param feature
	 * @return A newly created map containing objects and corresponding feature
	 *         values.
	 */
	<O extends IObjectWithFeatures, V> Map<O, IFeatureValue<V>> getFeatureValueMap(IFeature<V> feature,
			ObjectType<O> objectType);

	/**
	 * Retrieves all stores values of the specified feature in the order they were
	 * inserted into this store.
	 * 
	 * @param feature
	 * @return
	 */
	<V> Iterable<IFeatureValue<V>> getFeatureValues(IFeature<V> feature);

	/**
	 * Retrieves all stored values of the specified feature for objects of the
	 * specified type in the order they were inserted into this store.
	 * 
	 * @param feature
	 * @return
	 */
	<V> Iterable<IFeatureValue<V>> getFeatureValues(IFeature<V> feature, ObjectType<?> objectType);

	/**
	 * Retrieves unwrapped raw values of all the {@link IFeatureValue} of the
	 * specified feature.
	 * 
	 * @param feature
	 * @return
	 */
	<V> Collection<V> valuesRaw(IFeature<V> feature);

	void addFeatureAddedListener(IFeatureStoreFeatureAddedListener l);

	void removeFeatureAddedListener(IFeatureStoreFeatureAddedListener l);

	void addFeatureStoreChangeListener(IFeatureStoreChangedListener l);

	void removeFeatureStoreChangeListener(IFeatureStoreChangedListener l);

	boolean addFeatureValueStoredListener(IFeatureValueStoredListener l);

	boolean removeFeatureValueStoredListener(IFeatureValueStoredListener l);

	IFeatureStore copy();

	void clear();

	boolean removeFeature(IFeature<?> f);

	void removeFeatureRemovedListener(final IFeatureStoreFeatureRemovedListener l);

	void addFeatureRemovedListener(final IFeatureStoreFeatureRemovedListener l);
}