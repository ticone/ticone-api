/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.io.Serializable;

import dk.sdu.imada.ticone.clustering.ITiconeClusteringResult;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public class NewFeatureStoreEvent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5221318767208757708L;
	protected ITiconeClusteringResult clustering;
	protected IFeatureStore oldStore, newStore;

	public NewFeatureStoreEvent(final ITiconeClusteringResult clustering, final IFeatureStore oldStore,
			final IFeatureStore newStore) {
		super();
		this.clustering = clustering;
		this.oldStore = oldStore;
		this.newStore = newStore;
	}

	/**
	 * @return the clustering
	 */
	public ITiconeClusteringResult getClustering() {
		return this.clustering;
	}

	/**
	 * @return the oldStore
	 */
	public IFeatureStore getOldStore() {
		return this.oldStore;
	}

	/**
	 * @return the newStore
	 */
	public IFeatureStore getNewStore() {
		return this.newStore;
	}
}
