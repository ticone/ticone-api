/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 14, 2019
 *
 */
public class FeatureValueNotSetException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7633987887891417768L;

	private final IObjectWithFeatures object;

	private final IFeature<?> feature;

	public FeatureValueNotSetException(final IObjectWithFeatures object, final IFeature<?> feature) {
		super();
		this.object = object;
		this.feature = feature;
	}

	/**
	 * @return the object
	 */
	public IObjectWithFeatures getObject() {
		return this.object;
	}

	/**
	 * @return the feature
	 */
	public IFeature<?> getFeature() {
		return this.feature;
	}
}
