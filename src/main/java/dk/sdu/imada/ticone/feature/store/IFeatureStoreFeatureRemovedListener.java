/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 21, 2019
 *
 */
public interface IFeatureStoreFeatureRemovedListener {
	void storeFeatureRemoved(final FeaturesRemovedEvent e);
}
