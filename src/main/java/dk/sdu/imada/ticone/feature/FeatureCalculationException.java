/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class FeatureCalculationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5841485105468711721L;

	/**
	 * 
	 */
	public FeatureCalculationException(final Throwable e) {
		super(e);
	}

	/**
	 * 
	 */
	public FeatureCalculationException(final String message) {
		super(message);
	}
}
