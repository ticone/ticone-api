/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.pair.IClusterPairFeature;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 11, 2018
 *
 */
public interface IClusterPairFeatureShortestPath
		extends IClusterPairFeature<Double>, IFeatureWithValueProvider<Double> {

	boolean isDirected();

	boolean isEnsureKnownObjects();
}