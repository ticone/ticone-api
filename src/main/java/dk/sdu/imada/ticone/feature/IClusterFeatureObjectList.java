/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 11, 2018
 *
 */
public interface IClusterFeatureObjectList extends IClusterFeature<ITimeSeriesObjectList> {

}
