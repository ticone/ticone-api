/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.statistics.ICalculatePValues;
import dk.sdu.imada.ticone.util.IConvertibleToNumber;

/**
 * A feature that produces arithmetic values.
 * 
 * {@link IConvertibleToNumberFeature}s can be used in {@link IFitnessScore} and
 * {@link ICalculatePValues}. <br>
 * <br>
 * Arithmetic values are {@link Number}s and {@link IConvertibleToNumber}.
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 11, 2019
 *
 */
public interface IConvertibleToNumberFeature<R extends IConvertibleToNumber<?>> extends IArithmeticFeature<R> {

}
