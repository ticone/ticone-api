/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.io.Serializable;
import java.util.Collection;

import dk.sdu.imada.ticone.feature.IFeature;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public class FeaturesAddedEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1745890263304996563L;
	protected IFeatureStore store;
	protected Collection<IFeature<?>> addedFeatures;

	/**
	 * 
	 */
	public FeaturesAddedEvent(final IFeatureStore store) {
		super();
		this.store = store;
	}

	/**
	 * @param addedFeatures the addedFeatures to set
	 */
	public void setAddedFeatures(final Collection<IFeature<?>> addedFeatures) {
		this.addedFeatures = addedFeatures;
	}

	/**
	 * @return the addedFeatures
	 */
	public Collection<IFeature<?>> getAddedFeatures() {
		return this.addedFeatures;
	}

	/**
	 * @return the store
	 */
	public IFeatureStore getStore() {
		return this.store;
	}
}
