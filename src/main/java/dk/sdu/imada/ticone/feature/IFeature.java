/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.util.IProvider;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 * @param O The type of objects for which this feature can be calculated.
 * @param R The wrapped raw type of the {@link IFeatureValue} instances that
 *          this {@link IFeature} returns.
 */
public interface IFeature<R> extends Serializable {

	/**
	 * 
	 * @throws InterruptedException
	 */
	<O extends IObjectWithFeatures> IFeatureValue<R> calculate(IObjectWithFeatures object)
			throws IncompatibleFeatureAndObjectException, FeatureCalculationException, IncorrectlyInitializedException,
			InterruptedException;

	String getName();

	ObjectType<? extends IObjectWithFeatures> supportedObjectType();

	Class<R> getValueType();

	/**
	 * @return
	 */
	IFeature<R> copy();

	/**
	 * Overwrite this method if your subtype needs some fields initialized before
	 * {@link #calculate(IFeatureValueProvider, IObjectWithFeatures)} and
	 * {@link #doCalculate(IFeatureValueProvider, IObjectWithFeatures)} is called.
	 * 
	 * @return
	 * @throws IncorrectlyInitializedException
	 */
	default boolean validateInitialized() throws IncorrectlyInitializedException {
		return true;
	}

	IFeatureStore getCache();

	boolean hasCache();

	void setUseCache(final boolean useCache);

	boolean isStoreFeatureInValues();

	void setStoreFeatureInValues(boolean storeFeature);

	boolean isStoreObjectInValues();

	void setStoreObjectInValues(boolean storeObject);

	public interface IFeatureValueProvider extends IProvider {

		/**
		 * @return The types of objects this instance provides feature values for.
		 */
		default Collection<ObjectType<?>> providesValuesForObjectTypes() {
			return Collections.emptySet();
		}

		/**
		 * 
		 * @param objectType
		 * @return
		 * @throws IncompatibleFeatureValueProviderException If the provided object type
		 *                                                   is not supported.
		 */
		default Collection<? extends IFeatureWithValueProvider> featuresProvidedValuesFor(ObjectType<?> objectType)
				throws IncompatibleFeatureValueProviderException {
			if (!providesValuesForObjectTypes().contains(objectType))
				throw new IncompatibleFeatureValueProviderException(this, objectType);
			return Collections.emptySet();
		}

		void ensureObjectIsKnown(IObjectWithFeatures object)
				throws UnknownObjectFeatureValueProviderException, IncompatibleFeatureValueProviderException;

		boolean initializeForFeature(IFeature<?> feature);

		<V> IFeatureValue<V> getFeatureValue(IFeature<V> feature, IObjectWithFeatures object)
				throws IncompatibleFeatureValueProviderException, UnknownObjectFeatureValueProviderException,
				FeatureCalculationException, InterruptedException, IncompatibleFeatureAndObjectException;

		default void validateObjectType(ObjectType<?> type) throws IncompatibleFeatureValueProviderException {
			if (!providesValuesForObjectTypes().contains(type))
				throw new IncompatibleFeatureValueProviderException(this, type);
		}

		IFeatureValueProvider copy() throws InterruptedException;

		default Collection<? extends IFeatureValueProvider> dependsOn() {
			return Collections.emptySet();
		}

		default boolean updateDependency(IFeatureValueProvider previous, IFeatureValueProvider updated) {
			return false;
		}
	}

	IFeatureValue<R> value(IObjectWithFeatures object, R rawValue) throws IncompatibleFeatureAndObjectException;

	IFeatureValue<R> value(R rawValue);
}