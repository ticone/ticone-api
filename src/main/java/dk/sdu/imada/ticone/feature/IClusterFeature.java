/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 8, 2018
 *
 */
public interface IClusterFeature<R> extends IFeature<R> {

	public interface Provider extends IFeature.IFeatureValueProvider {
		void ensureObjectIsKnown(final ICluster cluster) throws UnknownObjectFeatureValueProviderException;
	}
}
