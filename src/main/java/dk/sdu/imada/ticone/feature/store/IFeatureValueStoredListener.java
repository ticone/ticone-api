/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.io.Serializable;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 6, 2019
 *
 */
public interface IFeatureValueStoredListener extends Serializable {
	void featureValuesStored(final FeatureValuesStoredEvent e);
}
