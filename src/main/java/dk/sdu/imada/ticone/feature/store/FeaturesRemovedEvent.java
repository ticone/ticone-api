/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.util.Collection;

import dk.sdu.imada.ticone.feature.IFeature;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 21, 2019
 *
 */
public class FeaturesRemovedEvent {

	protected IFeatureStore store;
	protected Collection<IFeature<?>> removedFeatures;

	/**
		 * 
		 */
	public FeaturesRemovedEvent(final IFeatureStore store) {
		super();
		this.store = store;
	}

	public void setRemovedFeatures(final Collection<IFeature<?>> removedFeatures) {
		this.removedFeatures = removedFeatures;
	}

	public Collection<IFeature<?>> getRemovedFeatures() {
		return this.removedFeatures;
	}

	/**
	 * @return the store
	 */
	public IFeatureStore getStore() {
		return this.store;
	}
}
