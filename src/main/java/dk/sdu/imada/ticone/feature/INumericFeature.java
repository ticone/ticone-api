/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 11, 2019
 *
 */
public interface INumericFeature<N extends Number> extends IArithmeticFeature<N> {
}
