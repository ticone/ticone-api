/**
 * 
 */
package dk.sdu.imada.ticone.feature.scale;

import java.io.Serializable;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.statistics.IFeatureValueSample;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IBuilder;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 3, 2018
 *
 */
public interface IScalerBuilder<V, O extends IObjectWithFeatures, F extends IArithmeticFeature<V>>
		extends IBuilder<IScaler, FactoryException, CreateInstanceFactoryException>, Serializable {

	/**
	 * @return the forObject
	 */
	IObjectProvider getObjectProvider();

	/**
	 * @param forObject the forObject to set
	 */
	IScalerBuilder<V, O, F> setObjectProvider(IObjectProvider forObject);

	IFeatureValueSample<V> getFeatureValueSample();

	IScalerBuilder<V, O, F> setFeatureValueSample(IFeatureValueSample<V> featureValueSample);

	F getFeatureToScale();

	IScalerBuilder<V, O, F> setFeatureToScale(F featureToScale);

	IScalerBuilder<V, O, F> setSeed(long seed);

	IScalerBuilder<V, O, F> setSampleSize(int sampleSize);
}
