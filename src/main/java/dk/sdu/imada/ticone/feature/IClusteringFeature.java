/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 7, 2019
 *
 */
public interface IClusteringFeature<R> extends IFeature<R> {
}
