/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.IncorrectlyInitializedException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 * @param <T>
 * @param <V>
 */
public interface IFeatureWithValueProvider<R> extends IFeature<R> {

	void setFeatureValueProvider(IFeatureValueProvider provider) throws IncompatibleFeatureValueProviderException;

	IFeatureValueProvider getFeatureValueProvider();

	@Override
	default boolean validateInitialized() throws IncorrectlyInitializedException {
		if (!IFeature.super.validateInitialized())
			return false;
		if (this.getFeatureValueProvider() == null)
			throw new IncorrectlyInitializedException("provider");
		return true;
	}

}