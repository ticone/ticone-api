/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 9, 2018
 *
 */
public interface IObjectPairFeature<R> extends IFeature<R> {
}
