/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;

/**
 * 
 * A general exception indicating that something went wrong with the specified
 * provider.
 * 
 * @author Christian Wiwie
 * 
 * @since Mar 29, 2019
 *
 */
public class FeatureValueProviderInitializationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -858370038938774229L;
	protected IFeature.IFeatureValueProvider provider;

	public FeatureValueProviderInitializationException(IFeatureValueProvider provider, Throwable e) {
		super(e);
		this.provider = provider;
	}

	/**
	 * @return the provider
	 */
	public IFeature.IFeatureValueProvider getProvider() {
		return this.provider;
	}
}
