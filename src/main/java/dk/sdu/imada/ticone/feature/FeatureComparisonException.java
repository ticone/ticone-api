/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 11, 2019
 *
 */
public class FeatureComparisonException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4117039211042458565L;

	public FeatureComparisonException() {
		super();
	}

	public FeatureComparisonException(String message) {
		super(message);
	}

	public FeatureComparisonException(Throwable cause) {
		super(cause);
	}

}
