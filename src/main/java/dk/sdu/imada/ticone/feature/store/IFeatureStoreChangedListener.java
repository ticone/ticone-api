/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public interface IFeatureStoreChangedListener extends Serializable {
	void featureStoreChanged(final FeatureStoreChangedEvent e);
}
