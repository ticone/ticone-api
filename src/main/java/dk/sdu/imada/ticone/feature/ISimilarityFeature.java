/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 17, 2018
 *
 * @param <O>
 */
public interface ISimilarityFeature
		extends IFeatureWithValueProvider<ISimilarityValue>, IConvertibleToNumberFeature<ISimilarityValue> {

	ISimilarityFunction getSimilarityFunction();

	void setSimilarityFunction(ISimilarityFunction similarityFunction) throws IncompatibleSimilarityFunctionException;
}