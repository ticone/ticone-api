/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.pair.IClusterObjectMappingPair;
import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public interface IObjectWithFeatures extends Serializable {

	/**
	 * Instances of this class denote the considered base types of
	 * {@link IObjectWithFeatures} in various operations. For instance, when
	 * deciding whether certain types of {@link IObjectWithFeatures} are provided by
	 * a {@link IObjectProvider} or whether feature values are provided for a type
	 * of object by a {@link IFeatureValueProvider}.
	 * 
	 * @author Christian Wiwie
	 * 
	 * @since Feb 14, 2019
	 *
	 * @param <T>
	 */
	public final class ObjectType<T extends IObjectWithFeatures> implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2913610669798530175L;
		public static final ObjectType<ICluster> CLUSTER = new ObjectType<>(ICluster.class);
		public static final ObjectType<IClusterObjectMapping> CLUSTERING = new ObjectType<>(
				IClusterObjectMapping.class);
		public static final ObjectType<IClusterObjectMappingPair> CLUSTERING_PAIR = new ObjectType<>(
				IClusterObjectMappingPair.class);
		public static final ObjectType<IClusterPair> CLUSTER_PAIR = new ObjectType<>(IClusterPair.class);
		public static final ObjectType<ITimeSeriesObject> OBJECT = new ObjectType<>(ITimeSeriesObject.class);
		public static final ObjectType<IObjectClusterPair> OBJECT_CLUSTER_PAIR = new ObjectType<>(
				IObjectClusterPair.class);
		public static final ObjectType<IObjectPair> OBJECT_PAIR = new ObjectType<>(IObjectPair.class);
		public static final ObjectType<ITimeSeries> TIME_SERIES = new ObjectType<>(ITimeSeries.class);
		public static final ObjectType<ITimeSeriesObjects> OBJECTS = new ObjectType<>(ITimeSeriesObjects.class);
		public static final ObjectType<ITiconeNetwork> NETWORK = new ObjectType<>(ITiconeNetwork.class);

		private final Class<T> baseType;

		private ObjectType(Class<T> baseType) {
			this.baseType = baseType;
		}

		/**
		 * @return the baseType
		 */
		public Class<T> getBaseType() {
			return this.baseType;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof ObjectType)
				return baseType.equals(((ObjectType) obj).baseType);
			return false;
		}

		@Override
		public int hashCode() {
			return baseType.hashCode();
		}

		@Override
		public String toString() {
			final String name = this.baseType.getSimpleName().replaceFirst("^I", "");
			final Matcher matcher = Pattern.compile("([a-z])([A-Z])").matcher(name);
			final StringBuffer sb = new StringBuffer();
			while (matcher.find()) {
				matcher.appendReplacement(sb, matcher.group(1) + "_" + matcher.group(2));
			}
			matcher.appendTail(sb);
			return sb.toString().toUpperCase();
		}
	}

	default String getName() {
		return toString();
	}

	ObjectType<? extends IObjectWithFeatures> getObjectType();

	default boolean storesFeatureValues() {
		return false;
	}

	default boolean hasFeatureValueSet(IFeature<?> feature) {
		return false;
	}

	default <V> void setFeatureValue(IFeature<V> feature, IFeatureValue<V> value) {
	}

	default <V> IFeatureValue<V> getFeatureValue(IFeature<V> feature) throws FeatureValueNotSetException {
		throw new FeatureValueNotSetException(this, feature);
	}
}
