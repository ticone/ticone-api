/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.io.Serializable;

import dk.sdu.imada.ticone.statistics.IFeatureValueSample;
import dk.sdu.imada.ticone.statistics.IFeatureValueSamples;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * Manages all {@link IFeatureValueSample}s created for a single
 * {@link IObjectProvider}.
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public interface IFeatureValueSampleManager extends Serializable {

	IObjectProvider getObjectProvider();

	IFeatureValueSamples getFeatureValueSamples();

}
