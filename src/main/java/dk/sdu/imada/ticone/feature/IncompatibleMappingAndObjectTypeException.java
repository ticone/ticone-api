/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 14, 2019
 *
 */
public class IncompatibleMappingAndObjectTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8988814696938629243L;

	public IncompatibleMappingAndObjectTypeException() {
		super();
	}

	public IncompatibleMappingAndObjectTypeException(String message) {
		super(message);
	}

	public IncompatibleMappingAndObjectTypeException(Throwable cause) {
		super(cause);
	}

}
