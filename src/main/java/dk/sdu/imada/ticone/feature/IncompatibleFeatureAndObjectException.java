/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 13, 2019
 *
 */
public class IncompatibleFeatureAndObjectException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -631809089754110741L;

	public IncompatibleFeatureAndObjectException() {
		super();
	}

	public IncompatibleFeatureAndObjectException(String message) {
		super(message);
	}

	public IncompatibleFeatureAndObjectException(Throwable cause) {
		super(cause);
	}

}
