/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 11, 2018
 *
 */
public interface IObjectPairFeatureShortestDistance
		extends IObjectPairFeature<Double>, IFeatureWithValueProvider<Double> {

	boolean isDirected();

	boolean isEnsureKnownObjects();
}