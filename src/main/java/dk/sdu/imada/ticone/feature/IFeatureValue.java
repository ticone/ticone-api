/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import java.io.Serializable;

import dk.sdu.imada.ticone.util.NotAnArithmeticFeatureValueException;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public interface IFeatureValue<R> extends Serializable {
	Class<? extends IFeature<R>> getFeatureType();

	Class<R> getValueType();

	IFeature<R> getFeature();

	IObjectWithFeatures getObject();

	R getValue();

	void setObject(IObjectWithFeatures object);

	void setFeature(IFeature<R> feature);

	Number toNumber() throws NotAnArithmeticFeatureValueException, ToNumberConversionException;

	boolean isArithmetic();

	boolean isNaN();
}
