/**
 * 
 */
package dk.sdu.imada.ticone.feature;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 10, 2018
 *
 */
public interface IClusterFeaturePrior extends IClusterFeature<Double>, IFeatureWithValueProvider<Double> {

	int getNumberBins();

}