/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 6, 2019
 *
 */
public class FeatureValuesStoredEvent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3251960668778460475L;
	protected final IFeatureStore store;
	private final Map<IFeature<?>, Collection<IObjectWithFeatures>> objectFeaturePairs;

	/**
	 * 
	 */
	public FeatureValuesStoredEvent(final IFeatureStore store,
			final Map<IFeature<?>, Collection<IObjectWithFeatures>> objectFeaturePairs) {
		super();
		this.store = store;
		this.objectFeaturePairs = objectFeaturePairs;
	}

	/**
	 * @return the store
	 */
	public IFeatureStore getStore() {
		return this.store;
	}

	/**
	 * @return the objectFeaturePairs
	 */
	public Map<IFeature<?>, Collection<IObjectWithFeatures>> getObjectFeaturePairs() {
		return this.objectFeaturePairs;
	}
}
