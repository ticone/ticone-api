/**
 * 
 */
package dk.sdu.imada.ticone.feature;

import dk.sdu.imada.ticone.statistics.IPvalue;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 10, 2018
 *
 */
public interface IFeaturePvalue extends IFeatureWithValueProvider<IPvalue>, IArithmeticFeature<IPvalue> {

}