/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public interface IFeatureStoreFeatureAddedListener extends Serializable {
	void storeFeatureAdded(final FeaturesAddedEvent e);
}
