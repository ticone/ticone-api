/**
 * 
 */
package dk.sdu.imada.ticone.feature.store;

import java.io.Serializable;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 6, 2019
 *
 */
public class FeatureStoreChangedEvent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6874989973988098081L;
	protected final IFeatureStore store;

	/**
	 * 
	 */
	public FeatureStoreChangedEvent(final IFeatureStore store) {
		super();
		this.store = store;
	}

	/**
	 * @return the store
	 */
	public IFeatureStore getStore() {
		return this.store;
	}
}
