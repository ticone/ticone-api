/**
 * 
 */
package dk.sdu.imada.ticone.feature.scale;

import java.io.Serializable;

import dk.sdu.imada.ticone.util.IConvertibleToNumber;
import dk.sdu.imada.ticone.util.ScalingException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public interface IScaler extends Serializable {

	double scale(IConvertibleToNumber<? extends Number> value) throws ScalingException;

	double scale(Number value) throws ScalingException;

	double[] scale(IConvertibleToNumber<? extends Number>... values) throws ScalingException;

	double[] scale(Number... values) throws ScalingException;

	IScaler copy();
}
