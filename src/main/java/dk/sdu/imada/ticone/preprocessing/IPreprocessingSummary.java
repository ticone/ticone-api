/**
 * 
 */
package dk.sdu.imada.ticone.preprocessing;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;

/**
 * @author Christian Wiwie
 * 
 * @since May 10, 2017
 *
 */
public interface IPreprocessingSummary<CLUSTERING extends IClusterObjectMapping> {

	void setRemoveObjectsLeastConserved(boolean removeObjectsLeastConserved);

	boolean isRemoveObjectsLeastConserved();

	void setRemoveObjectsLeastConservedThreshold(ISimilarityValue removeObjectsLeastConservedThreshold,
			boolean isPercent);

	ISimilarityValue getRemoveObjectsLeastConservedThreshold();

	boolean isRemoveObjectsLeastConservedIsPercent();

	void setRemoveObjectsLowVariance(boolean removeObjectsLowVariance);

	boolean isRemoveObjectsLowVariance();

	void setRemoveObjectsLowVarianceThreshold(double removeObjectsLowVarianceThreshold, boolean isPercent);

	double getRemoveObjectsLowVarianceThreshold();

	boolean isRemoveObjectsLowVarianceIsPercent();

	void setRemoveObjectsNotInNetwork(boolean removeObjectsNotInNetwork);

	boolean isRemoveObjectsNotInNetwork();

	void addRemovedObjectsNotInNetwork(ITimeSeriesObjects removedObjects);

	ITiconeNetwork getNetwork();

	void setNetwork(ITiconeNetwork network);

	String getNetworkName();

	ITimeSeriesObjects getRemovedObjectsNotInNetwork();

	void addRemovedObjectsLeastConserved(ITimeSeriesObjects removedObjects);

	ITimeSeriesObjects getRemovedObjectsLeastConserved();

	void addRemovedObjectsLowVariance(ITimeSeriesObjects removedObjects);

	ITimeSeriesObjects getRemovedObjectsLowVariance();

	void addListener(IPreprocessingSummaryListener l);

	void removeListener(IPreprocessingSummaryListener l);

	IPreprocessingSummary<CLUSTERING> copy();

}