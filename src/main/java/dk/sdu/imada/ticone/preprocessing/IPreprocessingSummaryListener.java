/**
 * 
 */
package dk.sdu.imada.ticone.preprocessing;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since May 10, 2017
 *
 */
public interface IPreprocessingSummaryListener extends Serializable {

	/**
	 * 
	 */
	void preprocessingSummaryChanged(final PreprocessingSummaryEvent e);

}
