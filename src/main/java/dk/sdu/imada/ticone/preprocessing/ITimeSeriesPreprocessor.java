/**
 * 
 */
package dk.sdu.imada.ticone.preprocessing;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public interface ITimeSeriesPreprocessor {

	void initializeObjects(ITimeSeriesObjectList timeSeriesDatas);

	void initializeObjects(ITimeSeriesObject... objects);

	ITimeSeriesObjectList getObjects();

	void process() throws PreprocessingException, InterruptedException;

	double getMinValue();

	double getMaxValue();

}