/**
 * 
 */
package dk.sdu.imada.ticone.preprocessing;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 15, 2019
 *
 */
public class PreprocessingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -563202888979423069L;

	public PreprocessingException() {
		super();
	}

	public PreprocessingException(String message) {
		super(message);
	}

	public PreprocessingException(Throwable cause) {
		super(cause);
	}

}
