/**
 * 
 */
package dk.sdu.imada.ticone.preprocessing;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since May 10, 2017
 *
 */
public class PreprocessingSummaryEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8016055432116195091L;

	protected IPreprocessingSummary source;

	/**
	 * 
	 */
	public PreprocessingSummaryEvent(final IPreprocessingSummary source) {
		super();
		this.source = source;
	}

	public IPreprocessingSummary getPreprocessingSummary() {
		return this.source;
	}
}
