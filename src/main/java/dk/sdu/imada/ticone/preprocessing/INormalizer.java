package dk.sdu.imada.ticone.preprocessing;

public interface INormalizer {
	double normalize(final double value);
}
