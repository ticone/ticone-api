/**
 * 
 */
package dk.sdu.imada.ticone.connectivity;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class ClusterConnectivityException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7987568307174528904L;

	/**
	 * 
	 */
	public ClusterConnectivityException(final String m) {
		super(m);
	}

	/**
	 * 
	 */
	public ClusterConnectivityException(final Throwable e) {
		super(e);
	}
}
