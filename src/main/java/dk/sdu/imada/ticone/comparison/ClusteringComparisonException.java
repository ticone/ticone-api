/**
 * 
 */
package dk.sdu.imada.ticone.comparison;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class ClusteringComparisonException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6885632501712122521L;

	/**
	 * 
	 */
	public ClusteringComparisonException(final Throwable e) {
		super(e);
	}
}
