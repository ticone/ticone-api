/**
 * 
 */
package dk.sdu.imada.ticone.comparison;

import dk.sdu.imada.ticone.clustering.pair.IClusterPairFeature;
import dk.sdu.imada.ticone.feature.INumericFeature;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 11, 2018
 *
 */
public interface IClusterPairFeatureCommonObjectsOverPrototypeSimilarity
		extends IClusterPairFeature<Double>, INumericFeature<Double> {

}
