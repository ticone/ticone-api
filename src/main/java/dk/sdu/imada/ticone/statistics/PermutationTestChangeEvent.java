/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import javax.swing.event.ChangeEvent;

public class PermutationTestChangeEvent extends ChangeEvent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3583184385017658407L;
	protected int finishedPermutations;
	protected int totalPermutations;

	/**
	 * 
	 */
	public PermutationTestChangeEvent(final Object source, final int totalPermutations, final int currentPermutation) {
		super(source);
		this.totalPermutations = totalPermutations;
		this.finishedPermutations = currentPermutation;
	}

	/**
	 * @return the currentPermutation
	 */
	public int getFinishedPermutations() {
		return this.finishedPermutations;
	}

	/**
	 * @return the totalPermutations
	 */
	public int getTotalPermutations() {
		return this.totalPermutations;
	}

	public double getPercentage() {
		return (double) this.finishedPermutations / this.totalPermutations;
	}
}