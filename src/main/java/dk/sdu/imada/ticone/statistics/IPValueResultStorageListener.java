/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 19, 2019
 *
 */
public interface IPValueResultStorageListener {
	void pvalueCalculationResultStored(PValueResultStorageEvent event);
}
