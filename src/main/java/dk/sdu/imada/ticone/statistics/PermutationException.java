/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 10, 2017
 *
 */
public class PermutationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1825269065415279405L;

	/**
	 * 
	 */
	public PermutationException(final String s) {
		super(s);
	}

	/**
	 * 
	 */
	public PermutationException(final Throwable t) {
		super(t);
	}
}
