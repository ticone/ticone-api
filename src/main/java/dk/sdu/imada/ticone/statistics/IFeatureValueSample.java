/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.Serializable;
import java.util.List;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 9, 2018
 *
 */
public interface IFeatureValueSample<R> extends Serializable, Iterable<IFeatureValueSample.Entry<R>> {

	IFeature<R> getFeature();

	IFeatureStore getFeatureStore();

	List<? extends IFeatureValue<R>> getValues();

	List<IObjectWithFeatures> getObjects();

	IFeatureValue<R> getValue(IObjectWithFeatures object);

	void setValue(IObjectWithFeatures object, IFeatureValue<R> value);

	default int size() {
		return getObjects().size();
	}

	public interface Entry<R> {
		IObjectWithFeatures getObject();

		IFeatureValue<R> getValue();
	}
}
