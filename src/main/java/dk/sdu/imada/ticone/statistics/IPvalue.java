/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import dk.sdu.imada.ticone.util.IConvertibleToNumber;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 29, 2019
 *
 */
public interface IPvalue extends Comparable<IPvalue>, IConvertibleToNumber<Double> {
	double getDouble();

	@Override
	default int compareTo(IPvalue o) {
		return Double.compare(getDouble(), o.getDouble());
	}

	@Override
	default Double toNumber() throws ToNumberConversionException {
		return getDouble();
	}

	@Override
	default int cardinality() {
		return 1;
	}
}
