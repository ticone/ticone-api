/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 9, 2018
 *
 */
public interface IFeatureValueSampler {

	<V> IFeatureValueSample<V> sampleFrom(final IFeature<V> feature, final IObjectProvider objectProvider,
			final int sampleSize, final long seed) throws FeatureValueSampleException;

	<V> IFeatureValueSample<V> sampleFrom(final IFeatureWithValueProvider<V> feature,
			final IObjectProvider objectProvider, final IFeatureValueProvider fvProvider, final int sampleSize,
			final long seed) throws FeatureValueSampleException;
}
