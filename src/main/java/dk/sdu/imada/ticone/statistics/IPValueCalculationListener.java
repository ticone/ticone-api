/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * @author Christian Wiwie
 * 
 * @since May 10, 2017
 *
 */
public interface IPValueCalculationListener {

	/**
	 * 
	 */
	void pvalueCalculationStarted(final PValueCalculationEvent event);

	/**
	 * 
	 */
	void pvalueCalculationFinished(final PValueCalculationEvent event);

}
