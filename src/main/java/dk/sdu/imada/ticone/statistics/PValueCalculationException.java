/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class PValueCalculationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5564877847283876122L;

	/**
	 * 
	 */
	public PValueCalculationException(final Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	public PValueCalculationException(final String message) {
		super(message);
	}
}
