/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.util.List;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeaturePvalue;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.IncompatibleFeatureAndObjectException;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.fitness.IFitnessValue;
import dk.sdu.imada.ticone.util.ITiconeResult;
import dk.sdu.imada.ticone.util.IncompatibleFeatureValueProviderException;
import dk.sdu.imada.ticone.util.UnknownObjectFeatureValueProviderException;
import it.unimi.dsi.fastutil.objects.ObjectList;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 14, 2018
 *
 */
public interface IPValueCalculationResult extends ITiconeResult, IFeatureValueProvider {

	<O extends IObjectWithFeatures> List<O> getObjects(ObjectType<O> type)
			throws IncompatibleFeatureValueProviderException;

	List<IFitnessScore> getFitnessScores(ObjectType<? extends IObjectWithFeatures> type);

	List<IArithmeticFeature<? extends Comparable<?>>> getFeatures(ObjectType<? extends IObjectWithFeatures> type);

	@Override
	List<ObjectType<?>> providesValuesForObjectTypes();

	default boolean containsPvaluesForObjectType(ObjectType<? extends IObjectWithFeatures> type) {
		return providesValuesForObjectTypes().contains(type);
	}

	/**
	 * @return the calculatePValues
	 */
	ICalculatePValues getCalculatePValues();

	/**
	 * @return the clusterFeatureValues
	 */
	IFeatureStore getFeatureStore();

	/**
	 * @return the clusterFeaturePValues
	 * @throws IncompatibleFeatureValueProviderException
	 */
	IPvalue getPValue(IFitnessScore fitnessScore, IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException;

	IPvalue getPValue(final IObjectWithFeatures key);

	ObjectList<IPvalue> getPValues(ObjectType<?> type) throws IncompatibleFeatureValueProviderException;

	boolean hasObjectSpecificPermutedFeatureValues(ObjectType<?> type) throws IncompatibleFeatureValueProviderException;

	/**
	 * @param calculatePValues the calculatePValues to set
	 */
	void setCalculatePValues(final ICalculatePValues calculatePValues);

	/**
	 * 
	 * @param featureStore
	 */
	void setFeatureStore(final IFeatureStore featureStore);

	boolean hasPermutedFeatureValues();

	<V> List<IFeatureValue<V>> getPermutedFeatureValues(ObjectType<?> objectType, IArithmeticFeature<V> feature)
			throws IncompatibleFeatureValueProviderException;

	<V> List<IFeatureValue<V>> getPermutedFeatureValuesObjectSpecific(IObjectWithFeatures object,
			IArithmeticFeature<V> feature) throws IncompatibleFeatureValueProviderException;

	IFitnessValue getOriginalFitness(IFitnessScore fitnessScore, IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException;

	void clearOriginalFitness();

	void clearPermutedFeatureValues();

	void clearPermutedFeatureValuesObjectSpecific();

	void clearPermutedFitnessValues();

	void clearPermutedFitnessValuesObjectSpecific();

	@Override
	default void ensureObjectIsKnown(IObjectWithFeatures object)
			throws UnknownObjectFeatureValueProviderException, IncompatibleFeatureValueProviderException {
		if (!getObjects(object.getObjectType()).contains(object))
			throw new UnknownObjectFeatureValueProviderException(this, object);
	}

	@Override
	default <V> IFeatureValue<V> getFeatureValue(IFeature<V> feature, IObjectWithFeatures object)
			throws IncompatibleFeatureValueProviderException, UnknownObjectFeatureValueProviderException,
			IncompatibleFeatureAndObjectException {
		if (!containsPvaluesForObjectType(object.getObjectType()))
			throw new IncompatibleFeatureValueProviderException(this, object.getObjectType());
		this.ensureObjectIsKnown(object);
		if (feature instanceof IFeaturePvalue)
			return feature.value(object, (V) this.getPValue(object));
		else
			return feature.value(object, (V) Double.valueOf(this.getPValue(object).getDouble()));
	}

	@Override
	default boolean initializeForFeature(IFeature<?> feature) {
		return true;
	}

	List<IArithmeticFeature<? extends Comparable<?>>> getConditionalFeaturesPerObjectType(ObjectType<?> objectType);

	long getSeed();
}
