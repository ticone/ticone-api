/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * @author Christian Wiwie
 * 
 * @since May 10, 2017
 *
 */
public class PValueCalculationEvent {
	protected Object source;

	public PValueCalculationEvent(final Object source) {
		super();
		this.source = source;
	}

	/**
	 * @return the source
	 */
	public Object getSource() {
		return this.source;
	}
}
