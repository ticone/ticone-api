/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.swing.event.ChangeListener;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.fitness.IFitnessScore;
import dk.sdu.imada.ticone.permute.IPermutable;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public interface ICalculatePValues extends Serializable {
	void addChangeListener(final ChangeListener listener);

	void removeChangeListener(final ChangeListener listener);

	boolean isCancelled();

	void cancel();

	Set<IArithmeticFeature<? extends Comparable<?>>> getFeatures();

	ICombinePValues getCombinePValues();

	int getNumberPermutations();

	<PROVIDER extends IObjectProvider & IObjectWithFeatures & IPermutable> IPValueCalculationResult calculatePValues(
			final PROVIDER objectProvider, final long masterSeed)
			throws InterruptedException, PValueCalculationException;

	List<IFitnessScore> getFitnessScores();

	IFeatureStore getFeatureStore();

	IShuffle getShuffle();

	int getNumberFinishedPermutations();

	List<IArithmeticFeature<? extends Comparable<?>>>[] getConditionalFeatures();

	void setShuffle(IShuffle shuffle);

	void setFeatureStore(IFeatureStore featureStore);

	List<ObjectType<?>> getObjectTypes();

	void setForceDisableObjectSpecificDistribution(boolean forceDisableObjectSpecificDistribution);

	boolean isForceDisableObjectSpecificDistribution();
}
