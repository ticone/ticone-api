/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.Serializable;
import java.util.List;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 19, 2017
 *
 */
public interface ICombinePValues extends Serializable {

	IPvalue combine(final IObjectWithFeatures cluster, final List<IPvalue> pvalues);

	String getName();

}
