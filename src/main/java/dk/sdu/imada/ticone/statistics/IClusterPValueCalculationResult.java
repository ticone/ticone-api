/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 1, 2019
 *
 */
public interface IClusterPValueCalculationResult extends IPValueCalculationResult {

}
