/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 17, 2018
 *
 */
public class FeatureValueSampleException extends Exception {

	public FeatureValueSampleException() {
		super();
	}

	public FeatureValueSampleException(final String message) {
		super(message);
	}

	public FeatureValueSampleException(final Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7776992751604415469L;

}
