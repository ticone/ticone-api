/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import java.io.Serializable;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeatureWithValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * 
 * Manages a collection of feature value samples for a {@link IObjectProvider}.
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 15, 2018
 */
public interface IFeatureValueSamples extends Serializable {

	IObjectProvider getProvider();

	boolean contains(IFeature<?> feature, int sampleSize);

	<O extends IObjectWithFeatures, V> IFeatureValueSample<V> get(IFeature<V> feature, int sampleSize, long seed)
			throws FeatureValueSampleException;

	<O extends IObjectWithFeatures, V> IFeatureValueSample<? extends V> get(
			IFeatureWithValueProvider<? extends V> feature, IFeatureValueProvider featureValueProvider, int sampleSize,
			long seed) throws FeatureValueSampleException;
}
