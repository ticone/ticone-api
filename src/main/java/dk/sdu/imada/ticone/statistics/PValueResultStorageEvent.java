/**
 * 
 */
package dk.sdu.imada.ticone.statistics;

import dk.sdu.imada.ticone.clustering.ITiconeClusteringResult;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 19, 2019
 *
 */
public class PValueResultStorageEvent {
	private final ITiconeClusteringResult source;
	private final int iteration;
	private final IPValueCalculationResult pvalueResult, predecessor;

	public PValueResultStorageEvent(final ITiconeClusteringResult source, final int iteration,
			final IPValueCalculationResult pvalueResult) {
		this(source, iteration, pvalueResult, null);
	}

	public PValueResultStorageEvent(final ITiconeClusteringResult source, final int iteration,
			final IPValueCalculationResult pvalueResult, final IPValueCalculationResult predecessor) {
		super();
		this.source = source;
		this.iteration = iteration;
		this.pvalueResult = pvalueResult;
		this.predecessor = predecessor;
	}

	/**
	 * @return the clusteringResult
	 */
	public ITiconeClusteringResult getSource() {
		return this.source;
	}

	/**
	 * @return the iteration
	 */
	public int getIteration() {
		return this.iteration;
	}

	/**
	 * @return the pvalueResult
	 */
	public IPValueCalculationResult getPvalueResult() {
		return this.pvalueResult;
	}

	/**
	 * @return the replaced
	 */
	public IPValueCalculationResult getPredecessor() {
		return this.predecessor;
	}
}
