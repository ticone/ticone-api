/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Arrays;
import java.util.Collection;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.permute.IPermutable;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public interface ITimeSeriesObjects
		extends Collection<ITimeSeriesObject>, IObjectProvider, IObjectWithFeatures, IPermutable {

	@Override
	default ObjectType<? extends IObjectWithFeatures> getObjectType() {
		return ObjectType.OBJECTS;
	}

	@Override
	default Collection<ObjectType<?>> providedTypesOfObjects() {
		return Arrays.asList(ObjectType.OBJECT_PAIR, ObjectType.OBJECT);
	}

	@Override
	default <T extends IObjectWithFeatures> int getNumberObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type == ObjectType.OBJECT_PAIR)
			return size() * size();
		if (type == ObjectType.OBJECT)
			return size();
		return IObjectProvider.super.getNumberObjectsOfType(type);
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type == ObjectType.OBJECT_PAIR)
			return (Collection<T>) getObjectPairs().asList();
		if (type == ObjectType.OBJECT)
			return (Collection<T>) getObjects().asList();
		return IObjectProvider.super.getObjectsOfType(type);
	}

	default ITimeSeriesObjects getObjects() {
		return this;
	}

	ITimeSeriesObjectSet asSet();

	ITimeSeriesObjectList asList();

	@Override
	ITimeSeriesObject[] toArray();

	ITimeSeriesObjects copy();

	/**
	 * Ensures that the objects in this collection are mapped to the specified
	 * network.
	 * 
	 * @param network
	 * @return
	 */
	ITimeSeriesObjects mapToNetwork(final ITiconeNetwork<?, ?> network);

	/**
	 * Ensures that the objects in this collection are not mapped to the specified
	 * network.
	 * 
	 * @param network
	 * @return
	 */
	ITimeSeriesObjects removeMappingToNetwork(ITiconeNetwork<?, ?> network);

	IObjectPairs getObjectPairs();

	/**
	 * Checks whether this collection contains an object with the specified name.
	 * 
	 * @param objectName
	 * @return True, if this collection contains an object with the specified name,
	 *         false otherwise.
	 */
	boolean contains(String objectName);

	/**
	 * Finds and returns the contained object with the specified name.
	 * 
	 * @param objectName
	 * @return The object with the specified name if it is contained in this
	 *         collection, null otherwise.
	 */
	ITimeSeriesObject get(String objectName);
}
