/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IPairList;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectSampleException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 7, 2018
 *
 */
public interface IObjectPairList extends IPairList<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>, IObjectPairs {

	@Override
	IObjectPairList copy();

	@Override
	default IObjectPairList asList() {
		return this;
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type.equals(ObjectType.OBJECT_PAIR))
			return (Collection<T>) this;
		return IObjectPairs.super.getObjectsOfType(type);
	}

	@Override
	default <T extends IObjectWithFeatures> List<T> sampleObjectsOfType(ObjectType<T> type, int sampleSize, long seed)
			throws IncompatibleObjectProviderException, ObjectSampleException {
		if (type == ObjectType.OBJECT_PAIR) {
			final int[] indices = new Random(seed).ints(sampleSize, 0, this.size()).toArray();
			final List<IObjectPair> result = new ArrayList<>();
			for (int i : indices)
				result.add(this.get(i));
			return (List<T>) result;
		}
		throw new ObjectSampleException();
	}

}
