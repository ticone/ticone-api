/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Collection;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IPairSet;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 14, 2019
 *
 */
public interface IObjectClusterPairSet
		extends IPairSet<ITimeSeriesObject, ICluster, IObjectClusterPair>, IObjectClusterPairs {
	@Override
	IObjectClusterPairSet copy();

	@Override
	default IObjectClusterPairSet asSet() {
		return this;
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type.equals(ObjectType.OBJECT_CLUSTER_PAIR))
			return (Collection<T>) this;
		return IObjectClusterPairs.super.getObjectsOfType(type);
	}
}
