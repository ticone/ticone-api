/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.List;

import dk.sdu.imada.ticone.network.ITiconeNetwork;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 12, 2018
 *
 */
public interface ITimeSeriesObjectList extends ITimeSeriesObjects, List<ITimeSeriesObject> {

	@Override
	ITimeSeriesObjectList copy();

	@Override
	default ITimeSeriesObjectList asList() {
		return this;
	}

	@Override
	ITimeSeriesObjectList mapToNetwork(ITiconeNetwork<?, ?> network);

	@Override
	ITimeSeriesObjectList removeMappingToNetwork(ITiconeNetwork<?, ?> network);
}
