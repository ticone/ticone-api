package dk.sdu.imada.ticone.data;

public class MultipleTimeSeriesSignalsForSameSampleParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9137692739935036952L;

	public MultipleTimeSeriesSignalsForSameSampleParseException(final String objectName, final String sampleName) {
		super(String.format("Encountered sample name '%s' several times for object '%s'. This is not supported.",
				sampleName, objectName));
	}
}
