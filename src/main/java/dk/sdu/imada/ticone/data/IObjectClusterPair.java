/**
 * 
 */
package dk.sdu.imada.ticone.data;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IPair;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 5, 2018
 *
 */
public interface IObjectClusterPair extends IPair<ITimeSeriesObject, ICluster>, IObjectWithFeatures {

	@Override
	default ObjectType<IObjectClusterPair> getObjectType() {
		return ObjectType.OBJECT_CLUSTER_PAIR;
	}

	default ITimeSeriesObject getObject() {
		return getFirst();
	}

	default ICluster getCluster() {
		return getSecond();
	}
}
