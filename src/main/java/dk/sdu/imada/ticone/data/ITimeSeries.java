/**
 * 
 */
package dk.sdu.imada.ticone.data;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.permute.IPermutable;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 1, 2018
 *
 */
public interface ITimeSeries extends IObjectWithFeatures, IPermutable {

	@Override
	default ObjectType<ITimeSeries> getObjectType() {
		return ObjectType.TIME_SERIES;
	}

	double[] asArray();

	ITimeSeries copy();

	int getNumberTimePoints();

	default double get(final int i) {
		return asArray()[i];
	}
}
