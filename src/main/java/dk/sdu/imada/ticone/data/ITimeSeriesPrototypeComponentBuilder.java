/**
 * 
 */
package dk.sdu.imada.ticone.data;

import dk.sdu.imada.ticone.clustering.prototype.discretize.IDiscretizeTimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder.ITimeSeriesPrototypeComponent;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder.IPrototypeComponent;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 1, 2018
 *
 */
public interface ITimeSeriesPrototypeComponentBuilder
		extends IPrototypeComponentBuilder<ITimeSeriesPrototypeComponent, ITimeSeries> {

	@Override
	ITimeSeriesPrototypeComponentBuilder copy();

	IDiscretizeTimeSeries getDiscretizeFunction();

	ITimeSeries getTimeSeries();

	ITimeSeriesPrototypeComponentBuilder setTimeSeries(ITimeSeries timeSeries);

	ITimeSeriesPrototypeComponentBuilder setTimeSeries(double[] timeSeries);

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Oct 31, 2018
	 *
	 */
	public interface ITimeSeriesPrototypeComponent extends IPrototypeComponent<ITimeSeries> {
		ITimeSeries getTimeSeries();
	}
}
