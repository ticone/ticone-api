/**
 * 
 */
package dk.sdu.imada.ticone.data.filter;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class FilterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7685449216876457073L;

	/**
	 * 
	 */
	public FilterException(final Throwable e) {
		super(e);
	}
}
