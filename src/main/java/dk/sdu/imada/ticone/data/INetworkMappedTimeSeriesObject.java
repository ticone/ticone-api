/**
 * 
 */
package dk.sdu.imada.ticone.data;

import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import it.unimi.dsi.fastutil.longs.LongSet;

public interface INetworkMappedTimeSeriesObject extends ITimeSeriesObject {
	LongSet mappedNetworks();

	boolean isMappedTo(ITiconeNetwork<?, ?> network);

	ITiconeNetworkNode getNode(ITiconeNetwork<?, ?> network);
}