package dk.sdu.imada.ticone.data.permute;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.IShuffle;

/**
 * Permutates the time series objects into a new dataset, for calculation of
 * pValues.
 * 
 * @param timeSeriesDataArrayList the arraylist of timeseries objects
 * @return the permuatated dataset as a .
 * 
 *         Created by christian on 6/8/15.
 */
public interface IShuffleDataset extends IShuffle {

	@Override
	ObjectType<ITimeSeriesObjects> supportedObjectType();

	@Override
	IShuffleDataset copy();

	@Override
	IShuffleDataset newInstance();
}
