/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Set;

import dk.sdu.imada.ticone.network.ITiconeNetwork;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 12, 2018
 *
 */
public interface ITimeSeriesObjectSet extends ITimeSeriesObjects, Set<ITimeSeriesObject> {

	@Override
	default ITimeSeriesObjectSet asSet() {
		return this;
	}

	@Override
	ITimeSeriesObjectSet copy();

	@Override
	ITimeSeriesObjectSet mapToNetwork(ITiconeNetwork<?, ?> network);

	@Override
	ITimeSeriesObjectSet removeMappingToNetwork(ITiconeNetwork<?, ?> network);
}
