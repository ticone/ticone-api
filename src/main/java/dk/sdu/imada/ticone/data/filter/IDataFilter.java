package dk.sdu.imada.ticone.data.filter;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

public interface IDataFilter {
	ITimeSeriesObjects filterObjectSets(ITimeSeriesObjects objectSets) throws FilterException, InterruptedException;
}
