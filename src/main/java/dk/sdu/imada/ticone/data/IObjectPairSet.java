/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Collection;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IPairSet;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public interface IObjectPairSet extends IPairSet<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>, IObjectPairs {
	@Override
	IObjectPairSet copy();

	@Override
	default IObjectPairSet asSet() {
		return this;
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type.equals(ObjectType.OBJECT_PAIR))
			return (Collection<T>) this;
		return IObjectPairs.super.getObjectsOfType(type);
	}
}
