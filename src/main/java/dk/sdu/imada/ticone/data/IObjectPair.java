/**
 * 
 */
package dk.sdu.imada.ticone.data;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IPair;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 5, 2018
 *
 */
public interface IObjectPair
		extends IPair<ITimeSeriesObject, ITimeSeriesObject>, IObjectWithFeatures, Comparable<IObjectPair> {

	@Override
	default ObjectType<IObjectPair> getObjectType() {
		return ObjectType.OBJECT_PAIR;
	}

	@Override
	default int compareTo(final IObjectPair o) {
		final int res = getFirst().compareTo(o.getFirst());
		if (res != 0)
			return res;
		return getSecond().compareTo(o.getSecond());
	}
}
