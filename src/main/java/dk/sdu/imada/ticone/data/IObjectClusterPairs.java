/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Arrays;
import java.util.Collection;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IPairs;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 7, 2018
 *
 */
public interface IObjectClusterPairs extends IPairs<ITimeSeriesObject, ICluster, IObjectClusterPair>, IObjectProvider {

	@Override
	default Collection<ObjectType<?>> providedTypesOfObjects() {
		return Arrays.asList(ObjectType.OBJECT_CLUSTER_PAIR);
	}

	default IObjectClusterPairs getObjectClusterPairs() {
		return this;
	}

	@Override
	IObjectClusterPairs copy();

	@Override
	IObjectClusterPairList asList();

	@Override
	IObjectClusterPairSet asSet();
}
