/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.Arrays;
import java.util.Collection;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IPairs;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public interface IObjectPairs extends IPairs<ITimeSeriesObject, ITimeSeriesObject, IObjectPair>, IObjectProvider {

	@Override
	default Collection<ObjectType<?>> providedTypesOfObjects() {
		return Arrays.asList(ObjectType.OBJECT_PAIR);
	}

	default IObjectPairs getObjectPairs() {
		return this;
	}

	@Override
	IObjectPairs copy();

	@Override
	IObjectPairList asList();

	@Override
	IObjectPairSet asSet();
}