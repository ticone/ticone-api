/**
 * 
 */
package dk.sdu.imada.ticone.data;

import java.util.List;
import java.util.Map;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public interface ITimeSeriesObject extends IObjectWithFeatures, Comparable<ITimeSeriesObject> {

	@Override
	default ObjectType<ITimeSeriesObject> getObjectType() {
		return ObjectType.OBJECT;
	}

	ITimeSeriesObject copy();

	void setAlternativeName(String alternativeName);

	String getAlternativeName();

	boolean hasAlternativeName();

	List<String> getSampleNameList();

	void addOriginalTimeSeriesToList(int sampleIdx, ITimeSeries signal, String sampleName)
			throws MultipleTimeSeriesSignalsForSameSampleParseException;

	void setOriginalTimeSeries(int sampleIdx, ITimeSeries signal, String sampleName);

	void addOriginalTimeSeriesToList(ITimeSeries signal, String sampleName)
			throws MultipleTimeSeriesSignalsForSameSampleParseException;

	ITimeSeries[] getOriginalTimeSeriesList();

	/**
	 * Update the nearest pattern and its distance
	 * 
	 * @param pattern the nearest pattern
	 * @param sim     the similarity to the given pattern
	 */
	void updateNearestPattern(ICluster pattern, ISimilarityValue sim);

	/**
	 * For multiple time series for each object, add a pattern to it's list.
	 * 
	 * @param pattern
	 */
	void addPreprocessedTimeSeries(int sampleNumber, ITimeSeries pattern);

	/**
	 * The time series of this object after applying preprocessing (e.g. fold-change
	 * or absolute values).
	 * 
	 * @return
	 */
	ITimeSeries[] getPreprocessedTimeSeriesList();

	/**
	 * Get the minimum distance from to nearest pattern
	 * 
	 * @return the distance to the nearest pattern
	 */
	ISimilarityValue getMaxSimilarity();

	/**
	 * Get the mapping to each pattern, with their distances
	 * 
	 * @return the hashmap with patterns as keys and their distances as values
	 */
	Map<IPrototype, ISimilarityValue> getMappingToPatterns();

	/**
	 * The time series data
	 * 
	 * @return the time series data in order
	 */
	ITimeSeries getOriginalTimeSeries();

	/**
	 * The name of the expression
	 * 
	 * @return the name/identifier
	 */
	String getName();

	/**
	 * Add a pattern to the mappings and set the value to the distance to given
	 * pattern
	 * 
	 * @param pattern  the pattern to add
	 * @param distance the distance to the given pattern
	 */
	void addPatternSimilarity(ICluster pattern, ISimilarityValue distance);

	/**
	 * Normalize the time series data.
	 */
	void normalize(double maxValueInDataset, double minValueInDataset);

	ITimeSeriesObjects asSingletonList();

	@Override
	default int compareTo(final ITimeSeriesObject o) {
		return getName().compareTo(o.getName());
	}

	INetworkMappedTimeSeriesObject mapToNetworkNode(ITiconeNetwork<?, ?> network, final ITiconeNetworkNode node);

	ITimeSeriesObject removeMappingToNetwork(ITiconeNetwork<?, ?> network);
}