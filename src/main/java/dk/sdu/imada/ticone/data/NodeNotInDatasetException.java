/**
 * 
 */
package dk.sdu.imada.ticone.data;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 15, 2018
 *
 */
public class NodeNotInDatasetException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8903255916214121174L;

	protected String nodeId;

	protected ITimeSeriesObjects dataset;

	public NodeNotInDatasetException(final String nodeId, final ITimeSeriesObjects dataset) {
		super();
		this.nodeId = nodeId;
		this.dataset = dataset;
	}

	/**
	 * @return the nodeId
	 */
	public String getNodeId() {
		return this.nodeId;
	}

	/**
	 * @return the dataset
	 */
	public ITimeSeriesObjects getDataset() {
		return this.dataset;
	}
}
