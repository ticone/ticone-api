/**
 * 
 */
package dk.sdu.imada.ticone.fitness;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Sep 27, 2018
 *
 */
public class FitnessScoreInitializationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8876288343698260326L;

	/**
	 * @param cause
	 */
	public FitnessScoreInitializationException(final Exception cause) {
		super(cause);
	}

}
