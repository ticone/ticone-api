/**
 * 
 */
package dk.sdu.imada.ticone.fitness;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.scale.IScaler;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 11, 2019
 *
 */
public interface IFitnessValue {
	IArithmeticFeature<? extends Comparable<?>>[] getFeatures();

	IScaler[] getFeatureScalers();

	double[] getFeatureWeights();

	double getValue();

	IFeatureValue<?>[] getFeatureValues();
}
