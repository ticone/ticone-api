/**
 * 
 */
package dk.sdu.imada.ticone.fitness;

import java.io.Serializable;
import java.util.Comparator;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.scale.IScalerBuilder;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.util.IObjectProvider;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 28, 2017
 *
 */
public interface IFitnessScore extends Comparator<IFitnessValue>, Serializable {

	String getName();

	ObjectType<?> getObjectType();

	IArithmeticFeature<? extends Comparable<?>>[] getFeatures();

	double[] getFeatureWeights();

	IFitnessValue calculateFitness(IObjectWithFeatures key) throws FitnessScoreNotInitializedException,
			FitnessCalculationException, IncompatibleFitnessScoreAndObjectException;

	IFeatureStore getFeatureValues();

	void setObjectsAndFeatureStore(IObjectProvider objects, IFeatureStore featureStore)
			throws FitnessScoreInitializationException, InterruptedException;

	/**
	 * Returns a deep copy of this fitness score, i.e. features are also copied.
	 * 
	 * @return
	 */
	IFitnessScore copy();

	void setSmallerIsBetter(boolean smallerIsBetter);

	IScalerBuilder[] getFeatureScalerBuilder();
}