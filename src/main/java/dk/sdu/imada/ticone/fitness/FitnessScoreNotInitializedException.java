/**
 * 
 */
package dk.sdu.imada.ticone.fitness;

/**
 * @author Christian Wiwie
 * 
 * @since May 8, 2017
 *
 */
public class FitnessScoreNotInitializedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 371328889587401352L;

	public FitnessScoreNotInitializedException() {
		super();
	}

	public FitnessScoreNotInitializedException(String message) {
		super(message);
	}

	public FitnessScoreNotInitializedException(Throwable cause) {
		super(cause);
	}

}
