/**
 * 
 */
package dk.sdu.imada.ticone.fitness;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 11, 2019
 *
 */
public class FitnessCalculationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8258621812769259555L;

	public FitnessCalculationException() {
		super();
	}

	public FitnessCalculationException(String message) {
		super(message);
	}

	public FitnessCalculationException(Throwable cause) {
		super(cause);
	}

}
