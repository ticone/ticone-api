/**
 * 
 */
package dk.sdu.imada.ticone.fitness;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 13, 2019
 *
 */
public class IncompatibleFitnessScoreAndObjectException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4267614003407166373L;

	public IncompatibleFitnessScoreAndObjectException() {
		super();
	}

	public IncompatibleFitnessScoreAndObjectException(String message) {
		super(message);
	}

	public IncompatibleFitnessScoreAndObjectException(Throwable cause) {
		super(cause);
	}

}
