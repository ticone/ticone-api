/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 22, 2019
 *
 */
public class IncompatibleSimilarityValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8909258152906502304L;

	public IncompatibleSimilarityValueException() {
		super();
	}

	public IncompatibleSimilarityValueException(String message) {
		super(message);
	}

	public IncompatibleSimilarityValueException(Throwable cause) {
		super(cause);
	}

}
