/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 18, 2019
 *
 */
public class IncompatibleSimilarityFunctionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5439815767672630598L;

	public IncompatibleSimilarityFunctionException() {
		super();
	}

	public IncompatibleSimilarityFunctionException(String message) {
		super(message);
	}

	public IncompatibleSimilarityFunctionException(Throwable cause) {
		super(cause);
	}

}
