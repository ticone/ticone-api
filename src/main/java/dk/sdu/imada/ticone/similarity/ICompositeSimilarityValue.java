/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.feature.scale.IScaler;

/**
 * A similarity value that, in contrast to {@link ISimpleSimilarityValue}, is
 * calculated using child values from other similarity functions.
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 8, 2018
 *
 */
public interface ICompositeSimilarityValue extends ISimilarityValue {

	ISimpleSimilarityFunction[] getChildFunctions();

	ISimpleSimilarityValue[] getChildValues();

	ISimpleSimilarityValue[] getScaledChildValues();

	ISimpleSimilarityValue getChildSimilarityValue(int i);

	ISimpleSimilarityValue getScaledChildSimilarityValue(int i);

	IScaler[] getFunctionScalers();

	Double[] getFunctionWeights();

	@Override
	ICompositeSimilarityValue divideBy(double value) throws SimilarityCalculationException;

	@Override
	default ISimilarityValue divideBy(final ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue)
			return this;
		if (value instanceof ICompositeSimilarityValue)
			return divideBy((ICompositeSimilarityValue) value);
		return divideBy(value.get());
	}

	@Override
	ICompositeSimilarityValue minus(double value) throws SimilarityCalculationException;

	@Override
	default ICompositeSimilarityValue minus(final ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue)
			return this;
		if (value instanceof ICompositeSimilarityValue)
			return minus((ICompositeSimilarityValue) value);
		return minus(value.get());
	}

	@Override
	default ICompositeSimilarityValue plus(double value) throws SimilarityCalculationException {
		return plus(value, 1);
	}

	@Override
	ICompositeSimilarityValue plus(double value, int cardinality) throws SimilarityCalculationException;

	@Override
	default ISimilarityValue plus(final ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue)
			return this;
		if (value instanceof ICompositeSimilarityValue)
			return plus((ICompositeSimilarityValue) value);
		return plus(value.get());
	}

	@Override
	ICompositeSimilarityValue times(double value) throws SimilarityCalculationException;

	@Override
	default ISimilarityValue times(final ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue)
			return this;
		if (value instanceof ICompositeSimilarityValue)
			return times((ICompositeSimilarityValue) value);
		return times(value.get());
	}

	ICompositeSimilarityValue plus(ICompositeSimilarityValue value) throws SimilarityCalculationException;

	ICompositeSimilarityValue minus(ICompositeSimilarityValue value) throws SimilarityCalculationException;

	ICompositeSimilarityValue times(ICompositeSimilarityValue value) throws SimilarityCalculationException;

	ICompositeSimilarityValue divideBy(ICompositeSimilarityValue value) throws SimilarityCalculationException;

	String toHtmlString();

}
