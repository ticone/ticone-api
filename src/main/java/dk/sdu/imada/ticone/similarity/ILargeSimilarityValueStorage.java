/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import it.unimi.dsi.fastutil.longs.LongBigList;
import it.unimi.dsi.fastutil.objects.ObjectBigList;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 27, 2019
 *
 */
public interface ILargeSimilarityValueStorage extends ISimilarityValueStorage {

	@Override
	ISimilarityValue get(long i);

	@Override
	ISimilarityValue[] get(long[] is);

	LongBigList decreasingByValue();

	LongBigList increasingByValue();

	ObjectBigList<ISimilarityValue> values();

	ILargeSimilarityValueStorage subset(LongBigList indices);

	/**
	 * @param indices
	 * @return
	 * @throws SimilarityValuesException
	 * @throws InterruptedException
	 */
	ISimilarityValue mean(LongBigList indices) throws InterruptedException, SimilarityValuesException;

	ISimilarityValue mean(long[] indices) throws InterruptedException, SimilarityValuesException;

	ISimilarityValue median(LongBigList indices) throws InterruptedException, SimilarityValuesException;

	ISimilarityValue median(long[] indices) throws InterruptedException, SimilarityValuesException;

	/**
	 * @param indices
	 * @return
	 */
	ISimilarityValue sum(LongBigList indices) throws InterruptedException, SimilarityValuesException;

	ISimilarityValue sum(long[] indices) throws InterruptedException, SimilarityValuesException;

	/**
	 * Subsets this storage with a small number of indices fitting into an array,
	 * generating a {@link ISmallSimilarityValueStorage}.
	 * 
	 * @param indices
	 * @return
	 */
	ISmallSimilarityValueStorage subset(long[] indices);
}
