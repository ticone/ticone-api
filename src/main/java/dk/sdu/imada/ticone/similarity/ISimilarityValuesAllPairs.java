/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.IPairsFactory;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.ObjectCollection;

/**
 * A data structure containing all pairwise similarities of two lists of objects
 * {@link #getObjects1()} and {@link #getObjects2()}.
 * 
 * To reduce the memory footprint, not all object pairs are stored, but rather
 * the two lists of objects of which pairs are dynamically created if needed.
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 14, 2018
 *
 */
public interface ISimilarityValuesAllPairs<O1, O2, P extends IPair<O1, O2>> extends ISimilarityValues<O1, O2, P> {

	O1[] getObjects1();

	O2[] getObjects2();

	O1 getObject1(int x);

	O2 getObject2(int y);

	int getObject1Index(O1 object1);

	int getObject2Index(O2 object2);

	/**
	 * @param firstObjectIds
	 * @param secondObjectIds
	 * @return The indices of the second object to which the respective first object
	 *         is most similar to; -1 if none of the object's similarity to any of
	 *         the objects can be calculated.
	 * @throws SimilarityValuesException
	 * @throws NoComparableSimilarityValuesException
	 * @throws InterruptedException
	 */
	int[] whichMaxForFirstObject(int[] firstObjectIds, int... secondObjectIds)
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	int whichMaxForSecondObject(int objectId, int... firstObjectIds)
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	ISimilarityValue maxForFirstObject(int objectId, int... secondObjectIds)
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	ISimilarityValue maxForSecondObject(int objectId, int... firstObjectIds)
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	@Override
	Int2ObjectMap<ISimilarityValuesAllPairs<O1, O2, P>> partitionByFirstObject()
			throws SimilarityValuesException, InterruptedException;

	@Override
	Int2ObjectMap<ISimilarityValuesAllPairs<O1, O2, P>> partitionBySecondObject()
			throws SimilarityValuesException, InterruptedException;

	int[] firstObjectIndices();

	int[] secondObjectIndices();

	ISimilarityValuesAllPairs<O1, O2, P> subset(int[] firstObjects, int[] secondObjects)
			throws SimilarityValuesException, InterruptedException;

	ISimilarityValue sum(int[] firstObjects, int[] secondObjects)
			throws SimilarityValuesException, InterruptedException;

	ISimilarityValue set(int x, int y, ISimilarityValue value)
			throws SimilarityValuesException, IncompatibleSimilarityValueException;

	ISimilarityValue get(int x, int y) throws SimilarityValuesException;

	ISimilarityValue[] get(int[] xs, int[] ys) throws SimilarityValuesException;

	ISimilarityValue[] getForAllPairs(int[] xs, int[] ys) throws SimilarityValuesException;

	P getPair(int[] xy) throws SimilarityValuesException, InterruptedException;

	boolean isMissing(int x, int y) throws SimilarityValuesException;

	boolean isSet(int x, int y) throws SimilarityValuesException;

	/**
	 * Returns indices sorted increasingly by similarity value.
	 * 
	 * @return
	 * @throws SimilarityValuesException
	 */
	ObjectCollection<int[]> increasingByValue() throws SimilarityValuesException;

	ObjectCollection<int[]> decreasingByValue() throws SimilarityValuesException;

	Iterable<int[]> pairIndices();

	IPairsFactory<O1, O2, P> getPairsFactory();

}
