/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import java.util.Collection;
import java.util.Map;

import dk.sdu.imada.ticone.util.IPair;

/**
 * A data structure containing some pairwise similarities of two (unknown) lists
 * of objects.
 * 
 * The involved pairs are always stored in memory.
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 14, 2018
 *
 */
public interface ISimilarityValuesSomePairs<O1, O2, P extends IPair<O1, O2>> extends ISimilarityValues<O1, O2, P> {

	Map<O1, P> whichMaxByFirstObject()
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	Map<O2, P> whichMaxBySecondObject()
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	Map<O1, ISimilarityValue> maxByFirstObject()
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	Map<O2, ISimilarityValue> maxBySecondObject()
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	@Override
	Map<O1, ? extends ISimilarityValuesSomePairs<O1, O2, P>> partitionByFirstObject()
			throws SimilarityValuesException, InterruptedException;

	@Override
	Map<O2, ? extends ISimilarityValuesSomePairs<O1, O2, P>> partitionBySecondObject()
			throws SimilarityValuesException, InterruptedException;

	Collection<? extends Number> pairIndices();

	/**
	 * Returns indices sorted increasingly by similarity value.
	 * 
	 * @return
	 * @throws SimilarityValuesException
	 */
	Collection<? extends Number> increasingByValue() throws SimilarityValuesException, InterruptedException;

	Collection<? extends Number> decreasingByValue() throws SimilarityValuesException, InterruptedException;

}
