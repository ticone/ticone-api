/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Sep 20, 2018
 *
 */
public class SimilarityInputNotCompatibleException extends SimilarityCalculationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3792905052456149130L;

	/**
	 * @param string
	 */
	public SimilarityInputNotCompatibleException(final String string) {
		super(string);
	}

	/**
	 * 
	 */
	public SimilarityInputNotCompatibleException() {
		super();
	}
}
