/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 14, 2018
 *
 */
public class TooFewSimilarityValuesAggregateCalculationException extends SimilarityAggregateCalculationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 632382407236731900L;

	/**
	 * @param cause
	 */
	public TooFewSimilarityValuesAggregateCalculationException(final Throwable cause) {
		super(cause);
	}

	/**
	 * @param string
	 */
	public TooFewSimilarityValuesAggregateCalculationException(final String string) {
		super(string);
	}

	/**
	 * 
	 */
	public TooFewSimilarityValuesAggregateCalculationException() {
		super();
	}
}
