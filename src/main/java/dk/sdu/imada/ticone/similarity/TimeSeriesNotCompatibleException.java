/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class TimeSeriesNotCompatibleException extends SimilarityInputNotCompatibleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7020281124135581404L;

	/**
	 * @param string
	 */
	public TimeSeriesNotCompatibleException(final String string) {
		super(string);
	}

	/**
	 * 
	 */
	public TimeSeriesNotCompatibleException() {
		super();
	}

}
