/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import java.util.Iterator;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 16, 2018
 *
 * @param <SIM>
 */
public interface ISimilarityValueStorage {

	ISimilarityValueStorage copy();

	ObjectType<?> getObjectType();

	ISimilarityValue sum() throws InterruptedException, SimilarityValuesException;

	ISimilarityValue mean() throws InterruptedException, SimilarityValuesException;

	ISimilarityValue median() throws InterruptedException, SimilarityValuesException;

	boolean isMissing(long i);

	boolean[] isMissing(long[] is);

	boolean isMissing(int i);

	boolean[] isMissing(int[] is);

	boolean isSet(long i);

	boolean[] isSet(long[] is);

	boolean isSet(int i);

	boolean[] isSet(int[] is);

	ISimilarityValue get(long i) throws IncompatibleSimilarityValueStorageException;

	ISimilarityValue[] get(long[] is) throws IncompatibleSimilarityValueStorageException;

	ISimilarityValue get(int i);

	ISimilarityValue[] get(int[] is);

	ISimilarityValue set(long i, ISimilarityValue value)
			throws SimilarityValuesException, IncompatibleSimilarityValueException;

	ISimilarityValue[] set(long[] is, ISimilarityValue[] values)
			throws SimilarityValuesException, IncompatibleSimilarityValueException;

	ISimilarityValue set(int i, ISimilarityValue value)
			throws SimilarityValuesException, IncompatibleSimilarityValueException;

	ISimilarityValue[] set(int[] is, ISimilarityValue[] values)
			throws SimilarityValuesException, IncompatibleSimilarityValueException;

	long size();

	/**
	 * @return
	 */
	Iterator<ISimilarityValue> iterator();

}