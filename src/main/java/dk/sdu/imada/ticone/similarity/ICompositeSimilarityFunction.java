/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.util.IPair;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 21, 2018
 *
 */
public interface ICompositeSimilarityFunction extends ISimilarityFunction {

	public static enum MISSING_CHILD_HANDLING {
		IGNORE_CHILD, RETURN_MISSING, TREAT_AS_ZERO;
	}

	@Override
	<O extends IObjectWithFeatures & IPair<?, ?>> ICompositeSimilarityValue calculateSimilarity(O object)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException;

	@Override
	ICompositeSimilarityValue missingValuePlaceholder();

	@Override
	ICompositeSimilarityValue maxValue();

	@Override
	ICompositeSimilarityValue minValue();

	ISimpleSimilarityFunction[] getSimilarityFunctions();

	IScaler getScalerForObjectClusterPair(int i);

	IScaler getScalerForObjectPair(int i);

	IScaler getScalerForClusterPair(int i);

	void setScalerForObjectClusterPair(int i, IScaler scaler);

	void setScalerForObjectPair(int i, IScaler scaler);

	void setScalerForClusterPair(int i, IScaler scaler);

	void setScalerForAnyPair(int i, IScaler scaler);

	default ICompositeSimilarityValue value(final ISimpleSimilarityValue[] values, final ObjectType<?> ofType) {
		return value(values, ofType, 1);
	}

	ICompositeSimilarityValue value(final ISimpleSimilarityValue[] values, final ObjectType<?> ofType, int cardinality);

	@Override
	default boolean initialize() throws InterruptedException {
		boolean result = true;
		for (ISimpleSimilarityFunction sf : getSimilarityFunctions())
			result &= sf.initialize();
		return result;
	}

	void setMissingChildHandling(final MISSING_CHILD_HANDLING missingChildHandling);
}
