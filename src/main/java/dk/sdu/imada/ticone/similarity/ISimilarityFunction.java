/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;

import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.IPairsFactory;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 19, 2018
 *
 */
public interface ISimilarityFunction extends IFeatureValueProvider, Comparator<ISimilarityValue>, Serializable {
	/**
	 * This method has to be invoked by the caller prior to any similarity
	 * calculations.
	 * 
	 * Long running initialization required by a similarity function should be
	 * performed in this method.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	default boolean initialize() throws InterruptedException {
		return true;
	}

	@Override
	ISimilarityFunction copy();

	/**
	 * A value larger zero means: Value s1 corresponds to a higher similarity than
	 * value s2. A value smaller zero means: Value s1 corresponds to a smaller
	 * similarity than value s2. A value of zero means: Values s1 and s2 correspond
	 * to the same level of similarity.
	 */
	@Override
	int compare(ISimilarityValue s1, ISimilarityValue s2);

	ISimilarityValue missingValuePlaceholder();

	ISimilarityValue maxValue();

	ISimilarityValue minValue();

	<O1, O2, P extends IObjectWithFeatures & IPair<O1, O2>> ISimilarityValuesSomePairs<O1, O2, P> emptySimilarities(
			Collection<P> objectPairs, ObjectType<P> ofType);

	<O1, O2, P extends IObjectWithFeatures & IPair<O1, O2>> ISimilarityValuesAllPairs<O1, O2, P> emptySimilarities(
			O1[] objects1, O2[] objects2, IPairsFactory<O1, O2, P> factory, ObjectType<P> ofType);

	ISimilarityValueStorage newSimilarityStorage(long size, ObjectType<? extends IObjectWithFeatures> ofType);

	/**
	 * 
	 * @param object
	 * @return A similarity value between 0.0 and 1.0.
	 * @throws IncompatibleObjectTypeException
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 */
	<O extends IObjectWithFeatures & IPair<?, ?>> ISimilarityValue calculateSimilarity(O object)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException;

	/**
	 * Calculate all similarities of the specified pairs of objects.
	 * 
	 * @param objects1
	 * @param objects2
	 * @return
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 * @throws IncompatibleSimilarityValueException
	 */
	<O1 extends IObjectWithFeatures, O2 extends IObjectWithFeatures, P extends IObjectWithFeatures & IPair<O1, O2>> ISimilarityValuesSomePairs<O1, O2, P> calculateSimilarities(
			Collection<P> pairs, ObjectType<P> type)
			throws SimilarityCalculationException, InterruptedException, IncompatibleSimilarityValueException;

	<O1 extends IObjectWithFeatures, O2 extends IObjectWithFeatures, P extends IObjectWithFeatures & IPair<O1, O2>> void calculateSimilarities(
			int[] objects1, int[] objects2, ISimilarityValuesAllPairs<O1, O2, P> similarityValues)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleSimilarityValueException;

	/**
	 * Calculate all similarities of pairs of objects of two sets.
	 * 
	 * @return
	 * @throws SimilarityCalculationException
	 * @throws InterruptedException
	 * @throws SimilarityValuesException
	 * @throws IncompatibleSimilarityValueException
	 */
	<O1 extends IObjectWithFeatures, O2 extends IObjectWithFeatures, P extends IObjectWithFeatures & IPair<O1, O2>> ISimilarityValuesAllPairs<O1, O2, P> calculateSimilarities(
			O1[] objects1, O2[] objects2, final IPairsFactory<O1, O2, P> factory, ObjectType<P> type)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			IncompatibleSimilarityValueException;

	default boolean isDefinedFor(IObjectWithFeatures object) throws InterruptedException {
		return true;
	}
}
