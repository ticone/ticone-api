/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.util.LazyValueCalculationException;

/**
 * Indicates that the calculation of a similarity value could not be completed
 * successfully.
 * 
 * @author Christian Wiwie
 * 
 * @since Sep 27, 2018
 *
 */
public class SimilarityCalculationException extends LazyValueCalculationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3975191244873312015L;

	/**
	 * @param cause
	 */
	public SimilarityCalculationException(final Throwable cause) {
		super(cause);
	}

	/**
	 * @param string
	 */
	public SimilarityCalculationException(final String string) {
		super(string);
	}

	/**
	 * 
	 */
	public SimilarityCalculationException() {
		super();
	}
}
