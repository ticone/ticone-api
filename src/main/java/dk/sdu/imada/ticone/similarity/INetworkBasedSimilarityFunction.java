
package dk.sdu.imada.ticone.similarity;

import java.util.Arrays;
import java.util.Collection;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.network.INetworkLocationPrototypeComponentBuilder.INetworkLocationPrototypeComponent;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.Pair;

public interface INetworkBasedSimilarityFunction
		extends ISimpleSimilarityFunction<Collection<? extends INetworkMappedTimeSeriesObject>> {

	ITiconeNetwork<?, ?> getNetwork();

	void setNetwork(ITiconeNetwork<?, ?> network);

	@Override
	default ISimilarityAspectExtractor<Collection<? extends INetworkMappedTimeSeriesObject>> getAspectExtractor(
			ObjectType<?> type) throws IncompatibleObjectTypeException {
		if (type == ObjectType.CLUSTER_PAIR)
			return new ClusterPairNetworkLocationExtractor(getNetwork());
		if (type == ObjectType.OBJECT_PAIR)
			return new ObjectPairNetworkLocationExtractor(getNetwork());
		if (type == ObjectType.OBJECT_CLUSTER_PAIR)
			return new ObjectClusterPairNetworkLocationExtractor(getNetwork());
		throw new IncompatibleObjectTypeException();
	}
}

class ClusterPairNetworkLocationExtractor
		implements ISimilarityAspectExtractor<Collection<? extends INetworkMappedTimeSeriesObject>> {

	private ITiconeNetwork<?, ?> network;

	public ClusterPairNetworkLocationExtractor(final ITiconeNetwork<?, ?> network) {
		this.network = network;
	}

	@Override
	public IPair<Collection<? extends INetworkMappedTimeSeriesObject>[], Collection<? extends INetworkMappedTimeSeriesObject>[]> extract(
			IObjectWithFeatures object) throws SimilarityAspectExtractException, IncompatibleObjectTypeException {
		if (object == null)
			throw new SimilarityAspectExtractException();

		if (object.getObjectType() != ObjectType.CLUSTER_PAIR)
			throw new IncompatibleObjectTypeException();

		final IClusterPair pair = ObjectType.CLUSTER_PAIR.getBaseType().cast(object);

		if (pair.getFirst() == null || pair.getSecond() == null)
			throw new SimilarityAspectExtractException();

		INetworkLocationPrototypeComponent p1, p2;
		try {
			p1 = PrototypeComponentType.NETWORK_LOCATION.getComponent(pair.getFirst().getPrototype());
			p2 = PrototypeComponentType.NETWORK_LOCATION.getComponent(pair.getSecond().getPrototype());

			return Pair.getPair(new Collection[] { p1.getNetworkLocation() },
					new Collection[] { p2.getNetworkLocation() });
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			throw new SimilarityAspectExtractException(e);
		}
	}
}

class ObjectClusterPairNetworkLocationExtractor
		implements ISimilarityAspectExtractor<Collection<? extends INetworkMappedTimeSeriesObject>> {

	private ITiconeNetwork<?, ?> network;

	public ObjectClusterPairNetworkLocationExtractor(final ITiconeNetwork<?, ?> network) {
		this.network = network;
	}

	@Override
	public IPair<Collection<? extends INetworkMappedTimeSeriesObject>[], Collection<? extends INetworkMappedTimeSeriesObject>[]> extract(
			IObjectWithFeatures pair) throws SimilarityAspectExtractException, IncompatibleObjectTypeException {
		if (pair == null)
			throw new SimilarityAspectExtractException();

		if (pair.getObjectType() != ObjectType.OBJECT_CLUSTER_PAIR)
			throw new IncompatibleObjectTypeException();

		final IObjectClusterPair cast = ObjectType.OBJECT_CLUSTER_PAIR.getBaseType().cast(pair);

		if (cast.getFirst() == null || cast.getSecond() == null)
			throw new SimilarityAspectExtractException();

		INetworkLocationPrototypeComponent p2;
		try {
			INetworkMappedTimeSeriesObject objectLocation;
			ITimeSeriesObject object = cast.getObject();
			if (object instanceof INetworkMappedTimeSeriesObject)
				objectLocation = ((INetworkMappedTimeSeriesObject) object);
			else
				objectLocation = object.mapToNetworkNode(network, network.getNode(object.getName()));

			p2 = PrototypeComponentType.NETWORK_LOCATION.getComponent(cast.getSecond().getPrototype());
			return Pair.getPair(new Collection[] { Arrays.asList(objectLocation) },
					new Collection[] { p2.getNetworkLocation() });
		} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
			throw new SimilarityAspectExtractException(e);
		}

	}
}

class ObjectPairNetworkLocationExtractor
		implements ISimilarityAspectExtractor<Collection<? extends INetworkMappedTimeSeriesObject>> {

	private ITiconeNetwork<?, ?> network;

	ObjectPairNetworkLocationExtractor(final ITiconeNetwork<?, ?> network) {
		this.network = network;
	}

	@Override
	public IPair<Collection<? extends INetworkMappedTimeSeriesObject>[], Collection<? extends INetworkMappedTimeSeriesObject>[]> extract(
			IObjectWithFeatures pair) throws SimilarityAspectExtractException, IncompatibleObjectTypeException {
		if (pair == null)
			throw new SimilarityAspectExtractException();

		if (pair.getObjectType() != ObjectType.OBJECT_PAIR)
			throw new IncompatibleObjectTypeException();

		final IObjectPair cast = ObjectType.OBJECT_PAIR.getBaseType().cast(pair);

		if (cast.getFirst() == null || cast.getSecond() == null)
			throw new SimilarityAspectExtractException();

		INetworkMappedTimeSeriesObject l1, l2;
		ITimeSeriesObject o1 = cast.getFirst();
		if (o1 instanceof INetworkMappedTimeSeriesObject)
			l1 = ((INetworkMappedTimeSeriesObject) o1);
		else
			l1 = o1.mapToNetworkNode(network, network.getNode(o1.getName()));
		ITimeSeriesObject o2 = cast.getSecond();
		if (o2 instanceof INetworkMappedTimeSeriesObject)
			l2 = ((INetworkMappedTimeSeriesObject) o2);
		else
			l2 = o2.mapToNetworkNode(network, network.getNode(o2.getName()));

		return Pair.getPair(new Collection[] { Arrays.asList(l1) }, new Collection[] { Arrays.asList(l2) });
	}
}
