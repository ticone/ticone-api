/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.feature.scale.IScaler;
import dk.sdu.imada.ticone.util.IPair;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 15, 2018
 *
 */
public interface ISimpleSimilarityFunction<A> extends ISimilarityFunction {

	default ISimpleSimilarityValue value(double value, ObjectType<?> ofType) {
		return this.value(value, ofType, 1);
	}

	/**
	 * 
	 * @param value A double value to be wrapped.
	 * @return A wrapper object for the passed double value.
	 */
	ISimpleSimilarityValue value(double value, ObjectType<?> ofType, int cardinality);

	@Override
	<O extends IObjectWithFeatures & IPair<?, ?>> ISimpleSimilarityValue calculateSimilarity(O object)
			throws IncompatibleObjectTypeException, SimilarityCalculationException, InterruptedException;

	@Override
	ISimpleSimilarityValue missingValuePlaceholder();

	@Override
	ISimpleSimilarityValue maxValue();

	@Override
	ISimpleSimilarityValue minValue();

	ISimilarityAspectExtractor<A> getAspectExtractor(ObjectType<?> type) throws IncompatibleObjectTypeException;

	IScaler getScalerForClusterPair();

	IScaler getScalerForObjectClusterPair();

	IScaler getScalerForObjectPair();

	void setScalerForAnyPair(IScaler scaler);

	void setScalerForClusterPair(IScaler scaler);

	void setScalerForObjectClusterPair(IScaler scaler);

	void setScalerForObjectPair(IScaler scaler);
}
