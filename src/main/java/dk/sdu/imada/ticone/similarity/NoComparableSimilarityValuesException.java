/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 30, 2018
 *
 */
public class NoComparableSimilarityValuesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7841187393145883576L;

	public NoComparableSimilarityValuesException() {
		super();
	}

	public NoComparableSimilarityValuesException(final String message) {
		super(message);
	}

	public NoComparableSimilarityValuesException(final Throwable cause) {
		super(cause);
	}

}
