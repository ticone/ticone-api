
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.clustering.pair.IClusterPair;
import dk.sdu.imada.ticone.data.IObjectClusterPair;
import dk.sdu.imada.ticone.data.IObjectPair;
import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.data.ITimeSeriesPrototypeComponentBuilder.ITimeSeriesPrototypeComponent;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeComponentType;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.Pair;

public interface ITimeSeriesSimilarityFunction extends ISimpleSimilarityFunction<ITimeSeries> {

	public final static ISimilarityAspectExtractor<ITimeSeries> CLUSTER_PAIR_TIME_SERIES_EXTRACTOR = new ISimilarityAspectExtractor<ITimeSeries>() {

		@Override
		public IPair<ITimeSeries[], ITimeSeries[]> extract(IObjectWithFeatures object)
				throws SimilarityAspectExtractException, IncompatibleObjectTypeException {
			if (object == null)
				throw new SimilarityAspectExtractException();

			if (object.getObjectType() != ObjectType.CLUSTER_PAIR)
				throw new IncompatibleObjectTypeException();

			final IClusterPair pair = ObjectType.CLUSTER_PAIR.getBaseType().cast(object);

			if (pair.getFirst() == null || pair.getSecond() == null)
				throw new SimilarityAspectExtractException();

			ITimeSeriesPrototypeComponent p1;
			ITimeSeriesPrototypeComponent p2;
			try {
				p1 = PrototypeComponentType.TIME_SERIES.getComponent(pair.getFirst().getPrototype());
				p2 = PrototypeComponentType.TIME_SERIES.getComponent(pair.getSecond().getPrototype());

				return Pair.getPair(new ITimeSeries[] { p1.getTimeSeries() }, new ITimeSeries[] { p2.getTimeSeries() });
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				throw new SimilarityAspectExtractException(e);
			}
		}
	};

	public final static ISimilarityAspectExtractor<ITimeSeries> OBJECT_PAIR_TIME_SERIES_EXTRACTOR = new ISimilarityAspectExtractor<ITimeSeries>() {

		@Override
		public IPair<ITimeSeries[], ITimeSeries[]> extract(IObjectWithFeatures object)
				throws SimilarityAspectExtractException, IncompatibleObjectTypeException {
			if (object == null)
				throw new SimilarityAspectExtractException();

			if (object.getObjectType() != ObjectType.OBJECT_PAIR)
				throw new IncompatibleObjectTypeException();

			final IObjectPair pair = ObjectType.OBJECT_PAIR.getBaseType().cast(object);

			if (pair.getFirst() == null || pair.getSecond() == null)
				throw new SimilarityAspectExtractException();

			return Pair.getPair(pair.getFirst().getPreprocessedTimeSeriesList(),
					pair.getSecond().getPreprocessedTimeSeriesList());
		}
	};

	public final static ISimilarityAspectExtractor<ITimeSeries> OBJECT_CLUSTER_PAIR_TIME_SERIES_EXTRACTOR = new ISimilarityAspectExtractor<ITimeSeries>() {

		@Override
		public IPair<ITimeSeries[], ITimeSeries[]> extract(IObjectWithFeatures object)
				throws SimilarityAspectExtractException, IncompatibleObjectTypeException {
			if (object == null)
				throw new SimilarityAspectExtractException();

			if (object.getObjectType() != ObjectType.OBJECT_CLUSTER_PAIR)
				throw new IncompatibleObjectTypeException();

			final IObjectClusterPair pair = ObjectType.OBJECT_CLUSTER_PAIR.getBaseType().cast(object);

			if (pair.getFirst() == null || pair.getSecond() == null)
				throw new SimilarityAspectExtractException();

			ITimeSeriesPrototypeComponent p2;
			try {
				p2 = PrototypeComponentType.TIME_SERIES.getComponent(pair.getSecond().getPrototype());
				return Pair.getPair(pair.getFirst().getPreprocessedTimeSeriesList(),
						new ITimeSeries[] { p2.getTimeSeries() });
			} catch (IncompatiblePrototypeComponentException | MissingPrototypeException e) {
				throw new SimilarityAspectExtractException(e);
			}

		}
	};

	boolean isNormalizeByNumberTimepoints();

	void setNormalizeByNumberTimepoints(boolean normalizeByNumberTimepoints);

	ITimeSeriesPreprocessor getTimeSeriesPreprocessor();

	void setTimeSeriesPreprocessor(ITimeSeriesPreprocessor preprocessor);

	@Override
	default ISimilarityAspectExtractor<ITimeSeries> getAspectExtractor(ObjectType<?> type)
			throws IncompatibleObjectTypeException {
		if (type == ObjectType.CLUSTER_PAIR)
			return (ISimilarityAspectExtractor<ITimeSeries>) CLUSTER_PAIR_TIME_SERIES_EXTRACTOR;
		if (type == ObjectType.OBJECT_PAIR)
			return (ISimilarityAspectExtractor<ITimeSeries>) OBJECT_PAIR_TIME_SERIES_EXTRACTOR;
		if (type == ObjectType.OBJECT_CLUSTER_PAIR)
			return (ISimilarityAspectExtractor<ITimeSeries>) OBJECT_CLUSTER_PAIR_TIME_SERIES_EXTRACTOR;
		throw new IncompatibleObjectTypeException();
	}
}
