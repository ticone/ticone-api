/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 21, 2019
 *
 */
public class SimilarityAspectExtractException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6146121353220012036L;

	public SimilarityAspectExtractException() {
		super();
	}

	public SimilarityAspectExtractException(String message) {
		super(message);
	}

	public SimilarityAspectExtractException(Throwable cause) {
		super(cause);
	}

}
