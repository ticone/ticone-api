package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.util.ITimePointWeighting;

public interface IWeightedTimeSeriesSimilarityFunction extends ITimeSeriesSimilarityFunction {

	default double getTimePointWeight(final int tp) {
		final ITimePointWeighting tpWeights = getTimePointWeights();
		if (tpWeights == null)
			return 1;
		return tpWeights.getWeightForTimePointColumn(tp);
	}

	ITimePointWeighting getTimePointWeights();

	void setTimePointWeights(ITimePointWeighting tpWeights);

	@Override
	IWeightedTimeSeriesSimilarityFunction copy();
}
