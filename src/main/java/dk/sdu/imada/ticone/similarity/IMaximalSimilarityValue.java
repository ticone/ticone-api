/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 3, 2018
 *
 */
public interface IMaximalSimilarityValue extends ISimilarityValue {

	@Override
	default int compareTo(final ISimilarityValue o) {
		if (o instanceof IMaximalSimilarityValue)
			return 0;
		return 1;
	}

	@Override
	default ISimilarityValue divideBy(final double value) throws SimilarityCalculationException {
		return this;
	}

	@Override
	default ISimilarityValue divideBy(final ISimilarityValue value) throws SimilarityCalculationException {
		return this;
	}

	@Override
	default ISimilarityValue minus(final double value) throws SimilarityCalculationException {
		return this;
	}

	@Override
	default ISimilarityValue minus(final ISimilarityValue value) throws SimilarityCalculationException {
		return this;
	}

	@Override
	default ISimilarityValue plus(final double value, final int cardinality) throws SimilarityCalculationException {
		return this;
	}

	@Override
	default ISimilarityValue plus(final ISimilarityValue value) throws SimilarityCalculationException {
		return this;
	}

	@Override
	default ISimilarityValue times(final double value) throws SimilarityCalculationException {
		return this;
	}

	@Override
	default ISimilarityValue times(final ISimilarityValue value) throws SimilarityCalculationException {
		return this;
	}

}
