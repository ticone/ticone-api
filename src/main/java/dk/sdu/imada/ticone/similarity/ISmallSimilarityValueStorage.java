/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 27, 2019
 *
 */
public interface ISmallSimilarityValueStorage extends ISimilarityValueStorage {

	int[] decreasingByValue();

	int[] increasingByValue();

	ISimilarityValue[] values();

	ISmallSimilarityValueStorage subset(int[] indices);

	ISimilarityValue mean(int[] indices) throws InterruptedException, SimilarityValuesException;

	ISimilarityValue median(int[] indices) throws InterruptedException, SimilarityValuesException;

	ISimilarityValue sum(int[] indices) throws InterruptedException, SimilarityValuesException;

}
