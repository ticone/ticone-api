package dk.sdu.imada.ticone.similarity;

/**
 * 
 * A similarity value that, in contrast to {@link ICompositeSimilarityValue},
 * wraps a double and is not calculated using other similarity values.
 * 
 * @author Christian Wiwie
 * 
 * @since Apr 12, 2019
 *
 */
public interface ISimpleSimilarityValue extends ISimilarityValue {

	@Override
	default ISimpleSimilarityValue plus(double value) throws SimilarityCalculationException {
		return plus(value, 1);
	}

	@Override
	ISimpleSimilarityValue plus(double value, int cardinality) throws SimilarityCalculationException;

	@Override
	ISimpleSimilarityValue plus(ISimilarityValue value) throws SimilarityCalculationException;

	@Override
	ISimpleSimilarityValue minus(double value) throws SimilarityCalculationException;

	@Override
	ISimpleSimilarityValue minus(ISimilarityValue value) throws SimilarityCalculationException;

	@Override
	ISimpleSimilarityValue times(double value) throws SimilarityCalculationException;

	@Override
	ISimpleSimilarityValue times(ISimilarityValue value) throws SimilarityCalculationException;

	@Override
	ISimpleSimilarityValue divideBy(double value) throws SimilarityCalculationException;

	@Override
	ISimpleSimilarityValue divideBy(ISimilarityValue value) throws SimilarityCalculationException;

}