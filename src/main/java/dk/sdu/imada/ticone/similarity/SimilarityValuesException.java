/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * Indicates that an operation on similarity values, such as element retrieval,
 * did not complete successfully.
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 19, 2018
 *
 */
public class SimilarityValuesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8632436443284025334L;

	public SimilarityValuesException() {
		super();
	}

	public SimilarityValuesException(final String message) {
		super(message);
	}

	public SimilarityValuesException(final Throwable cause) {
		super(cause);
	}

}
