/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 15, 2018
 *
 */
public interface IMissingSimilarityValue extends ISimilarityValue {

	@Override
	default int compareTo(ISimilarityValue o) {
		if (o instanceof IMinimalSimilarityValue)
			return -1;
		else if (o instanceof IMissingSimilarityValue)
			return 0;
		return -1;
	}

	@Override
	default ISimilarityValue plus(ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue)
			return this;
		return value;
	}
}
