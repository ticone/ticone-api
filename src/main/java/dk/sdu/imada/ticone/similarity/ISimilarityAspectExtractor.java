/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IPair;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 21, 2019
 *
 */
@FunctionalInterface
public interface ISimilarityAspectExtractor<A> {
	IPair<A[], A[]> extract(IObjectWithFeatures pair)
			throws SimilarityAspectExtractException, IncompatibleObjectTypeException;
}
