/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 21, 2019
 *
 */
public class IncompatibleObjectTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -316865552888309912L;

	public IncompatibleObjectTypeException() {
		super();
	}

	public IncompatibleObjectTypeException(String message) {
		super(message);
	}

	public IncompatibleObjectTypeException(Throwable cause) {
		super(cause);
	}

}
