/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 14, 2018
 *
 */
public class SimilarityAggregateCalculationException extends SimilarityCalculationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1773612584292375679L;

	/**
	 * @param cause
	 */
	public SimilarityAggregateCalculationException(final Throwable cause) {
		super(cause);
	}

	/**
	 * @param string
	 */
	public SimilarityAggregateCalculationException(final String string) {
		super(string);
	}

	/**
	 * 
	 */
	public SimilarityAggregateCalculationException() {
		super();
	}
}
