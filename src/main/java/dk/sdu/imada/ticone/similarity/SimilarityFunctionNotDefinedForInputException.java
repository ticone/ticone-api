/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 5, 2019
 *
 */
public class SimilarityFunctionNotDefinedForInputException extends SimilarityCalculationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4485018676488541565L;

	public SimilarityFunctionNotDefinedForInputException() {
		super();
	}

	public SimilarityFunctionNotDefinedForInputException(String message) {
		super(message);
	}

	public SimilarityFunctionNotDefinedForInputException(Throwable cause) {
		super(cause);
	}

}
