package dk.sdu.imada.ticone.similarity;

import java.io.Serializable;

import dk.sdu.imada.ticone.util.IConvertibleToNumber;
import dk.sdu.imada.ticone.util.ToNumberConversionException;

public interface ISimilarityValue extends IConvertibleToNumber<Double>, Serializable, Comparable<ISimilarityValue> {

	@Override
	default Double toNumber() throws ToNumberConversionException {
		try {
			return get();
		} catch (SimilarityCalculationException e) {
			throw new ToNumberConversionException(e);
		}
	}

	ISimilarityValue calculate() throws SimilarityCalculationException;

	double get() throws SimilarityCalculationException;

	boolean isCalculated();

	/**
	 * 
	 * Returns a new instance representing the difference of both values.
	 * 
	 * @param value
	 * @return
	 * @throws SimilarityCalculationException
	 */
	default ISimilarityValue minus(final ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue || value instanceof IMaximalSimilarityValue
				|| value instanceof IMinimalSimilarityValue)
			return this;
		return minus(value.get());
	}

	/**
	 * Returns a new instance representing the sum of both values.
	 * 
	 * @param value
	 * @return
	 * @throws SimilarityCalculationException
	 */
	default ISimilarityValue plus(final ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue || value instanceof IMaximalSimilarityValue
				|| value instanceof IMinimalSimilarityValue)
			return this;
		final ISimilarityValue plus = plus(value.get(), value.cardinality());
		return plus;
	}

	/**
	 * 
	 * Returns a new instance representing the difference of both values.
	 * 
	 * @param value
	 * @return
	 * @throws SimilarityCalculationException
	 */
	ISimilarityValue minus(double value) throws SimilarityCalculationException;

	default ISimilarityValue plus(double value) throws SimilarityCalculationException {
		return plus(value, 1);
	}

	/**
	 * 
	 * Returns a new instance representing the sum of both values.
	 * 
	 * @param value
	 * @return
	 * @throws SimilarityCalculationException
	 */
	ISimilarityValue plus(double value, int cardinality) throws SimilarityCalculationException;

	/**
	 * Returns a new instance representing the product of both values.
	 * 
	 * @param value
	 * @return
	 * @throws SimilarityCalculationException
	 */
	default ISimilarityValue times(final ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue)
			return this;
		return times(value.get());
	}

	/**
	 * 
	 * Returns a new instance representing the product of both values.
	 * 
	 * @param value
	 * @return
	 * @throws SimilarityCalculationException
	 */
	ISimilarityValue times(double value) throws SimilarityCalculationException;

	/**
	 * Returns a new instance representing the division of both values.
	 * 
	 * @param value
	 * @return
	 * @throws SimilarityCalculationException
	 */
	default ISimilarityValue divideBy(final ISimilarityValue value) throws SimilarityCalculationException {
		if (value instanceof IMissingSimilarityValue)
			return this;
		return divideBy(value.get());
	}

	/**
	 * 
	 * Returns a new instance representing the division of both values.
	 * 
	 * @param value
	 * @return
	 * @throws SimilarityCalculationException
	 */
	ISimilarityValue divideBy(double value) throws SimilarityCalculationException;

	@Override
	default int compareTo(final ISimilarityValue value) {
		if (value instanceof IMinimalSimilarityValue || value instanceof IMissingSimilarityValue)
			// we are not an IMinimalSimilarityValue/ImissingSimilarityValue, as compareTo
			// is overriden in
			// those
			return 1;
		else if (value instanceof IMaximalSimilarityValue)
			// we are not an IMaximalSimilarityValue, as compareTo is overriden in
			// IMaximalSimilarityValue
			return -1;
		try {
			return compareTo(value.get());
		} catch (final SimilarityCalculationException e) {
			throw new ClassCastException(
					"One of the similarity values could not be evaluated causing compareTo to fail.");
		}
	}

	default int compareTo(final double v) {
		try {
			return Double.compare(get(), v);
		} catch (final SimilarityCalculationException e) {
			throw new ClassCastException("This similarity value could not be evaluated causing compareTo to fail.");
		}
	}

	default boolean equalTo(final double value) {
		return compareTo(value) == 0;
	}

	default boolean equalTo(final ISimilarityValue value) {
		return compareTo(value) == 0;
	}

	default boolean greaterThan(final ISimilarityValue value) {
		return compareTo(value) > 0;
	}

	default boolean greaterThanOrEquals(final ISimilarityValue value) {
		return greaterThan(value) || equalTo(value);
	}

	default boolean lessThan(final ISimilarityValue value) {
		return compareTo(value) < 0;
	}

	default boolean lessThanOrEquals(final ISimilarityValue value) {
		return lessThan(value) || equalTo(value);
	}

	default boolean greaterThan(final double value) {
		return compareTo(value) > 0;
	}

	default boolean greaterThanOrEquals(final double value) {
		return compareTo(value) >= 0;
	}

	default boolean lessThan(final double value) {
		return compareTo(value) < 0;
	}

	default boolean lessThanOrEquals(final double value) {
		return compareTo(value) <= 0;
	}

}