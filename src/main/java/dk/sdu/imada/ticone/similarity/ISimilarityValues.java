/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

import java.io.Serializable;
import java.util.Map;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IPair;
import dk.sdu.imada.ticone.util.TriFunction;
import dk.sdu.imada.ticone.util.TriPredicate;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectCollection;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 28, 2018
 *
 */
public interface ISimilarityValues<O1, O2, P extends IPair<O1, O2>> extends Iterable<ISimilarityValue>, Serializable {

	boolean isEmpty();

	long size();

	ISimilarityValueStorage getStorage();

	/**
	 * Returns the minimal similarity value.
	 * 
	 * Incomparable similarity values are ignored.
	 * 
	 * @return
	 * @throws SimilarityValuesException
	 * @throws InterruptedException
	 * @throws NoComparableSimilarityValuesException Thrown if this
	 *                                               {@link ISimilarityValues}
	 *                                               instance does not hold any
	 *                                               comparable similarity values.
	 */
	ISimilarityValue min()
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	/**
	 * Returns the maximal similarity value.
	 * 
	 * Incomparable similarity values are ignored.
	 * 
	 * @return
	 * @throws SimilarityValuesException
	 * @throws InterruptedException
	 * @throws NoComparableSimilarityValuesException Thrown if this
	 *                                               {@link ISimilarityValues}
	 *                                               instance does not hold any
	 *                                               comparable similarity values.
	 */
	ISimilarityValue max()
			throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	ISimilarityValue mean() throws SimilarityCalculationException, InterruptedException, SimilarityValuesException;

	ISimilarityValue median() throws SimilarityCalculationException, InterruptedException, SimilarityValuesException;

	ISimilarityValue sum() throws SimilarityCalculationException, InterruptedException, SimilarityValuesException;

	Iterable<P> getPairs() throws SimilarityValuesException;

	ObjectType<?> getObjectType();

	long getNumberOfPairs();

	/**
	 * Sorts this collection by similarity value in descending order.
	 * 
	 * @throws SimilarityValuesException
	 * @throws InterruptedException
	 */
	ISimilarityValuesSomePairs<O1, O2, P> sortedByValue() throws SimilarityValuesException, InterruptedException;

	/**
	 * Returns a new instance of this type that contains a subset of similarity
	 * values of this instance for which the given predicate is true.
	 * 
	 * @param p A predicate to filter on the similarity values in this set.
	 * @return A new instance of this type with a subset this instance's similarity
	 *         values.
	 * @throws InterruptedException
	 */
	ISimilarityValuesSomePairs<O1, O2, P> filter(final TriPredicate<O1, O2, ISimilarityValue> p)
			throws SimilarityValuesException, InterruptedException;

	<R> Map<R, ? extends ISimilarityValuesSomePairs<O1, O2, P>> groupBy(
			final TriFunction<O1, O2, ISimilarityValue, R> f) throws SimilarityValuesException, InterruptedException;

	/**
	 * Partitions the similarity values according to their first object. The
	 * iteration order of the result map is consistent with the ordering of first
	 * objects in this collection.
	 * 
	 * @return
	 * @throws SimilarityValuesException
	 * @throws InterruptedException
	 */
	Map<?, ? extends ISimilarityValues<O1, O2, P>> partitionByFirstObject()
			throws SimilarityValuesException, InterruptedException, SimilarityValuesException;

	/**
	 * Partitions the similarity values according to their second object. The
	 * iteration order of the result map is consistent with the ordering of second
	 * objects in this collection.
	 * 
	 * @return
	 * @throws SimilarityValuesException
	 * @throws InterruptedException
	 */
	Map<?, ? extends ISimilarityValues<O1, O2, P>> partitionBySecondObject()
			throws SimilarityValuesException, InterruptedException, SimilarityValuesException;

	default ObjectCollection<ISimilarityValue> values() {
		if (getStorage() instanceof ISmallSimilarityValueStorage)
			return ObjectArrayList.wrap(((ISmallSimilarityValueStorage) getStorage()).values());
		return ((ILargeSimilarityValueStorage) getStorage()).values();
	}

	ISimilarityValue[] set(long[] is, ISimilarityValue[] values)
			throws SimilarityValuesException, IncompatibleSimilarityValueException;

	ISimilarityValue[] set(int[] is, ISimilarityValue[] values)
			throws SimilarityValuesException, IncompatibleSimilarityValueException;

	ISimilarityValue[] get(long[] is) throws IncompatibleSimilarityValueStorageException;

	ISimilarityValue[] get(int[] is) throws SimilarityValuesException;

	P getPair(long i) throws SimilarityValuesException, InterruptedException;

	boolean[] isMissing(long[] is) throws IncompatibleSimilarityValueStorageException;

	boolean[] isMissing(int[] is) throws SimilarityValuesException;

	boolean[] isSet(long[] is) throws IncompatibleSimilarityValueStorageException;

	boolean[] isSet(int[] is) throws SimilarityValuesException;

	/**
	 * Returns the pair/similarity index with the maximal similarity value.
	 * 
	 * Incomparable similarity values are ignored.
	 * 
	 * @return
	 * @throws SimilarityValuesException
	 * @throws InterruptedException
	 * @throws NoComparableSimilarityValuesException Thrown if this
	 *                                               {@link ISimilarityValues}
	 *                                               instance does not hold any
	 *                                               comparable similarity values.
	 */
	long whichMax() throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	/**
	 * Returns the object with the minimal similarity value.
	 * 
	 * Incomparable similarity values are ignored.
	 * 
	 * @return
	 * @throws SimilarityValuesException
	 * @throws InterruptedException
	 * @throws NoComparableSimilarityValuesException Thrown if this
	 *                                               {@link ISimilarityValues}
	 *                                               instance does not hold any
	 *                                               comparable similarity values.
	 */
	long whichMin() throws SimilarityValuesException, NoComparableSimilarityValuesException, InterruptedException;

	ISimilarityFunction getSimilarityFunction();

}
