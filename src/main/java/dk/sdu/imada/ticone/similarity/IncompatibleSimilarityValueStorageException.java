/**
 * 
 */
package dk.sdu.imada.ticone.similarity;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 27, 2019
 *
 */
public class IncompatibleSimilarityValueStorageException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3008792588258530954L;

	public IncompatibleSimilarityValueStorageException() {
		super();
	}

	public IncompatibleSimilarityValueStorageException(String message) {
		super(message);
	}

	public IncompatibleSimilarityValueStorageException(Throwable cause) {
		super(cause);
	}

}
