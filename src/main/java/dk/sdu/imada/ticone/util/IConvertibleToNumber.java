/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 11, 2019
 *
 */
public interface IConvertibleToNumber<N extends Number> {
	N toNumber() throws ToNumberConversionException;
	
	int cardinality();
}
