/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 23, 2019
 *
 */
public interface IPairs<O1, O2, P extends IPair<O1, O2>> extends IIterableWithSize<P> {

	default P[] toArray() {
		return (P[]) this.asList().toArray();
	}

	IPairList<O1, O2, P> asList();

	IPairSet<O1, O2, P> asSet();

	IPairBuilder<O1, O2, P> getPairBuilder();

}
