/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 10, 2018
 *
 */
public class ObjectProviderChangeEvent {
	protected IObjectProvider provider;

	public ObjectProviderChangeEvent(final IObjectProvider provider) {
		super();
		this.provider = provider;
	}

	/**
	 * @return the provider
	 */
	public IObjectProvider getProvider() {
		return this.provider;
	}
}
