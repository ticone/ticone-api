/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Iterator;

public abstract class BoundIterator<T> implements Iterator<T> {
	final protected long start, end;

	public BoundIterator(final long offset, final long end) {
		this.start = offset;
		this.end = end;
	}

	public long start() {
		return this.start;
	}

	public long end() {
		return this.end;
	}
}