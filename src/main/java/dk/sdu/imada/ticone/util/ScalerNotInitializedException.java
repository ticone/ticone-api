/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 6, 2018
 *
 */
public class ScalerNotInitializedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6523391385103421762L;

	public ScalerNotInitializedException(final String m) {
		super(String.format("This scaler has not been fully initialized: Field '%s' is not set.", m));
	}
}
