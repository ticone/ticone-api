/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public interface IFactory<T, FE extends FactoryException, IE extends CreateInstanceFactoryException> {

	/**
	 * Returns a copy of this factory.
	 * 
	 * @return
	 */
	IFactory<T, FE, IE> copy();

}
