/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * A queue which can be closed, effectively preventing addition of new elements
 * while allowing retrieval of remaining elements.
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 16, 2018
 *
 * @param <E>
 */
public final class ClosableBlockingQueue<E> extends LinkedBlockingQueue<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7866281278615752261L;

	protected boolean isClosed = false;

	public ClosableBlockingQueue(final int capacity) {
		super(capacity);
	}

	public void close() {
		this.isClosed = true;
	}

	/**
	 * @return the isClosed
	 */
	public boolean isClosed() {
		return this.isClosed;
	}

	@Override
	public E peek() {
		if (this.isEmpty() && this.isClosed())
			return null;
		return super.peek();
	}

	@Override
	public E take() throws InterruptedException {
		if (this.isEmpty() && this.isClosed())
			return null;
		return super.take();
	}

	@Override
	public boolean offer(final E e) {
		if (this.isClosed)
			return false;
		return super.offer(e);
	}

	@Override
	public boolean add(final E e) {
		if (this.isClosed)
			throw new IllegalStateException("Queue closed");
		return super.add(e);
	}

	@Override
	public boolean addAll(final Collection<? extends E> c) {
		if (this.isClosed)
			return false;
		return super.addAll(c);
	}

	@Override
	public boolean offer(final E e, final long timeout, final TimeUnit unit) throws InterruptedException {
		if (this.isEmpty() && this.isClosed())
			return false;
		return super.offer(e, timeout, unit);
	}

	@Override
	public void put(final E e) throws InterruptedException {
		if (this.isEmpty() && this.isClosed())
			return;
		super.put(e);
	}
}