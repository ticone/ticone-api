/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 11, 2019
 *
 */
public class NotAnArithmeticFeatureValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5327601374522308001L;

	public NotAnArithmeticFeatureValueException() {
		super();
	}

	public NotAnArithmeticFeatureValueException(String message) {
		super(message);
	}

	public NotAnArithmeticFeatureValueException(Throwable cause) {
		super(cause);
	}

}
