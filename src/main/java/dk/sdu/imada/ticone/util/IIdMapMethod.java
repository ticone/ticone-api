/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public interface IIdMapMethod {

	String getAlternativeId(String objectId);

	void setActive(boolean isActive);

	boolean isActive();

	IIdMapMethod copy();

}