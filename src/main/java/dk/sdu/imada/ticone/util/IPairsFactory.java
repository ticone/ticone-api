/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 23, 2019
 *
 */
public interface IPairsFactory<O1, O2, P extends IPair<O1, O2>> {

	IPairs<O1, O2, P> createIterable(final O1[] objects, final O2[] otherObjects);

	IPairs<O1, O2, P> createList(final Iterable<? extends O1> objects, final Iterable<? extends O2> otherObjects);

	IPairs<O1, O2, P> createSet(final Iterable<? extends O1> objects, final Iterable<? extends O2> otherObjects);

	IPairs<O1, O2, P> createEmptyList();

	IPairs<O1, O2, P> createEmptySet();

	IPairBuilder<O1, O2, P> getPairBuilder();

}
