package dk.sdu.imada.ticone.util;

import java.util.Date;

public interface ITiconeResult {
	Date getDate();

	String getTiconeVersion();

	void addChangeListener(final ITiconeResultChangeListener listener);

	void removeChangeListener(final ITiconeResultChangeListener listener);

	void addOnDestroyListener(final ITiconeResultDestroyedListener listener);

	void removeOnDestroyListener(final ITiconeResultDestroyedListener listener);

	void destroy();
}
