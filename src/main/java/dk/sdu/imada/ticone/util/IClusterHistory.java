/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.List;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public interface IClusterHistory<CLUSTERING extends IClusterObjectMapping> {

	int getIterationNumber();

	CLUSTERING getClusterObjectMapping();

	void addChild(IClusterHistory<CLUSTERING> patternHistory);

	IClusterHistory<CLUSTERING> getParent();

	void setParent(final IClusterHistory<CLUSTERING> history);

	List<IClusterHistory<CLUSTERING>> getChildren();

	String getNote();

	@Override
	String toString();

	ITimeSeriesObjects getAllObjects();

}