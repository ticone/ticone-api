/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.Iterator;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 30, 2018
 *
 */
public abstract class BoundIterable<T> implements IIterableWithSize<T>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6667177964536965496L;
	final protected long start, end;

	/**
	 * 
	 */
	public BoundIterable(final long start, final long end) {
		this.start = start;
		this.end = end;
	}

	/**
	 * @return the start
	 */
	public long start() {
		return this.start;
	}

	/**
	 * @return the end
	 */
	public long end() {
		return this.end;
	}

	@Override
	public long longSize() {
		return this.end - this.start;
	}

	@Override
	public abstract BoundIterator iterator();

	public abstract BoundIterable<T> range(long start, long end);

	public abstract class BoundIterator implements Iterator<T> {
	}
}
