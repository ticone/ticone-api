/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 21, 2019
 *
 */
public class TiconeResultChangeEvent {
	private final ITiconeResult result;

	public TiconeResultChangeEvent(ITiconeResult result) {
		super();
		this.result = result;
	}

	/**
	 * @return the result
	 */
	public ITiconeResult getSource() {
		return this.result;
	}
}
