package dk.sdu.imada.ticone.util;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 * @param O The object being aggregated.
 * @param A The result type when applying this aggregation function.
 */
public interface IAggregate<O, A> extends Serializable {

	IAggregate<O, A> copy();

	A aggregate(O objects) throws AggregationException, InterruptedException;
}
