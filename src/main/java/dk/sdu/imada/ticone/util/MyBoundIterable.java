/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyBoundIterable<V> extends BoundIterable<V> {

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Mar 14, 2019
	 *
	 */
	private final class BoundIteratorExtension extends BoundIterator {
		private long pos = 0;
		private Iterator<V> it;

		/**
		 * 
		 */
		BoundIteratorExtension() {
			super();
			this.it = MyBoundIterable.this.elements.iterator();
			while (this.it.hasNext() && pos < MyBoundIterable.this.start) {
				this.it.next();
				pos++;
			}
		}

		@Override
		public boolean hasNext() {
			return this.it.hasNext() && this.pos < MyBoundIterable.this.end;
		}

		@Override
		public V next() {
			if (!hasNext())
				throw new NoSuchElementException();
			pos++;
			return this.it.next();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1250954576579340845L;
	final protected Iterable<V> elements;

	/**
	 * @param start
	 * @param end
	 */
	MyBoundIterable(final Iterable<V> elements) {
		this(elements, 0, (int) Iterables.size(elements));
	}

	MyBoundIterable(final Iterable<V> elements, final int start, final int end) {
		super(start, end);
		this.elements = elements;
	}

	@Override
	public BoundIterable<V>.BoundIterator iterator() {
		return new BoundIteratorExtension();
	}

	@Override
	public BoundIterable<V> range(final long start, final long end) {
		return new MyBoundIterable<>(this.elements, (int) start, (int) end);
	}

}