/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 9, 2018
 *
 */
public interface IFeatureValueProviderListener {
	void featureValueProviderChanged(final FeatureValueProviderChangeEvent e);
}
