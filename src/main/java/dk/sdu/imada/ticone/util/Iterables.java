/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

import dk.sdu.imada.ticone.util.MyParallel.BatchCount;
import it.unimi.dsi.fastutil.doubles.DoubleIterable;
import it.unimi.dsi.fastutil.doubles.DoubleIterator;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntBigArrayBigList;
import it.unimi.dsi.fastutil.ints.IntBigList;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntIterable;
import it.unimi.dsi.fastutil.ints.IntIterator;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.longs.LongBigArrayBigList;
import it.unimi.dsi.fastutil.longs.LongBigList;
import it.unimi.dsi.fastutil.longs.LongIterable;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.objects.ObjectIterable;
import it.unimi.dsi.fastutil.objects.ObjectIterator;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 23, 2018
 *
 */
public class Iterables {

	public static long size(final Iterable<?> iterable) {
		if (iterable instanceof IIterableWithSize)
			return ((IIterableWithSize<?>) iterable).longSize();
		else if (iterable instanceof Collection)
			return ((Collection) iterable).size();
		final Iterator<?> it = iterable.iterator();
		long c;
		for (c = 0; it.hasNext(); it.next())
			c++;
		return c;
	}

	public static boolean isAtLeastOfSize(final Iterable<?> iterable, final long size) {
		long c = 0;
		for (Object o : iterable) {
			c++;
			if (c >= size)
				return true;
		}
		return false;
	}

	public static <T> List<T> toList(final Iterable<T> iterable) {
		final List<T> result = new ArrayList<>();
		final Iterator<T> it = iterable.iterator();
		while (it.hasNext())
			result.add(it.next());
		return result;
	}

	public static BoundLongIterable range(final long start, final long end, final boolean inclusiveEnd) {
		return new BoundLongIterable(start, inclusiveEnd ? end + 1 : end);
	}

	public static BoundLongIterable range(final long end) {
		return range(0, end, false);
	}

	public static BoundIntIterable range(final int end) {
		return range(0, end, false);
	}

	public static BoundIntIterable range(final int start, final int end, final boolean inclusiveEnd) {
		return new BoundIntIterable(start, inclusiveEnd ? end + 1 : end);
	}

	public static class BoundLongIterable extends BoundIterable<Long> implements LongIterable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4157636166254756593L;

		public BoundLongIterable(long start, long end) {
			super(start, end);
		}

		@Override
		public BoundLongIterator iterator() {
			return new BoundLongIterator();
		}

		@Override
		public BoundIterable<Long> range(long start, long end) {
			return new BoundLongIterable(start, end);
		}

		class BoundLongIterator extends BoundIterator implements LongIterator {

			long pos = start;

			@Override
			public boolean hasNext() {
				return pos < end;
			}

			@Override
			public long nextLong() {
				if (!hasNext())
					throw new NoSuchElementException();
				return pos++;
			}

		}
	}

	static class BoundIntIterable extends BoundIterable<Integer> implements IntIterable {

		public BoundIntIterable(long start, long end) {
			super(start, end);
		}

		@Override
		public BoundIntIterator iterator() {
			return new BoundIntIterator();
		}

		@Override
		public BoundIntIterable range(long start, long end) {
			return new BoundIntIterable(start, end);
		}

		class BoundIntIterator extends BoundIterator implements IntIterator {

			int pos = (int) start;

			@Override
			public boolean hasNext() {
				return pos < end;
			}

			@Override
			public int nextInt() {
				if (!hasNext())
					throw new NoSuchElementException();
				return pos++;
			}

		}
	}

	public static <V> Iterable<V[]> batchify(final V[] elements, final BatchCount desiredBatchCount) {
		if (desiredBatchCount.count <= 0)
			throw new IllegalArgumentException();

		final long totalCount = elements.length;
		final int batchSize = calculateBatchSize(desiredBatchCount, totalCount);

		return new Iterable<V[]>() {

			@Override
			public Iterator<V[]> iterator() {
				return new BatchArrayIterator<>(elements, batchSize);
			}
		};
	}

	private static int calculateBatchSize(final BatchCount desiredBatchCount, final long totalCount) {
		final int batchSize;

		if (desiredBatchCount.hasMinBatchSize()) {
			// everything belongs into one batch
			if (totalCount <= desiredBatchCount.minBatchSize) {
				batchSize = (int) totalCount;
			} else {
				final int tmpBatchSize = (int) Math.ceil((double) totalCount / desiredBatchCount.count);

				if (tmpBatchSize < desiredBatchCount.minBatchSize) {
					batchSize = desiredBatchCount.minBatchSize.intValue();
				} else {
					batchSize = tmpBatchSize;
				}
			}
		} else {
			batchSize = (int) Math.ceil((double) totalCount / desiredBatchCount.count);
		}
		return batchSize;
	}

	public static Iterable<int[]> batchify(final int[] elements, final BatchCount desiredBatchCount) {
		if (desiredBatchCount.count <= 0)
			throw new IllegalArgumentException();

		final long totalCount = elements.length;
		final int batchSize = calculateBatchSize(desiredBatchCount, totalCount);

		return new Iterable<int[]>() {

			@Override
			public Iterator<int[]> iterator() {
				return new IntArrayBatchIterator<>(elements, batchSize);
			}
		};
	}

	public static Iterable<long[]> batchify(final long[] elements, final BatchCount desiredBatchCount) {
		if (desiredBatchCount.count <= 0)
			throw new IllegalArgumentException();

		final long totalCount = elements.length;
		final int batchSize = calculateBatchSize(desiredBatchCount, totalCount);

		return new Iterable<long[]>() {

			@Override
			public Iterator<long[]> iterator() {
				return new LongArrayBatchIterator<>(elements, batchSize);
			}
		};
	}

	public static <V> Iterable<Iterable<V>> batchify(final BoundIterable<V> elements,
			final BatchCount desiredBatchCount) {
		if (desiredBatchCount.count <= 0)
			throw new IllegalArgumentException();

		final long totalCount = Iterables.size(elements);
		final int batchSize = calculateBatchSize(desiredBatchCount, totalCount);

		return new Iterable<Iterable<V>>() {

			@Override
			public Iterator<Iterable<V>> iterator() {
				return new BatchBoundIterator<>(elements, batchSize);
			}
		};
	}

	public static Iterable<IntIterable> batchify(final IntCollection elements, final BatchCount desiredBatchCount) {
		if (desiredBatchCount.count <= 0)
			throw new IllegalArgumentException();

		final long totalCount = Iterables.size(elements);
		final int batchSize = calculateBatchSize(desiredBatchCount, totalCount);

		// Logger logger = LoggerFactory.getLogger(Iterables.class);
		// logger.debug("batchify: {} elements into {} batches of (max) size {}",
		// totalCount, batchCount, batchSize);

		return new Iterable<IntIterable>() {

			@Override
			public Iterator<IntIterable> iterator() {
				return new IntBatchIterator(elements, batchSize);
			}
		};
	}

	public static <V> Iterable<Iterable<V>> batchify(final Iterable<V> elements, final BatchCount desiredBatchCount) {
		if (elements instanceof BoundIterable)
			return batchify((BoundIterable<V>) elements, desiredBatchCount);

		if (desiredBatchCount.count <= 0)
			throw new IllegalArgumentException();

		final long totalCount = Iterables.size(elements);
		final int batchSize = calculateBatchSize(desiredBatchCount, totalCount);

		// Logger logger = LoggerFactory.getLogger(Iterables.class);
		// logger.debug("batchify: {} elements into {} batches of (max) size {}",
		// totalCount, batchCount, batchSize);

		return new Iterable<Iterable<V>>() {

			@Override
			public Iterator<Iterable<V>> iterator() {
				return new BatchIterator<>(elements, batchSize);
			}
		};
	}

	public static Iterable<IntIterable> batchifyInts(final Iterable<Integer> elements,
			final BatchCount desiredBatchCount) {
		if (desiredBatchCount.count <= 0)
			throw new IllegalArgumentException();

		final long totalCount = Iterables.size(elements);
		final int batchSize = calculateBatchSize(desiredBatchCount, totalCount);

		return new Iterable<IntIterable>() {

			@Override
			public Iterator<IntIterable> iterator() {
				return new IntBatchIterator(elements, batchSize);
			}
		};
	}

	public static Iterable<long[]> batchifyLongs(final Iterable<Long> elements, final BatchCount desiredBatchCount) {
		if (desiredBatchCount.count <= 0)
			throw new IllegalArgumentException();

		final long totalCount = Iterables.size(elements);
		final int batchSize = calculateBatchSize(desiredBatchCount, totalCount);

		return new Iterable<long[]>() {

			@Override
			public Iterator<long[]> iterator() {
				return new LongBatchIterator(elements, batchSize);
			}
		};
	}

	public static <V> BoundIterable<V> toBoundIterable(final Iterable<V> elements) {
		return new MyBoundIterable<>(elements);
	}

	public static <V> Iterable<V> filter(Iterable<V> iterable, Predicate<V> pred) {
		return new Iterable<V>() {
			@Override
			public Iterator<V> iterator() {
				return new Iterator<V>() {
					final Iterator<V> it = iterable.iterator();
					V nextHit = null;

					@Override
					public boolean hasNext() {
						while (nextHit == null && it.hasNext()) {
							final V next = it.next();
							if (pred.test(next))
								nextHit = next;
						}
						return nextHit != null;
					}

					@Override
					public V next() {
						if (!hasNext())
							throw new NoSuchElementException();
						V next = nextHit;
						nextHit = null;
						return next;
					}
				};
			}
		};
	}

	public static IntIterable iterable(int[] array) {
		return new IntArrayIterable(array);
	}

	public static DoubleIterable iterable(double[][] array) {
		return new DoubleIterable() {

			@Override
			public DoubleIterator iterator() {
				return new DoubleIterator() {
					int i = 0, j = -1;

					@Override
					public boolean hasNext() {
						if (i >= array.length)
							return false;

						if (j + 1 < array[i].length)
							return true;
						// we are at the end of y
						// as there another x, with some y's?
						return i + 1 < array.length && array[i + 1].length > 0;
					}

					@Override
					public double nextDouble() {
						if (i >= array.length)
							throw new NoSuchElementException();
						else if (j + 1 < array[i].length)
							j++;
						else if (i + 1 < array.length) {
							i++;
							j = -1;
						} else
							throw new NoSuchElementException();
						return array[i][j];
					}
				};
			}
		};
	}

	public static DoubleIterable iterable(List<Double>[] array) {
		return new DoubleIterable() {

			@Override
			public DoubleIterator iterator() {
				return new DoubleIterator() {
					int i = 0, j = -1;

					@Override
					public boolean hasNext() {
						if (i >= array.length)
							return false;

						if (j + 1 < array[i].size())
							return true;
						// we are at the end of y
						// as there another x, with some y's?
						return i + 1 < array.length && array[i + 1].size() > 0;
					}

					@Override
					public double nextDouble() {
						if (i >= array.length)
							throw new NoSuchElementException();
						else if (j + 1 < array[i].size())
							j++;
						else if (i + 1 < array.length) {
							i++;
							j = -1;
						} else
							throw new NoSuchElementException();
						return array[i].get(j);
					}
				};
			}
		};
	}

	public static <K> ObjectIterable<K> iterable(K[][] array) {
		return new ObjectIterable<K>() {

			@Override
			public ObjectIterator<K> iterator() {
				return new ObjectIterator<K>() {
					int i = 0, j = -1;

					@Override
					public boolean hasNext() {
						if (i >= array.length)
							return false;

						if (j + 1 < array[i].length)
							return true;
						// we are at the end of y
						// as there another x, with some y's?
						return i + 1 < array.length && array[i + 1].length > 0;
					}

					@Override
					public K next() {
						if (i >= array.length)
							throw new NoSuchElementException();
						else if (j + 1 < array[i].length)
							j++;
						else if (i + 1 < array.length) {
							i++;
							j = -1;
						} else
							throw new NoSuchElementException();
						return array[i][j];
					}
				};
			}
		};
	}
}

class BatchBoundIterator<V> implements Iterator<Iterable<V>> {

	final protected long batchSize;
	protected long currentBatchOffset = 0;
	final protected BoundIterable<V> elements;

	/**
	 * @param elements
	 */
	public BatchBoundIterator(final BoundIterable<V> elements, final long batchSize) {
		if (elements == null)
			throw new IllegalArgumentException();
		if (elements.longSize() > 0 && batchSize == 0)
			throw new IllegalArgumentException();
		this.elements = elements;
		this.batchSize = batchSize;
	}

	@Override
	public boolean hasNext() {
		return this.currentBatchOffset < this.elements.longSize();
	}

	@Override
	public BoundIterable<V> next() {
		if (!hasNext())
			throw new NoSuchElementException();
		final BoundIterable<V> offsetIterable = this.elements.range(this.currentBatchOffset,
				Math.min(this.elements.longSize(), this.currentBatchOffset + this.batchSize));
		this.currentBatchOffset += this.batchSize;
		return offsetIterable;
	}
}

class BatchArrayIterator<V> implements Iterator<V[]> {

	final protected int batchSize;
	protected int currentBatchOffset = 0;
	final protected V[] elements;

	/**
	 * @param elements
	 */
	public BatchArrayIterator(final V[] elements, final int batchSize) {
		if (elements == null)
			throw new IllegalArgumentException();
		if (elements.length > 0 && batchSize == 0)
			throw new IllegalArgumentException();
		this.elements = elements;
		this.batchSize = batchSize;
	}

	@Override
	public boolean hasNext() {
		return this.currentBatchOffset < this.elements.length;
	}

	@Override
	public V[] next() {
		if (!hasNext())
			throw new NoSuchElementException();
		final V[] offsetIterable;
		if (this.currentBatchOffset == 0 && this.elements.length <= this.batchSize)
			offsetIterable = this.elements;
		else
			offsetIterable = Arrays.copyOfRange(this.elements, this.currentBatchOffset,
					Math.min(this.elements.length, this.currentBatchOffset + this.batchSize));
		this.currentBatchOffset += this.batchSize;
		return offsetIterable;
	}
}

class IntArrayBatchIterator<V> implements Iterator<int[]> {

	final protected int batchSize;
	protected int currentBatchOffset = 0;
	final protected int[] elements;

	/**
	 * @param elements
	 */
	public IntArrayBatchIterator(final int[] elements, final int batchSize) {
		if (elements == null)
			throw new IllegalArgumentException();
		if (elements.length > 0 && batchSize == 0)
			throw new IllegalArgumentException();
		this.elements = elements;
		this.batchSize = batchSize;
	}

	@Override
	public boolean hasNext() {
		return this.currentBatchOffset < this.elements.length;
	}

	@Override
	public int[] next() {
		if (!hasNext())
			throw new NoSuchElementException();
		final int[] offsetIterable = Arrays.copyOfRange(this.elements, this.currentBatchOffset,
				Math.min(this.elements.length, this.currentBatchOffset + this.batchSize));
		this.currentBatchOffset += this.batchSize;
		return offsetIterable;
	}
}

class LongArrayBatchIterator<V> implements Iterator<long[]> {

	final protected int batchSize;
	protected int currentBatchOffset = 0;
	final protected long[] elements;

	/**
	 * @param elements
	 */
	public LongArrayBatchIterator(final long[] elements, final int batchSize) {
		if (elements == null)
			throw new IllegalArgumentException();
		if (elements.length > 0 && batchSize == 0)
			throw new IllegalArgumentException();
		this.elements = elements;
		this.batchSize = batchSize;
	}

	@Override
	public boolean hasNext() {
		return this.currentBatchOffset < this.elements.length;
	}

	@Override
	public long[] next() {
		if (!hasNext())
			throw new NoSuchElementException();
		final long[] offsetIterable = Arrays.copyOfRange(this.elements, this.currentBatchOffset,
				Math.min(this.elements.length, this.currentBatchOffset + this.batchSize));
		this.currentBatchOffset += this.batchSize;
		return offsetIterable;
	}
}

class BatchIterator<V> implements Iterator<Iterable<V>> {

	final protected long batchSize;
	final protected Iterator<V> elements;

	/**
	 * @param elements
	 */
	public BatchIterator(final Iterable<V> elements, final long batchSize) {
		if (elements == null)
			throw new IllegalArgumentException();
		if (batchSize == 0)
			throw new IllegalArgumentException();
		this.elements = elements.iterator();
		this.batchSize = batchSize;
	}

	@Override
	public boolean hasNext() {
		return this.elements.hasNext();
	}

	@Override
	public Iterable<V> next() {
		if (!hasNext())
			throw new NoSuchElementException();
		final List<V> batch = new ArrayList<>();
		while (batch.size() < this.batchSize && this.elements.hasNext())
			batch.add(this.elements.next());
		return batch;
	}
}

class LongBatchIterator implements Iterator<long[]> {

	final protected long batchSize;
	final protected Iterator<Long> elements;

	/**
	 * @param elements
	 */
	public LongBatchIterator(final Iterable<Long> elements, final int batchSize) {
		if (elements == null)
			throw new IllegalArgumentException();
		if (batchSize == 0)
			throw new IllegalArgumentException();
		this.elements = elements.iterator();
		this.batchSize = batchSize;
	}

	@Override
	public boolean hasNext() {
		return this.elements.hasNext();
	}

	@Override
	public long[] next() {
		if (!hasNext())
			throw new NoSuchElementException();
		final LongBigList batch = new LongBigArrayBigList();
		while (batch.size64() < this.batchSize && this.elements.hasNext())
			batch.add(this.elements.next().longValue());
		return batch.toLongArray();
	}
}

class IntArrayIterable implements IntIterable {
	private final IntList array;

	public IntArrayIterable(final int[] array) {
		super();
		this.array = IntArrayList.wrap(array);
	}

	@Override
	public IntIterator iterator() {
		return this.array.iterator();
	}
}

class IntBatchIterator implements Iterator<IntIterable> {

	final protected long batchSize;
	final protected Iterator<Integer> elements;

	/**
	 * @param elements
	 */
	public IntBatchIterator(final Iterable<Integer> elements, final long batchSize) {
		if (elements == null)
			throw new IllegalArgumentException();
		if (batchSize == 0)
			throw new IllegalArgumentException();
		this.elements = elements.iterator();
		this.batchSize = batchSize;
	}

	@Override
	public boolean hasNext() {
		return this.elements.hasNext();
	}

	@Override
	public IntIterable next() {
		if (!hasNext())
			throw new NoSuchElementException();
		final IntBigList batch = new IntBigArrayBigList();
		while (batch.size64() < this.batchSize && this.elements.hasNext())
			batch.add(this.elements.next().intValue());
		return batch;
	}
}
