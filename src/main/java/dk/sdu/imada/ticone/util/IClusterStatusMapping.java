/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Collection;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusters;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 15, 2018
 *
 */
public interface IClusterStatusMapping extends IStatusMapping<ICluster> {

	void addCluster(ICluster pattern, ITimeSeriesObjects patternsData, boolean newCluster, boolean splitCluster,
			boolean filteredCluster);

	IClusters getChildren(ICluster pattern);

	Collection<ICluster> getSplitClusters();

	IClusters getClustersToShow();

}
