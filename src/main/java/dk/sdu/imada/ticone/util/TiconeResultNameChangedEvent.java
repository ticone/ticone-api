/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 17, 2018
 *
 */
public class TiconeResultNameChangedEvent {
	protected final INamedTiconeResult source;

	protected final String newName;

	public TiconeResultNameChangedEvent(INamedTiconeResult source, String newName) {
		super();
		this.source = source;
		this.newName = newName;
	}

	/**
	 * @return the source
	 */
	public INamedTiconeResult getSource() {
		return this.source;
	}

	/**
	 * @return the newName
	 */
	public String getNewName() {
		return this.newName;
	}
}
