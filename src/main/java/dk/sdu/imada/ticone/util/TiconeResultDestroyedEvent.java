/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 18, 2019
 *
 */
public class TiconeResultDestroyedEvent {

	final protected ITiconeResult result;

	public TiconeResultDestroyedEvent(ITiconeResult result) {
		super();
		this.result = result;
	}

	public ITiconeResult getResult() {
		return this.result;
	}
}
