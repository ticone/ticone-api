/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;

import dk.sdu.imada.ticone.clustering.IStatusMappingListener;
import dk.sdu.imada.ticone.clustering.StatusMappingEvent;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public interface IStatusMapping<T extends IObjectWithFeatures> extends Serializable {
	Collection<T> getNew();

	Collection<T> getDeleted();

	boolean isFilterActive();

	Collection<T> getFiltered();

	void addStatusMappingListener(final IStatusMappingListener l);

	void removeStatusMappingListener(final IStatusMappingListener l);

	void fireStatusMappingChanged(final StatusMappingEvent e);

	IFeatureStore getFeatureStore();

	void add(final T object, final boolean isNew, final boolean isDeleted, final boolean isFiltered);

	void setDeleted(final T object);

	void setNew(final T object);

	IStatusComparator<T> comparator();

	public interface IStatusComparator<T extends IObjectWithFeatures> extends Comparator<T> {

		IStatusComparator<T> setReverse(final boolean reverseComparator);

		IStatusComparator<T> setDecreasing(final boolean decreasing);

		boolean isDecreasing();

	}
}