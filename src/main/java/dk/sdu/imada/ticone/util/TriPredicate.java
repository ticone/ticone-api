/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Objects;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 14, 2018
 *
 */
public interface TriPredicate<T, U, V> {

	boolean test(T t, U u, V v);

	default TriPredicate<T, U, V> and(final TriPredicate<? super T, ? super U, ? super V> other) {
		Objects.requireNonNull(other);
		return (final T t, final U u, final V v) -> test(t, u, v) && other.test(t, u, v);
	}

	/**
	 * Returns a predicate that represents the logical negation of this predicate.
	 *
	 * @return a predicate that represents the logical negation of this predicate
	 */
	default TriPredicate<T, U, V> negate() {
		return (final T t, final U u, final V v) -> !test(t, u, v);
	}

	/**
	 * Returns a composed predicate that represents a short-circuiting logical OR of
	 * this predicate and another. When evaluating the composed predicate, if this
	 * predicate is {@code true}, then the {@code other} predicate is not evaluated.
	 *
	 * <p>
	 * Any exceptions thrown during evaluation of either predicate are relayed to
	 * the caller; if evaluation of this predicate throws an exception, the
	 * {@code other} predicate will not be evaluated.
	 *
	 * @param other a predicate that will be logically-ORed with this predicate
	 * @return a composed predicate that represents the short-circuiting logical OR
	 *         of this predicate and the {@code other} predicate
	 * @throws NullPointerException if other is null
	 */
	default TriPredicate<T, U, V> or(final TriPredicate<? super T, ? super U, ? super V> other) {
		Objects.requireNonNull(other);
		return (final T t, final U u, final V v) -> test(t, u, v) || other.test(t, u, v);
	}
}
