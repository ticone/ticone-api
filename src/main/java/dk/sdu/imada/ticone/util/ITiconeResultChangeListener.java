/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 21, 2019
 *
 */
public interface ITiconeResultChangeListener {

	void resultChanged(TiconeResultChangeEvent e);

}
