/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 18, 2019
 *
 */
public interface ITiconeResultDestroyedListener {
	void ticoneResultDestroyed(TiconeResultDestroyedEvent event);
}
