/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 11, 2018
 *
 */
public interface IObjectProviderListener {
	void objectProviderChanged(final ObjectProviderChangeEvent e);
}
