/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class AggregationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -386632074530813517L;

	public AggregationException(final String message) {
		super(message);
	}

	public AggregationException(final Throwable e) {
		super(e);
	}
}
