/**
 * 
 */
package dk.sdu.imada.ticone.util;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;

/**
 * Indicates that the {@link IFeatureValueProvider} supports the specified
 * {@link IFeature}, but does not know the {@link IObjectWithFeatures}.
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 10, 2018
 *
 */
public class UnknownObjectFeatureValueProviderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4371978446334356918L;

	protected IFeature.IFeatureValueProvider provider;

	protected IObjectWithFeatures object;

	public UnknownObjectFeatureValueProviderException(final IFeature.IFeatureValueProvider provider,
			final IObjectWithFeatures object) {
		super(String.format("Object '%s' is unknown to feature value provider '%s'", object, provider));
		this.provider = provider;
		this.object = object;
	}

	public UnknownObjectFeatureValueProviderException(final IFeature.IFeatureValueProvider provider,
			final IObjectWithFeatures object, final String message) {
		super(message);
		this.provider = provider;
		this.object = object;
	}

	/**
	 * @return the provider
	 */
	public IFeature.IFeatureValueProvider getProvider() {
		return this.provider;
	}

	/**
	 * @return the object
	 */
	public IObjectWithFeatures getObject() {
		return this.object;
	}
}
