/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 15, 2018
 *
 */
public interface IPair<S, T> {
	S getFirst();

	T getSecond();
}
