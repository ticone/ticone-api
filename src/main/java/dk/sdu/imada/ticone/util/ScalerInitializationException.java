/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 27, 2018
 *
 */
public class ScalerInitializationException extends ScalingException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8329873729669303449L;

	/**
	 * @param string
	 */
	public ScalerInitializationException(final String string) {
		super(string);
	}

	/**
	 * @param cause
	 */
	public ScalerInitializationException(final Throwable cause) {
		super(cause);
	}

}
