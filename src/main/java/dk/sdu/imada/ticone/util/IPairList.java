/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.List;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 23, 2019
 *
 */
public interface IPairList<O1, O2, P extends IPair<O1, O2>> extends IPairCollection<O1, O2, P>, List<P> {

	@Override
	default P[] toArray() {
		return IPairCollection.super.toArray();
	}

}
