/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public class Utility {

	private static final Progress progress = new Progress();

	public static Progress getProgress() {
		return progress;
	}

}
