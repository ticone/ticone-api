/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import dk.sdu.imada.ticone.feature.IFeatureValueSampleManager;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.statistics.IFeatureValueSamples;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 9, 2018
 *
 */
public interface IObjectProvider extends IProvider {

	Collection<ObjectType<?>> providedTypesOfObjects();

	default boolean isProvidingObjectsOfType(ObjectType<?> type) {
		return providedTypesOfObjects().contains(type);
	}

	default <T extends IObjectWithFeatures> int getNumberObjectsOfType(final ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (!isProvidingObjectsOfType(type))
			throw new IncompatibleObjectProviderException(this, type);
		return 0;
	}

	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(final ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (!isProvidingObjectsOfType(type))
			throw new IncompatibleObjectProviderException(this, type);
		return Collections.emptySet();
	}

	default <T extends IObjectWithFeatures> List<T> sampleObjectsOfType(ObjectType<T> type, final int sampleSize,
			final long seed) throws IncompatibleObjectProviderException, ObjectSampleException, InterruptedException {
		if (getNumberObjectsOfType(type) == 0)
			throw new ObjectSampleException(String.format(
					"Cannot sample objects of type '%s' from object provider '%s' as it provides 0 objects of that type",
					type, this));

		throw new ObjectSampleException();
	}

	IObjectProviderManager getObjectProviderManager();

	default void addObjectProviderListener(final IObjectProviderListener listener) {
		getObjectProviderManager().addObjectProviderListener(listener);
	}

	default void removeObjectProviderListener(final IObjectProviderListener listener) {
		getObjectProviderManager().removeObjectProviderListener(listener);
	}

	default void fireObjectProviderChanged() {
		getObjectProviderManager().fireObjectProviderChanged();
	}

	IObjectProvider copy() throws InterruptedException;

	IFeatureValueSampleManager getFeatureValueSampleManager();

	default IFeatureValueSamples getFeatureValueSamples() {
		return getFeatureValueSampleManager().getFeatureValueSamples();
	}
}
