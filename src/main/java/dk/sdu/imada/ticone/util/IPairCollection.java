/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.util.Collection;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 23, 2019
 *
 */
public interface IPairCollection<O1, O2, P extends IPair<O1, O2>> extends Collection<P>, IPairs<O1, O2, P> {

	default IPairCollection<O1, O2, P> addPairsOf(final Iterable<? extends O1> objects,
			final Iterable<? extends O2> otherObjects) {
		for (final O1 o : objects)
			for (final O2 o2 : otherObjects)
				this.addPairOf(o, o2);
		return this;
	}

	default IPairCollection<O1, O2, P> addPairsOf(final O1 object, final O2... otherObjects) {
		for (final O2 otherObject : otherObjects)
			this.addPairOf(object, otherObject);
		return this;
	}

	default IPairCollection<O1, O2, P> addPairsOf(final O1 object, final Iterable<? extends O2> otherObjects) {
		for (final O2 o2 : otherObjects)
			this.addPairOf(object, o2);
		return this;
	}

	boolean addPairOf(final O1 object, final O2 otherObject);

	@Override
	default P[] toArray() {
		return IPairs.super.toArray();
	}

	@Override
	default long longSize() {
		return this.size();
	}
}
