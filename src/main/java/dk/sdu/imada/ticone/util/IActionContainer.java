/**
 * 
 */
package dk.sdu.imada.ticone.util;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitClusterContainer;
import dk.sdu.imada.ticone.clustering.suggestclusters.IClusterSuggestion;
import dk.sdu.imada.ticone.clustering.suggestclusters.ISuggestNewCluster;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public interface IActionContainer {

	public enum ACTION_TYPE {
		SPLIT_PATTERN, SUGGEST_PATTERN, DELETE_PATTERN
	}

	ACTION_TYPE getActionType();

	ISplitCluster getSplitPattern();

	ISplitClusterContainer getSplitPatternContainer();

	ICluster getPatternToDelete();

	IClusterObjectMapping.DELETE_METHOD getDeleteType();

	IClusterSuggestion getClusteringTransition();

	ISuggestNewCluster getSuggestNewPattern();

}