/*
 * Copyright 2011 Matt Crinklaw-Vogt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package dk.sdu.imada.ticone.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * @author Christian Wiwie
 * @author Christian Nørskov
 * @since 20-02-16
 */
public class MyParallel<V> {
	public static final int NUM_CORES = Runtime.getRuntime().availableProcessors();
	public static final int DEFAULT_THREADS = NUM_CORES - 1;
	public static final int DEFAULT_QUEUE_SIZE = 10000;
	public static final long DEFAULT_QUEUE_POLLING = 20;
	public static final ScheduledThreadPoolExecutor SIMILARITY_THREADPOOL = new ScheduledThreadPoolExecutor(
			DEFAULT_THREADS, new ThreadFactory() {

				ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();

				@Override
				public Thread newThread(Runnable r) {
					Thread t = defaultThreadFactory.newThread(r);

					t.setName(t.getName().replace("pool-", "global-similarity-pool-"));

					return t;
				}
			});
	private static final ScheduledThreadPoolExecutor globalForPool = new ScheduledThreadPoolExecutor(DEFAULT_THREADS);

	public static class BatchSize {
		int size;

		public BatchSize(final int size) {
			this.size = size;
		}
	}

	public static class BatchCount {
		long count;

		Integer minBatchSize;

		public BatchCount(final long count) {
			this.count = count;
		}

		public BatchCount(final long maxCount, final int minBatchSize) {
			this.count = maxCount;
			this.minBatchSize = minBatchSize;
		}

		public boolean hasMinBatchSize() {
			return this.minBatchSize != null;
		}
	}
	//
	// public static <V> Iterable<Iterable<V>> batchify(final BoundIterable<V>
	// elements, final BatchSize batchSize) {
	// if (batchSize.size <= 0)
	// throw new IllegalArgumentException();
	//
	// return new Iterable<Iterable<V>>() {
	//
	// @Override
	// public Iterator<Iterable<V>> iterator() {
	// return new BatchBoundIterator<>(elements, batchSize.size);
	// }
	// };
	// }

	private final ScheduledThreadPoolExecutor forPool;
	private final boolean shutdownThreadPoolOnFinish;

	/**
	 * Uses a global thread pool. Warning: Using this constructor may thus lead to
	 * deadlocks.
	 */
	public MyParallel() {
		this(globalForPool);
	}

	public MyParallel(final ScheduledThreadPoolExecutor threadPool) {
		super();
		this.forPool = threadPool;
		this.shutdownThreadPoolOnFinish = false;
	}

	public MyParallel(final int numberOfThreads, final String poolPrefix) {
		super();
		if (poolPrefix != null)
			this.forPool = new ScheduledThreadPoolExecutor(numberOfThreads, new ThreadFactory() {

				ThreadFactory defaultThreadFactory = Executors.defaultThreadFactory();

				@Override
				public Thread newThread(Runnable r) {
					Thread t = defaultThreadFactory.newThread(r);

					t.setName(t.getName().replace("pool", poolPrefix));

					return t;
				}
			});
		else
			this.forPool = new ScheduledThreadPoolExecutor(numberOfThreads);
		this.shutdownThreadPoolOnFinish = true;
	}

	public <T> ClosableBlockingQueue<Future<V>> ForQueue(final Iterable<T> elements, final int queueSize,
			final Operation<T, V> operation) {
		final ClosableBlockingQueue<Future<V>> q = new ClosableBlockingQueue<>(queueSize);

		new Thread("ClosableBlockingQueueMasterThread") {
			@Override
			public void run() {
				final Iterator<T> iterator = elements.iterator();
				try {
					boolean tryAgain = false;
					do {
						tryAgain = false;
						try {
							while (iterator.hasNext())
								q.put(MyParallel.this.forPool
										.submit(MyParallel.this.createCallable(iterator.next(), operation)));
						} catch (final OutOfMemoryError e) {
							if (MyParallel.this.forPool.getCorePoolSize() > 1) {
								MyParallel.this.forPool.setCorePoolSize(MyParallel.this.forPool.getCorePoolSize() - 1);
								tryAgain = true;
							} else {
								throw e;
							}
						} catch (final Exception e) {
							if (MyParallel.this.isOnExceptionShutdown(e)) {
								MyParallel.this.forPool.shutdownNow();
							} else
								e.printStackTrace();
						}
					} while (tryAgain);
				} finally {
					q.close();
					if (MyParallel.this.shutdownThreadPoolOnFinish) {
						MyParallel.this.forPool.shutdown();
						try {
							while (!MyParallel.this.forPool.awaitTermination(100, TimeUnit.MILLISECONDS))
								;
						} catch (InterruptedException e) {
						}

					}
				}
			};
		}.start();

		return q;
	}

	public <T> List<Future<V>> For(final Iterable<T> elements, final Operation<T, V> operation) {
		try {
			// invokeAll blocks for us until all submitted tasks in the call
			// complete
			try {
				return this.forPool.invokeAll(this.createCallables(elements, operation));
			} catch (final OutOfMemoryError e) {
				throw e;
			} catch (final Exception e) {
				e.printStackTrace();
				if (this.isOnExceptionShutdown(e)) {
					this.forPool.shutdownNow();
				}
			}
		} finally {
			if (this.shutdownThreadPoolOnFinish)
				this.forPool.shutdown();
		}
		return null;
	}

	protected boolean isOnExceptionShutdown(final Exception e) {
		return e instanceof InterruptedException || e instanceof RejectedExecutionException;
	}

	protected <T> Collection<Callable<V>> createCallables(final Iterable<T> elements, final Operation<T, V> operation) {
		final List<Callable<V>> callables = new LinkedList<>();
		for (final T elem : elements)
			callables.add(this.createCallable(elem, operation));

		return callables;
	}

	protected <T> Callable<V> createCallable(final T elem, final Operation<T, V> operation) {
		return new Callable<V>() {
			@Override
			public V call() throws Exception {
				return operation.perform(elem);
			}
		};
	}

	public interface Operation<T, V> {
		V perform(T pParameter) throws Exception;
	}
}
