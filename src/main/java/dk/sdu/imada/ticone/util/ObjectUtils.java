/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 12, 2019
 *
 */
public final class ObjectUtils {
	public static final boolean bothNullOrNone(Object o1, Object o2) {
		return (o1 != null) == (o2 != null);
	}
}
