/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 15, 2018
 *
 */
public interface IBuilder<T, FE extends FactoryException, IE extends CreateInstanceFactoryException> {

	/**
	 * Returns a copy of this builder.
	 * 
	 * @return
	 */
	IBuilder<T, FE, IE> copy();

	/**
	 * Uses all field values to construct a new instance.
	 * 
	 * @return
	 * @throws FactoryException               Indicates that the builder hasn't been
	 *                                        properly initialized for the creation
	 *                                        of instances.
	 * @throws CreateInstanceFactoryException Indicates that the creation of an
	 *                                        instance failed, albeit valid
	 *                                        initialization.
	 * @throws InterruptedException
	 */
	T build() throws FE, IE, InterruptedException;
}
