package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.Observable;

/**
 * This class is used to update the current status of the program, such that a
 * progress bar can be updated.
 *
 * Further it is used to stop the program, if necessary.
 *
 * @author Christian Nørskov
 */
public class Progress extends Observable implements Serializable, IProgress {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5014913556007340701L;
	private double progress = 0.0;
	private String progressMessage = null;
	private String title = null;

	private boolean status = true;

	@Override
	public void updateProgress(final Double progress, final String progressMessage) {
		this.updateProgress(progress, null, progressMessage);
	}

	@Override
	public void updateProgress(final Double progress, final String title, final String progressMessage) {
		if (progress != null)
			this.progress = progress;
		if (progressMessage != null)
			this.progressMessage = progressMessage;
		if (title != null)
			this.title = title;
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public double getProgress() {
		return this.progress;
	}

	@Override
	public String getProgressMessage() {
		return this.progressMessage;
	}

	@Override
	public synchronized void stop() {
		this.status = false;
	}

	@Override
	public synchronized void start() {
		this.status = true;
	}

	@Override
	public boolean getStatus() {
		return this.status;
	}

	@Override
	public String getTitle() {
		return this.title;
	}
}
