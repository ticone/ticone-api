/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 15, 2018
 *
 */
public class Pair<S, T> implements IPair<S, T>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5998501909801561057L;

	public static <S, T> Pair<S, T> of(final S first, final T second) {
		return new Pair<>(first, second);
	}

	public static <S, T> Pair<S, T> getPair(final S first, final T second) {
		return of(first, second);
	}

	protected S first;

	protected T second;

	public Pair(final S first, final T second) {
		super();
		this.first = first;
		this.second = second;
	}

	@Override
	public String toString() {
		return String.format("(%s,%s)", this.first, this.second);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof Pair)
			return Objects.equals(this.first, ((Pair<?, ?>) obj).first)
					&& Objects.equals(this.second, ((Pair<?, ?>) obj).second);
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.first, this.second);
	}

	@Override
	public S getFirst() {
		return this.first;
	}

	public S getLeft() {
		return this.getFirst();
	}

	@Override
	public T getSecond() {
		return this.second;
	}

	public T getRight() {
		return this.getSecond();
	}
}
