/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 12, 2019
 *
 */
public interface ITimePointWeighting {

	ITimePointWeighting copy();

	void setTimePointColumns(int columns);

	/**
	 * 
	 * @param tp     0-based index for the time-point
	 * @param weight
	 */
	void setWeightForTimePointColumn(int tp, double weight);

	double getWeightForTimePointColumn(int idx);

	int getTimePointColumns();

}