/**
 * 
 */
package dk.sdu.imada.ticone.util;

import dk.sdu.imada.ticone.clustering.DuplicateMappingForObjectException;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 19, 2018
 *
 */
public interface IObjectProviderFactory<S extends Object, P extends IObjectProvider> {
	P generateFrom(final S template) throws FactoryException, CreateInstanceFactoryException, InterruptedException,
			IncompatiblePrototypeException, SimilarityCalculationException, SimilarityValuesException,
			IncompatibleSimilarityValueException, DuplicateMappingForObjectException;
}