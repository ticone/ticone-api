/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 14, 2019
 *
 */
public class ObjectSampleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6231232589324083418L;

	public ObjectSampleException() {
		super();
	}

	public ObjectSampleException(String message) {
		super(message);
	}

	public ObjectSampleException(Throwable cause) {
		super(cause);
	}

}
