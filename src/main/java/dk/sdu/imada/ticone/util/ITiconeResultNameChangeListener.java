package dk.sdu.imada.ticone.util;

public interface ITiconeResultNameChangeListener {
	void resultNameChanged(final TiconeResultNameChangedEvent event);
}
