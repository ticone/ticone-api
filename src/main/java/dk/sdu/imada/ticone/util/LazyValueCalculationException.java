/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 30, 2018
 *
 */
public class LazyValueCalculationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7382518596702541597L;

	public LazyValueCalculationException() {
		super();
	}

	public LazyValueCalculationException(final String message) {
		super(message);
	}

	public LazyValueCalculationException(final Throwable cause) {
		super(cause);
	}

}
