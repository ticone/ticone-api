/**
 * 
 */
package dk.sdu.imada.ticone.util;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 10, 2018
 *
 */
public class FeatureValueProviderChangeEvent {
	protected IFeature.IFeatureValueProvider provider;

	public FeatureValueProviderChangeEvent(final IFeatureValueProvider provider) {
		super();
		this.provider = provider;
	}

	/**
	 * @return the provider
	 */
	public IFeature.IFeatureValueProvider getProvider() {
		return this.provider;
	}
}
