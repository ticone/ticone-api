/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 20, 2017
 *
 */
public interface INamedTiconeResult extends ITiconeResult {

	String getName();

	void setName(String newName);

	void addNameChangeListener(final ITiconeResultNameChangeListener l);

	void removeNameChangeListener(final ITiconeResultNameChangeListener listener);
}