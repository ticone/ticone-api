/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 15, 2018
 *
 */
public class IncorrectlyInitializedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1583687616719488446L;

	public IncorrectlyInitializedException(final String m) {
		super(String.format("This instance was incorrectly initialized: %s", m));
	}
}
