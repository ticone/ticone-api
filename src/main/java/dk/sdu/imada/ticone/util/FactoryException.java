/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * Indicates that a factory was not properly initialized to create instances.
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 4, 2018
 *
 */
public class FactoryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2413581724799488716L;

	public FactoryException() {
		super();
	}

	public FactoryException(final String message) {
		super(message);
	}

	public FactoryException(final Throwable cause) {
		super(cause);
	}

}
