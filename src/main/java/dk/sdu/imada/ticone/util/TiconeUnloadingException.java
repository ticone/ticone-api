/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 25, 2018
 *
 */
public class TiconeUnloadingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 493712809842562355L;

	public TiconeUnloadingException() {
		super();
	}

	public TiconeUnloadingException(final String message) {
		super(message);
	}

	public TiconeUnloadingException(final Throwable cause) {
		super(cause);
	}

}
