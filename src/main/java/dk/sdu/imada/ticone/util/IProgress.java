/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 6, 2017
 *
 */
public interface IProgress {

	void updateProgress(Double progress, String progressMessage);

	void updateProgress(Double progress, String title, String progressMessage);

	double getProgress();

	String getProgressMessage();

	void stop();

	void start();

	boolean getStatus();

	String getTitle();
}