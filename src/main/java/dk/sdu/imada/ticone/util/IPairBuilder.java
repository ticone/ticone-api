/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 15, 2018
 *
 */
public interface IPairBuilder<S, T, P extends IPair<S, T>>
		extends IBuilder<P, FactoryException, CreateInstanceFactoryException> {

	/**
	 * Multithreading safe
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	P build(S first, T second) throws FactoryException, CreateInstanceFactoryException, InterruptedException;

	@Override
	IPairBuilder<S, T, P> copy();

	IPairBuilder<S, T, P> setFirst(final S first);

	IPairBuilder<S, T, P> setSecond(final T second);
}
