/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * Indicates that a factory was not able to successfully complete construction
 * of an instance.
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 4, 2018
 *
 */
public class CreateInstanceFactoryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4595818227257608161L;

	public CreateInstanceFactoryException() {
		super();
	}

	public CreateInstanceFactoryException(final String message) {
		super(message);
	}

	public CreateInstanceFactoryException(final Throwable cause) {
		super(cause);
	}

}
