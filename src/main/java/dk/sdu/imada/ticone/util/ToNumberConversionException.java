/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 11, 2019
 *
 */
public class ToNumberConversionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9135458508453300969L;

	public ToNumberConversionException() {
		super();
	}

	public ToNumberConversionException(String message) {
		super(message);
	}

	public ToNumberConversionException(Throwable cause) {
		super(cause);
	}

}
