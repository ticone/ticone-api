/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * An {@link Iterable} with a size retrievable via {@link #longSize()}.
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 30, 2018
 *
 */
public interface IIterableWithSize<T> extends Iterable<T> {
	long longSize();
}
