/**
 * 
 */
package dk.sdu.imada.ticone.util;

import java.io.Serializable;

/**
 * Manages events and listeners of a single {@link IObjectProvider}.
 * 
 * @author Christian Wiwie
 * 
 * @since Nov 12, 2018
 *
 */
public interface IObjectProviderManager extends Serializable {

	void addObjectProviderListener(IObjectProviderListener listener);

	void fireObjectProviderChanged();

	void removeObjectProviderListener(IObjectProviderListener listener);

}
