/**
 * 
 */
package dk.sdu.imada.ticone.util;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 15, 2018
 *
 */
public class IncompatibleObjectProviderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3449390738298375290L;

	protected IObjectProvider provider;

	protected ObjectType objectType;

	public IncompatibleObjectProviderException(final IObjectProvider provider, final ObjectType objectType) {
		super(String.format("'%s' is not a provider for '%s'", provider.getClass().getSimpleName(),
				objectType.getBaseType().getSimpleName()));
		this.provider = provider;
		this.objectType = objectType;
	}

	/**
	 * @return the provider
	 */
	public IObjectProvider getProvider() {
		return this.provider;
	}

	/**
	 * @return the objectType
	 */
	public ObjectType getObjectType() {
		return this.objectType;
	}
}
