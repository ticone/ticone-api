/**
 * 
 */
package dk.sdu.imada.ticone.util;

/**
 * @author Christian Wiwie
 * 
 * @since Sep 27, 2018
 *
 */
public class ScalingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1016207548924032085L;

	/**
	 * @param string
	 */
	public ScalingException(final String string) {
		super(string);
	}

	/**
	 * @param cause
	 */
	public ScalingException(final Throwable cause) {
		super(cause);
	}

}
