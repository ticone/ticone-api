/**
 * 
 */
package dk.sdu.imada.ticone.util;

import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IFeatureValue;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;

/**
 * Indicates that the {@link IFeatureValueProvider} does not provide
 * {@link IFeatureValue}s for the specified {@link IFeature} or for the
 * specified type of {@link IObjectWithFeatures}.
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 10, 2018
 *
 */
public class IncompatibleFeatureValueProviderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2663965179521288925L;

	protected IFeature.IFeatureValueProvider provider;

	protected IFeature<?> feature;

	protected ObjectType<? extends IObjectWithFeatures> object;

	public IncompatibleFeatureValueProviderException(final IFeature.IFeatureValueProvider provider,
			final IFeature<?> feature) {
		super();
		this.provider = provider;
		this.feature = feature;
	}

	public IncompatibleFeatureValueProviderException(final IFeature.IFeatureValueProvider provider,
			final ObjectType<? extends IObjectWithFeatures> object) {
		super();
		this.provider = provider;
		this.object = object;
	}

	/**
	 * @return the provider
	 */
	public IFeature.IFeatureValueProvider getProvider() {
		return this.provider;
	}

	/**
	 * @return the feature
	 */
	public IFeature<?> getFeature() {
		return this.feature;
	}

	/**
	 * @return the object
	 */
	public ObjectType<? extends IObjectWithFeatures> getObject() {
		return this.object;
	}
}
