
package dk.sdu.imada.ticone.variance;

public interface IVariance {

	double calculateVariance(double[] signal);
}
