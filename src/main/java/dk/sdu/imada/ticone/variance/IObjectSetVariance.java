/**
 * 
 */
package dk.sdu.imada.ticone.variance;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 18, 2017
 *
 */
public interface IObjectSetVariance extends IVariance {

	/**
	 * Calculates the variance of an object set
	 *
	 * @param timeSeriesData the time series data to calculate the similarity of
	 * @return returns the variance of the object set
	 */
	double calculateObjectSetVariance(ITimeSeriesObject timeSeriesData);

}
