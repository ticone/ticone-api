/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.List;

import dk.sdu.imada.ticone.clustering.filter.IFilter;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;
import dk.sdu.imada.ticone.feature.store.INewFeatureStoreListener;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.permute.IShuffle;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.statistics.IPValueCalculationResult;
import dk.sdu.imada.ticone.statistics.IPValueResultStorageListener;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.IClusterStatusMapping;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.INamedTiconeResult;
import dk.sdu.imada.ticone.util.ITimePointWeighting;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 4, 2019
 *
 */
public interface ITiconeClusteringResult extends INamedTiconeResult {

	void addNewFeatureStoreListener(INewFeatureStoreListener listener);

	void removeNewFeatureStoreListener(INewFeatureStoreListener listener);

	boolean addClusteringIterationAddedListener(IClusteringIterationAddedListener listener);

	boolean removeClusteringIterationAddedListener(IClusteringIterationAddedListener listener);

	boolean addClusteringChangeListener(IClusteringChangeListener listener);

	boolean removeChangeListener(IClusteringChangeListener listener);

	boolean addClusteringIterationDeletionListener(IClusteringIterationDeletionListener listener);

	boolean removeClusteringIterationDeletionListener(IClusteringIterationDeletionListener listener);

	boolean addPValueResultStorageListener(IPValueResultStorageListener listener);

	boolean removePValueResultStorageListener(IPValueResultStorageListener listener);

	List<Integer> getIterations();

	<O extends IObjectWithFeatures> IPValueCalculationResult getPvalueCalculationResult(final int iteration);

	<O extends IObjectWithFeatures> IPValueCalculationResult getPvalueCalculationResult();

	void removeClusterPValues(final int iteration);

	IFilter<ICluster> getClusterFilter();

	void removeFeatureStore(final int iteration);

	IFeatureStore getFeatureStore(final int iteration);

	IFeatureStore getFeatureStore();

	boolean hasFeatureStore(final int iteration);

	int getClusteringNumber();

	IPreprocessingSummary<? extends IClusterObjectMapping> getPreprocessingSummary();

	IIdMapMethod getIdMapMethod();

	ITimePointWeighting getTimePointWeighting();

	IInitialClusteringProvider<? extends IClusterObjectMapping> getInitialClusteringProvider();

	ILoadDataMethod getLoadDataMethod();

	IClusteringMethodBuilder<? extends IClusteringMethod<? extends IClusterObjectMapping>> getClusteringMethodBuilder();

	ISimilarityFunction getSimilarityFunction();

	long getSeed();

	IShuffle getPermutationMethod();

	ITimeSeriesPreprocessor getTimeSeriesPreprocessor();

	IPrototypeBuilder getPrototypeBuilder();

	void resetToIteration(final int iteration) throws InterruptedException;

	IClusterHistory<? extends IClusterObjectMapping> getClusterHistory();

	IClusterObjectMapping getClusteringOfIteration(final int iteration);

	IClusterObjectMapping getLatestClustering();

	IClusterStatusMapping getClusterStatusMapping();

}