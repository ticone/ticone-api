/**
 * 
 */
package dk.sdu.imada.ticone.clustering.splitpattern;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public interface ISplitCluster {

	void setClustering(IClusterObjectMapping clustering);

	ISplitClusterContainer splitCluster(ICluster cluster) throws SplitClusterException, InterruptedException;

	IClusterObjectMapping applyNewClusters(ISplitClusterContainer splitPatternContainer) throws SplitClusterException;

}