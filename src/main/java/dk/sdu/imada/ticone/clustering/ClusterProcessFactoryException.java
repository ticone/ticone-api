/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.util.FactoryException;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 18, 2019
 *
 */
public class ClusterProcessFactoryException extends FactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3540786752557896088L;

	public ClusterProcessFactoryException() {
		super();
	}

	public ClusterProcessFactoryException(String message) {
		super(message);
	}

	public ClusterProcessFactoryException(Throwable cause) {
		super(cause);
	}

}
