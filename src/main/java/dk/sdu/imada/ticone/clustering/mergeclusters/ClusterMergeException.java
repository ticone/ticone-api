/**
 * 
 */
package dk.sdu.imada.ticone.clustering.mergeclusters;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class ClusterMergeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1357158788508125319L;

	/**
	 * 
	 */
	public ClusterMergeException(final Throwable e) {
		super(e);
	}
}
