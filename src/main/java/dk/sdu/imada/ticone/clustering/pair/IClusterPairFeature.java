/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import dk.sdu.imada.ticone.feature.IFeature;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 8, 2018
 *
 */
public interface IClusterPairFeature<R> extends IFeature<R> {

}
