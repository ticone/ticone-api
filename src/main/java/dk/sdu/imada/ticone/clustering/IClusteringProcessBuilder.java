package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;

import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeComponentException;
import dk.sdu.imada.ticone.prototype.MissingPrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityFunctionException;
import dk.sdu.imada.ticone.util.IBuilder;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.ITimePointWeighting;

public interface IClusteringProcessBuilder<CLUSTERING extends IClusterObjectMapping, R extends ITiconeClusteringResult>
		extends
		IBuilder<IClusteringProcess<CLUSTERING, R>, ClusterProcessFactoryException, CreateClusterProcessInstanceFactoryException>,
		Serializable {

	void setTimeSeriesPreprocessor(final ITimeSeriesPreprocessor timeSeriesPreprocessor);

	void setTimePointWeighting(final ITimePointWeighting timePointWeighting);

	void setSimilarityFunction(final ISimilarityFunction similarityFunction)
			throws IncompatiblePrototypeComponentException, MissingPrototypeFactoryException,
			IncompatibleSimilarityFunctionException;

	void setSeed(long seed);

	void setPrototypeBuilder(final IPrototypeBuilder prototypeFactory);

	void setPreprocessingSummary(final IPreprocessingSummary<CLUSTERING> preprocessingSummary);

	void setLoadDataMethod(final ILoadDataMethod loadDataMethod);

	void setInitialClusteringProvider(IInitialClusteringProvider<CLUSTERING> initialClusteringProvider);

	void setIdMapMethod(final IIdMapMethod idMapMethod);

	void setClusteringMethodBuilder(
			final IClusteringMethodBuilder<? extends IClusteringMethod<CLUSTERING>> initialClusteringInterface);

	void setResultFactory(ITiconeClusteringResultFactory<CLUSTERING, R> resultFactory);

	int getNumberOfObjectsInDataset();

	ITimeSeriesPreprocessor getTimeSeriesPreprocessor();

	ITimePointWeighting getTimePointWeighting();

	ISimilarityFunction getSimilarityFunction();

	long getSeed();

	IPrototypeBuilder getPrototypeBuilder();

	IPreprocessingSummary<CLUSTERING> getPreprocessingSummary();

	ILoadDataMethod getLoadDataMethod();

	IInitialClusteringProvider<CLUSTERING> getInitialClusteringProvider();

	IIdMapMethod getIdMapMethod();

	IClusteringMethodBuilder<? extends IClusteringMethod<CLUSTERING>> getClusteringMethodBuilder();

	ITiconeClusteringResultFactory<CLUSTERING, R> getResultFactory();
}
