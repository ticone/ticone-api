/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 18, 2019
 *
 */
public class CreateClusterProcessInstanceFactoryException extends CreateInstanceFactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1949794028667821609L;

	public CreateClusterProcessInstanceFactoryException() {
		super();
	}

	public CreateClusterProcessInstanceFactoryException(String message) {
		super(message);
	}

	public CreateClusterProcessInstanceFactoryException(Throwable cause) {
		super(cause);
	}

}
