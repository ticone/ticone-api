/**
 * 
 */
package dk.sdu.imada.ticone.clustering.validity;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 22, 2019
 *
 */
public class ValidityCalculationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -109003291151233539L;

	public ValidityCalculationException() {
		super();
	}

	public ValidityCalculationException(String message) {
		super(message);
	}

	public ValidityCalculationException(Throwable cause) {
		super(cause);
	}

}
