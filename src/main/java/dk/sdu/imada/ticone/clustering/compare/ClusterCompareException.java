/**
 * 
 */
package dk.sdu.imada.ticone.clustering.compare;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class ClusterCompareException extends CompareException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6391820348286540320L;

	/**
	 * 
	 */
	public ClusterCompareException(final Throwable cause) {
		super(cause);
	}
}
