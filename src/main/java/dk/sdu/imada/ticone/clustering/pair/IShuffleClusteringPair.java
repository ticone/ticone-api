/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.IShuffle;

/**
 * @author Christian Wiwie
 * 
 * @since May 5, 2017
 *
 */
public interface IShuffleClusteringPair extends IShuffle {

	@Override
	ObjectType<IClusterObjectMappingPair> supportedObjectType();

	@Override
	IShuffleClusteringPair copy();

	@Override
	IShuffleClusteringPair newInstance();
}