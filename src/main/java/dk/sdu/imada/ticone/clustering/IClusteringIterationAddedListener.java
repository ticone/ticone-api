/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 6, 2019
 *
 */
public interface IClusteringIterationAddedListener {
	void clusteringIterationAdded(final ClusteringIterationAddedEvent event);
}
