/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public class ClusteringChangeEvent {
	protected ITiconeClusteringResult clustering;

	public ClusteringChangeEvent(final ITiconeClusteringResult clustering) {
		super();
		this.clustering = clustering;
	}

	/**
	 * @return the clustering
	 */
	public ITiconeClusteringResult getClustering() {
		return this.clustering;
	}
}
