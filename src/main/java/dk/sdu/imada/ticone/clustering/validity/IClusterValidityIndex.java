/**
 * 
 */
package dk.sdu.imada.ticone.clustering.validity;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 5, 2019
 *
 */
public interface IClusterValidityIndex {

	double getValidity(final IClusterObjectMapping clustering, final ISimilarityFunction similarityFunction)
			throws InterruptedException, ValidityCalculationException;
}
