/**
 * 
 */
package dk.sdu.imada.ticone.clustering.mergeclusters;

import java.util.Collection;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.clustering.IClusters;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 13, 2019
 *
 */
public class ClusterMergeResult<C extends IClusterObjectMapping> {
	final protected C resultClustering;
	final protected Collection<? extends IClusters> mergedClusterSets;

	public ClusterMergeResult(final C resultClustering, final Collection<? extends IClusters> mergedClusterSets) {
		super();
		this.resultClustering = resultClustering;
		this.mergedClusterSets = mergedClusterSets;
	}

	/**
	 * @return the resultClustering
	 */
	public C getResultClustering() {
		return this.resultClustering;
	}

	/**
	 * @return the mergedClusters
	 */
	public Collection<? extends IClusters> getMergedClusterSets() {
		return this.mergedClusterSets;
	}
}
