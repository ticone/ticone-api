/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.permute.IPermutable;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public interface ICluster extends IObjectWithFeatures, IObjectProvider, IPermutable {

	@Override
	default ObjectType<ICluster> getObjectType() {
		return ObjectType.CLUSTER;
	}

	@Override
	default Collection<ObjectType<?>> providedTypesOfObjects() {
		return Arrays.asList(ObjectType.OBJECT);
	}

	@Override
	default <T extends IObjectWithFeatures> int getNumberObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type == ObjectType.OBJECT)
			return size();
		return IObjectProvider.super.getNumberObjectsOfType(type);
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type == ObjectType.OBJECT)
			return (Collection<T>) getObjects();
		return IObjectProvider.super.getObjectsOfType(type);
	}

	IClusterObjectMapping getClustering();

	void setName(String name);

	/**
	 * This is for CLARA clustering, since a pattern is already calculated, this
	 * will be used for recalculation.
	 * 
	 * @param pattern
	 */
	void updatePrototype(IPrototype pattern);

	void setParent(ICluster parent);

	ICluster getParent();

	IPrototype getPrototype();

	void setKeep(boolean keep);

	boolean isKeep();

	int getClusterNumber();

	/**
	 * Returns a unique ID over all clusters.
	 */
	long getInternalClusterId();

	IClusterList asSingletonList();

	void addObject(final ITimeSeriesObject object, final ISimilarityValue similarity)
			throws DuplicateMappingForObjectException;

	void removeObjects(final ITimeSeriesObjects object);

	boolean containsObject(final ITimeSeriesObject object);

	ISimilarityValue getSimilarity(final ITimeSeriesObject object) throws ClusterObjectMappingNotFoundException;

	Map<ITimeSeriesObject, ISimilarityValue> getSimilarities();

	/**
	 * The number of objects.
	 * 
	 * @return
	 */
	int size();

	ITimeSeriesObjectList getObjects();
}