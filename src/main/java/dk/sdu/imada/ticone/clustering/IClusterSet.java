/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Set;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 12, 2018
 *
 */
public interface IClusterSet extends IClusters, Set<ICluster> {
	@Override
	IClusterSet copy();

	@Override
	default IClusterSet asSet() {
		return this;
	}
}
