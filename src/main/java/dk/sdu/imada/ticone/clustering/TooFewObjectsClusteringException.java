package dk.sdu.imada.ticone.clustering;

public class TooFewObjectsClusteringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5731124735723395148L;

	public TooFewObjectsClusteringException(final String message) {
		super(message);
	}

	public TooFewObjectsClusteringException(final Throwable cause) {
		super(cause);
	}
}
