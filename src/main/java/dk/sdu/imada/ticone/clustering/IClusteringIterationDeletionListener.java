/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since May 12, 2017
 *
 */
public interface IClusteringIterationDeletionListener {
	void clusteringIterationDeleted(final ClusteringIterationDeletedEvent event);
}
