/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;

public class CreateClusteringMethodInstanceFactoryException extends CreateInstanceFactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1957111386844338429L;

	public CreateClusteringMethodInstanceFactoryException() {
		super();
	}

	public CreateClusteringMethodInstanceFactoryException(final String message) {
		super(message);
	}

	public CreateClusteringMethodInstanceFactoryException(final Throwable cause) {
		super(cause);
	}

}