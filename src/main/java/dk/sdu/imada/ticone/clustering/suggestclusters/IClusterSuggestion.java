/**
 * 
 */
package dk.sdu.imada.ticone.clustering.suggestclusters;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public interface IClusterSuggestion {

	void keepPattern(ICluster pattern, boolean keep);

	boolean getKeepStatusForPattern(ICluster pattern);

	IClusterObjectMapping getNewClusterObjectMapping();

	IClusterObjectMapping getOldClusterObjectMapping();

	ITimeSeriesObjects getObjectsToDelete();

}