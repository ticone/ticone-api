package dk.sdu.imada.ticone.clustering.splitpattern;

public class SplitClusterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5038758203297901403L;

	public SplitClusterException(final String message) {
		super(message);
	}

	public SplitClusterException(final Throwable cause) {
		super(cause);
	}
}
