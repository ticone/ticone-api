/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 16, 2019
 *
 */
public class ClusterSelectionChangeEvent {
	final protected IClusterObjectMapping clustering;

	final protected IClusters newlySelectedClusters, newlyUnselectedClusters;

	public ClusterSelectionChangeEvent(IClusterObjectMapping clustering, IClusters newlySelectedClusters,
			IClusters newlyUnselectedClusters) {
		super();
		this.clustering = clustering;
		this.newlySelectedClusters = newlySelectedClusters;
		this.newlyUnselectedClusters = newlyUnselectedClusters;
	}

	/**
	 * @return the newlySelectedClusters
	 */
	public IClusters getNewlySelectedClusters() {
		return this.newlySelectedClusters;
	}

	/**
	 * @return the newlyUnselectedClusters
	 */
	public IClusters getNewlyUnselectedClusters() {
		return this.newlyUnselectedClusters;
	}

	/**
	 * @return the clustering
	 */
	public IClusterObjectMapping getClustering() {
		return this.clustering;
	}
}
