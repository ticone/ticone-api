/**
 * 
 */
package dk.sdu.imada.ticone.clustering.mergeclusters;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 13, 2019
 *
 */
public interface IMergeClusters {
	<C extends IClusterObjectMapping> ClusterMergeResult<C> doMerge(C clustering)
			throws ClusterMergeException, InterruptedException;
}
