/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;

public class CreateClusterInstanceFactoryException extends CreateInstanceFactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8557187469523109011L;

	public CreateClusterInstanceFactoryException() {
		super();
	}

	public CreateClusterInstanceFactoryException(final String message) {
		super(message);
	}

	public CreateClusterInstanceFactoryException(final Throwable cause) {
		super(cause);
	}

}