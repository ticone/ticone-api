/**
 * 
 */
package dk.sdu.imada.ticone.clustering.filter;

import java.io.Serializable;

import dk.sdu.imada.ticone.feature.IArithmeticFeature;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.store.IFeatureStore;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 26, 2017
 *
 */
public interface IFilter<T extends IObjectWithFeatures> extends Serializable {

	public enum FILTER_OPERATOR {
		LESS, LESS_OR_EQUAL, EQUAL, EQUAL_OR_GREATER, GREATER, NOT_EQUAL;

		@Override
		public String toString() {
			switch (this) {
			case LESS:
				return "<";
			case LESS_OR_EQUAL:
				return "<=";
			case EQUAL:
				return "==";
			case EQUAL_OR_GREATER:
				return ">=";
			case GREATER:
				return ">";
			case NOT_EQUAL:
				return "!=";
			default:
				throw new IllegalArgumentException();
			}
		}
	}

	double getFilterInput();

	FILTER_OPERATOR getFilterOperator();

	IArithmeticFeature<?> getFeature();

	void setActive(boolean isActive);

	boolean isActive();

	void setFilterInput(double filterInput);

	void setFilterOperator(FILTER_OPERATOR filterOperator);

	void setFeature(IArithmeticFeature<?> feature);

	boolean check(final IFeatureStore featureStore, final T object);

	void addFilterListener(final IFilterListener l);

	void removeFilterListener(final IFilterListener l);

	void fireFilterChanged();
}
