/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public interface IClusteringChangeListener extends Serializable {
	void clusteringChanged(ClusteringChangeEvent e);
}
