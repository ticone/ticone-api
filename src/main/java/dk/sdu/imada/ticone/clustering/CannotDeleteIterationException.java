/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since May 11, 2017
 *
 */
public class CannotDeleteIterationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7560071024802435403L;

	protected IClusteringProcess clustering;
	protected int firstIterationToKeep;

	public CannotDeleteIterationException(final IClusteringProcess clustering, final int firstIterationToKeep,
			final String message) {
		super(message);
		this.clustering = clustering;
		this.firstIterationToKeep = firstIterationToKeep;
	}

	/**
	 * @return the clustering
	 */
	public IClusteringProcess getClustering() {
		return this.clustering;
	}

	/**
	 * @return the firstIterationToKeep
	 */
	public int getFirstIterationToKeep() {
		return this.firstIterationToKeep;
	}
}
