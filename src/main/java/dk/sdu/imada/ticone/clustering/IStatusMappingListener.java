/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public interface IStatusMappingListener extends Serializable {

	/**
	 * 
	 */
	void clusterStatusMappingChanged(final StatusMappingEvent e);

}
