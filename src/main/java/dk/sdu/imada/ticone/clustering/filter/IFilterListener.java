/**
 * 
 */
package dk.sdu.imada.ticone.clustering.filter;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 26, 2017
 *
 */
public interface IFilterListener extends Serializable {

	void filterChanged(final FilterEvent e);
}
