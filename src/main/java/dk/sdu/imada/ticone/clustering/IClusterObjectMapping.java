/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Collection;
import java.util.Map;

import dk.sdu.imada.ticone.clustering.pair.IClusterPairs;
import dk.sdu.imada.ticone.data.IObjectClusterPairs;
import dk.sdu.imada.ticone.data.IObjectPairs;
import dk.sdu.imada.ticone.data.ITimeSeriesObject;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectSet;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.IFeature;
import dk.sdu.imada.ticone.feature.IFeature.IFeatureValueProvider;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.permute.IPermutable;
import dk.sdu.imada.ticone.prototype.CreatePrototypeInstanceFactoryException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.prototype.IncompatiblePrototypeException;
import dk.sdu.imada.ticone.prototype.PrototypeFactoryException;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.similarity.IncompatibleSimilarityValueException;
import dk.sdu.imada.ticone.similarity.NoComparableSimilarityValuesException;
import dk.sdu.imada.ticone.similarity.SimilarityCalculationException;
import dk.sdu.imada.ticone.similarity.SimilarityValuesException;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.Progress;
import it.unimi.dsi.fastutil.ints.IntSet;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public interface IClusterObjectMapping
		extends IPermutable, IObjectProvider, IFeatureValueProvider, IObjectWithFeatures {

	@Override
	IClusterObjectMapping copy() throws InterruptedException;

	@Override
	default ObjectType<IClusterObjectMapping> getObjectType() {
		return ObjectType.CLUSTERING;
	}

	enum DELETE_METHOD {
		ONLY_PROTOTYPE, ONLY_OBJECTS, BOTH_PROTOTYPE_AND_OBJECTS
	}

	@Override
	default boolean initializeForFeature(IFeature<?> feature) {
		return true;
	}

	boolean equalObjectPartition(IClusterObjectMapping other);

	boolean addAll(Collection<? extends ICluster> elems);

	/**
	 * This is the function used in clustering with Overrepresented patterns. Some
	 * extra data is kept, namely a list for each pattern, with all timeseries
	 * objects assigned to said pattern.
	 * 
	 * @param timeSeriesData
	 * @param pattern
	 * @param coefficient
	 * @throws DuplicateMappingForObjectException
	 */
	void addMapping(ITimeSeriesObject timeSeriesData, ICluster pattern, ISimilarityValue coefficient)
			throws DuplicateMappingForObjectException;

	void addMappings(Map<ITimeSeriesObject, Map<ICluster, ISimilarityValue>> coefficients)
			throws DuplicateMappingForObjectException;

	/**
	 * Adds the patterns, and its mappings, from the given
	 * <tt>IClusterObjectMapping</tt> to this mapping.
	 * 
	 * @param patternObjectMapping
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws DuplicateMappingForObjectException
	 */
	void mergeMappings(IClusterObjectMapping patternObjectMapping) throws CreateClusterInstanceFactoryException,
			InterruptedException, ClusterFactoryException, DuplicateMappingForObjectException;

	/**
	 * Gets all the coefficients for the given objects
	 * 
	 * @param objectName
	 * @return
	 */
	Map<ICluster, ISimilarityValue> getSimilarities(ITimeSeriesObject object);

	/**
	 *
	 * @param objectName
	 * @return
	 */
	ITimeSeriesObject getTimeSeriesData(String objectName);

	ITimeSeriesObjectList getAllObjects();

	ITimeSeriesObjectList getAssignedObjects();

	ITimeSeriesObjectList getUnassignedObjects();

	IClusterSet getClusters();

	long getClusteringId();

	/**
	 * Delete data mapped to a cluster. Option decides whether to delete only
	 * prototype, data objects or both.
	 * 
	 * @param clusterNumber The number of the cluster to delete and/or delete all
	 *                      objects from.
	 * @param delete_method delete_method is a enum that specifies what to delete
	 *                      (pattern, objects or both).
	 *
	 */
	void removeData(int clusterNumber, DELETE_METHOD delete_method);

	/**
	 * Deletes the specified objects from the specified pattern.
	 * 
	 * @param pattern         The pattern to remove the data from
	 * @param timeSeriesDatas The data to remove from the pattern
	 */
	void removeObjectsFromCluster(ICluster pattern, ITimeSeriesObjects timeSeriesDatas);

	/**
	 * Deletes the specified objects from the clustering and all its clusters.
	 * 
	 * @param objects The objects to remove from the clustering.
	 */
	void removeObjects(ITimeSeriesObjects objects);

	/**
	 * Adds a new cluster to this clustering with the specified {@link IPrototype}.
	 * <p>
	 * If another cluster is already present with a prototype that is equal to the
	 * specified one, the objects are added to the existing cluster and that cluster
	 * is returned instead.
	 * </p>
	 * 
	 * @param prototype
	 * @return The newly added cluster with the specified prototype if successful,
	 *         otherwise null.
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws IncompatiblePrototypeException
	 */
	ICluster addCluster(IPrototype prototype) throws ClusterFactoryException, CreateClusterInstanceFactoryException,
			InterruptedException, IncompatiblePrototypeException;

	/**
	 * Adds a new cluster to this clustering with the specified objects.
	 * <p>
	 * The specified similarity function is used to calculate object-cluster
	 * similarities.
	 * </p>
	 * 
	 * @param prototype
	 * @return The newly added cluster with the specified objects if successful,
	 *         otherwise null.
	 * @throws InterruptedException
	 * @throws CreateClusterInstanceFactoryException
	 * @throws ClusterFactoryException
	 * @throws DuplicateMappingForObjectException
	 * @throws IncompatiblePrototypeException
	 */
	ICluster addCluster(final ITimeSeriesObjects objects, final ISimilarityFunction simFunc)
			throws PrototypeFactoryException, CreatePrototypeInstanceFactoryException, InterruptedException,
			ClusterFactoryException, CreateClusterInstanceFactoryException, SimilarityCalculationException,
			SimilarityValuesException, IncompatibleSimilarityValueException, DuplicateMappingForObjectException;

	/**
	 *
	 * @return
	 * @throws InterruptedException
	 */
	IClusterObjectMappingCopy getCopy() throws InterruptedException;

	/**
	 * Returns a new, empty {@link IClusterObjectMapping} instance initialized with
	 * a copy of this instances {@link IClusterBuilder}.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	IClusterObjectMapping newInstance(ITimeSeriesObjectSet allObjects) throws InterruptedException;

	void removeObjectsNotInNetwork(final ITiconeNetwork network);

	IObjectPartition asObjectPartition();

	IClusterPairs getClusterPairs();

	IObjectPairs getObjectPairs();

	IObjectClusterPairs getObjectClusterPairs();

	IPrototypeBuilder getPrototypeBuilder();

	/**
	 * Updates all clusters' {@link IPrototype} according to their objects and the
	 * clustering's {@link IPrototypeBuilder}.
	 * 
	 * @throws PrototypeFactoryException
	 * @throws CreatePrototypeInstanceFactoryException
	 * @throws InterruptedException
	 */
	void updatePrototypes()
			throws PrototypeFactoryException, CreatePrototypeInstanceFactoryException, InterruptedException;

	int removeDuplicatePrototypes(final boolean ignoreKeep);

	int removeDuplicatePrototypes();

	void addObjectsToMostSimilarCluster(final ITimeSeriesObjects objects, final ISimilarityFunction similarity,
			final Progress progress) throws SimilarityCalculationException, InterruptedException,
			SimilarityValuesException, NoComparableSimilarityValuesException, IncompatibleSimilarityValueException;

	void addObjectsToMostSimilarCluster(final ITimeSeriesObjects objects, final ISimilarityFunction similarity)
			throws SimilarityCalculationException, InterruptedException, SimilarityValuesException,
			NoComparableSimilarityValuesException, IncompatibleSimilarityValueException;

	int removeEmptyClusters(final boolean ignoreKeep);

	int removeEmptyClusters();

	ICluster getCluster(final int clusterNumber);

	IntSet getClusterNumbers();

	void removeCluster(final ICluster cluster);
}