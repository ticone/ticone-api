/**
 * 
 */
package dk.sdu.imada.ticone.clustering.filter;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 26, 2017
 *
 */
public class FilterEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8092969304552039254L;
	protected IFilter<?> filter;

	public FilterEvent(final IFilter<?> filter) {
		super();
		this.filter = filter;
	}

	/**
	 * @return the filter
	 */
	public IFilter<?> getFilter() {
		return this.filter;
	}
}
