package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;
import java.util.Set;

import dk.sdu.imada.ticone.clustering.splitpattern.ISplitCluster;
import dk.sdu.imada.ticone.clustering.splitpattern.ISplitClusterContainer;
import dk.sdu.imada.ticone.clustering.splitpattern.SplitClusterException;
import dk.sdu.imada.ticone.clustering.suggestclusters.IClusterSuggestion;
import dk.sdu.imada.ticone.clustering.suggestclusters.ISuggestNewCluster;
import dk.sdu.imada.ticone.clustering.suggestclusters.SuggestClusterException;
import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.feature.IFeatureCalculationManager;
import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkEdge;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.preprocessing.PreprocessingException;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.statistics.ICalculatePValues;
import dk.sdu.imada.ticone.statistics.IPValueCalculationListener;
import dk.sdu.imada.ticone.statistics.IPValueCalculationResult;
import dk.sdu.imada.ticone.statistics.PValueCalculationException;
import dk.sdu.imada.ticone.util.IActionContainer;
import dk.sdu.imada.ticone.util.IClusterHistory;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.IProgress;
import dk.sdu.imada.ticone.util.ITimePointWeighting;

/**
 * This is the interface shared between the backend and the visualizations that
 * allows to retrieve the assignment of objects to the predefined patterns.
 *
 * @author Christian Wiwie
 * @author Christian Nørskov
 * @since 2015-03-27
 *
 */

public interface IClusteringProcess<CLUSTERING extends IClusterObjectMapping, R extends ITiconeClusteringResult>
		extends IFeatureCalculationManager, Serializable {

	boolean start() throws InterruptedException, ClusterOperationException;

	/**
	 * Compute one iteration of the timeseries clustering
	 * 
	 * @return
	 * @throws TooFewObjectsClusteringException
	 * @throws InterruptedException
	 */
	CLUSTERING doIteration() throws TooFewObjectsClusteringException, ClusterOperationException, InterruptedException;

	/**
	 * Returns the observable object progress, which gives the possibility to see
	 * the progress.
	 * 
	 * @return The Progress objects
	 */
	IProgress getProgress();

	/**
	 * <p>
	 * Add an empty cluster with the specified prototype.
	 * </p>
	 * <p>
	 * If the prototype is valid and a new cluster is added, it will be set to
	 * 'keep' via {@link ICluster#setKeep(boolean)}.
	 * </p>
	 * 
	 * @see #getPrototypeBuilder()
	 * @see ICluster#setKeep(boolean)
	 * @param prototype The prototype of the new cluster.
	 * @return The new {@link ICluster} created from the passed {@link IPrototype}
	 *         if it was valid, null otherwise.
	 * @throws InterruptedException
	 * @throws ClusterOperationException If the specified prototype has components
	 *                                   that are not compatible to the prototypes
	 *                                   of this process.
	 */
	ICluster addCluster(IPrototype prototype) throws ClusterOperationException, InterruptedException;

	/**
	 * Will suggest new patterns with the given SuggestNewPatternInterface, and
	 * return them in the SuggestPatternsContainer, which will hold the old
	 * patterns, and the new.
	 * 
	 * @param suggestCluster a class that implements the suggestNewPatternInterface,
	 *                       which suggests new patterns.
	 * @return returns a container for the old and new patterns.
	 * @throws TooFewObjectsClusteringException
	 * @throws InterruptedException
	 */
	IClusterSuggestion suggestNewClusters(ISuggestNewCluster suggestCluster) throws SuggestClusterException,
			TooFewObjectsClusteringException, ClusterOperationException, InterruptedException;

	/**
	 *
	 * @param suggestCluster
	 * @param clusterSuggestion
	 * @return
	 * @throws InterruptedException
	 */
	void applySuggestedClusters(ISuggestNewCluster suggestCluster, IClusterSuggestion clusterSuggestion)
			throws ClusterOperationException, InterruptedException;

	/**
	 *
	 * @param splitCluster
	 * @param clusterNumber The number of the cluster to split.
	 * @return
	 * @throws SplitClusterException
	 * @throws TooFewObjectsClusteringException
	 */
	ISplitClusterContainer splitCluster(ISplitCluster splitCluster, int clusterNumber) throws SplitClusterException,
			TooFewObjectsClusteringException, ClusterOperationException, InterruptedException;

	/**
	 * Apply the found patterns, which were found with the splitPatternInterface.
	 * 
	 * @param splitCluster
	 * @param splitClusterContainer
	 * @throws InterruptedException
	 */
	void applySplitClusters(ISplitCluster splitCluster, ISplitClusterContainer splitClusterContainer)
			throws ClusterOperationException, InterruptedException;

	/**
	 * Get the current history.
	 * 
	 * @return returns the current history.
	 */
	IClusterHistory<CLUSTERING> getHistory();

	void appendHistory(IClusterHistory<CLUSTERING> history);

	/**
	 *
	 * @return
	 */
	ISimilarityValue getAverageObjectClusterSimilarity();

	/**
	 * Resets this clustering process to the specified iteration.
	 * 
	 * @param iteration The iteration to reset to.
	 * @throws InterruptedException
	 */
	void resetToIteration(int iteration) throws InterruptedException;

	/**
	 * This function deletes data depending on the delete method; either deletes the
	 * pattern, the objects or both.
	 * 
	 * @param clusterNumber The number of the cluster to delete and/or delete all
	 *                      objects from.
	 * @param deleteMethod  deleteMethod takes values from the enum DELETE_METHOD,
	 *                      and is either: ONLY_PATTERN, ONLY_OBJECTS or
	 *                      BOTH_PATTERN_AND_OBJECTS.
	 * @throws InterruptedException
	 */
	void deleteData(int clusterNumber, IClusterObjectMapping.DELETE_METHOD deleteMethod)
			throws ClusterOperationException, InterruptedException;

	/**
	 * Delete objects from clusters with the specified cluster numbers. That is, if
	 * an object belongs to a cluster with any of the specified numbers it will be
	 * removed from the mapping.
	 * <p>
	 * If no cluster numbers are specified, objects are removed from all clusters.
	 * </p>
	 * 
	 * @param clusterNumbers the numbers of clusters to remove the objects from.
	 * @param objects        the objects to remove if they are contained in the
	 *                       specified clusters.
	 * @throws ClusterOperationException
	 * @throws InterruptedException
	 */
	void removeObjectsFromClusters(ITimeSeriesObjects objects, int... clusterNumbers)
			throws ClusterOperationException, InterruptedException;

	ITimeSeriesObjects getObjects();

	/**
	 *
	 * @return
	 */
	CLUSTERING getLatestClustering();

	/**
	 * Creates a new cluster with the objects of clusters with the specified
	 * numbers.
	 * 
	 * @param clusterNumbers the numbers of clusters that should be merged.
	 * @throws InterruptedException
	 */
	void mergeClusters(int... clusterNumbers) throws ClusterOperationException, InterruptedException;

	/**
	 * Transitively merges clusters exceeding the specified similarity threshold.
	 * 
	 * @param threshold A comparable similarity value used as threshold.
	 * @throws InterruptedException
	 */
	void mergeClusters(ISimilarityValue threshold) throws ClusterOperationException, InterruptedException;

	void createClusterWithObjects(ITimeSeriesObjects objects) throws ClusterOperationException, InterruptedException;

	IPValueCalculationResult getPValues();

	IPValueCalculationResult getPValues(int iteration);

	void calculatePValues(ICalculatePValues calculateClusterPValues, long seed)
			throws InterruptedException, PValueCalculationException;

	void deleteOldIterations(int firstIterationToKeep) throws CannotDeleteIterationException;

	R getClusteringResult();

	boolean isFirstIteration();

	boolean isStarted();

	void resetActionsToApplyBeforeNextIteration();

	void addNewActionsToApplyBeforeNextIteration(final IActionContainer actionContainer);

	void applyActionsBeforeNextIteration() throws ClusterOperationException, InterruptedException;

	IClusteringMethodBuilder<? extends IClusteringMethod<CLUSTERING>> getClusteringMethodBuilder();

	int getCurrentIteration();

	long getSeed();

	ITimeSeriesPreprocessor getTimeSeriesPreprocessor();

	ITimePointWeighting getTimePointWeighting();

	IPrototypeBuilder getPrototypeBuilder();

	IPreprocessingSummary<CLUSTERING> getPreprocessingSummary();

	ILoadDataMethod getLoadDataMethod();

	IInitialClusteringProvider<CLUSTERING> getInitialClusteringProvider();

	IIdMapMethod getIdMapMethod();

	ISimilarityFunction getSimilarityFunction();

	int getNumberOfInitialClusters();

	ITimeSeriesObjects removeDataNotInNetwork(
			final ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge<?>> network)
			throws PreprocessingException;

	int calculateNumberOfMappedObjects(
			final ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge> network);

	Set<String> getUnmappedNodes();

	boolean addPValueCalculationListener(IPValueCalculationListener listener);

	boolean removePValueCalculationListener(IPValueCalculationListener listener);
}
