package dk.sdu.imada.ticone.clustering.suggestclusters;

public class SuggestClusterException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5977289108229137167L;

	public SuggestClusterException(final String message) {
		super(message);
	}

	public SuggestClusterException(final Throwable cause) {
		super(cause);
	}
}
