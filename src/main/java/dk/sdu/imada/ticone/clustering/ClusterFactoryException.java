/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.util.FactoryException;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 19, 2018
 *
 */
public class ClusterFactoryException extends FactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7609325742898577515L;

	public ClusterFactoryException() {
		super();
	}

	public ClusterFactoryException(final String message) {
		super(message);
	}

	public ClusterFactoryException(final Throwable cause) {
		super(cause);
	}

}
