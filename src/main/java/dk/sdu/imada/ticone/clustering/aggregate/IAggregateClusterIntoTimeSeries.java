package dk.sdu.imada.ticone.clustering.aggregate;

import dk.sdu.imada.ticone.data.ITimeSeries;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 15, 2019
 */
public interface IAggregateClusterIntoTimeSeries extends IAggregateCluster<ITimeSeries> {

	@Override
	IAggregateClusterIntoTimeSeries copy();
}
