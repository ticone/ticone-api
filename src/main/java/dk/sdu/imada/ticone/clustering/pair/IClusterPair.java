/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.util.IPair;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 29, 2017
 *
 */
public interface IClusterPair extends IPair<ICluster, ICluster>, IObjectWithFeatures {

	@Override
	default ObjectType<IClusterPair> getObjectType() {
		return ObjectType.CLUSTER_PAIR;
	}

}