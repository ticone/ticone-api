/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;

/**
 * @author Christian Wiwie
 * 
 * @since Dec 7, 2018
 *
 */
public interface IClusterObjectMappingCopy extends Serializable {

	/**
	 * @return the original
	 */
	IClusterObjectMapping getOriginal();

	IClusterObjectMapping getCopy();

	ICluster getOriginal(ICluster copy);

	ICluster getCopy(ICluster original);

}