/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class ClusterOperationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 204005439201084185L;

	/**
	 * 
	 */
	public ClusterOperationException(final Throwable e) {
		super(e);
	}

	public ClusterOperationException(String message) {
		super(message);
	}

}
