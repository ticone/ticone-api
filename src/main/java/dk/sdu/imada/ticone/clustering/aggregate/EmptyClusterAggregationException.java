/**
 * 
 */
package dk.sdu.imada.ticone.clustering.aggregate;

import dk.sdu.imada.ticone.util.AggregationException;

/**
 * Thrown if aggregation is attempted for an empty cluster.
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 18, 2018
 *
 */
public class EmptyClusterAggregationException extends AggregationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7395293501642225098L;

	public EmptyClusterAggregationException(final String message) {
		super(message);
	}

	public EmptyClusterAggregationException(final Throwable e) {
		super(e);
	}
}
