/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since May 2, 2017
 *
 */
public class ClusterNotInClusteringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3073139307209731851L;

	protected ICluster cluster;
	protected IClusterObjectMapping clustering;

	public ClusterNotInClusteringException(final ICluster cluster, final IClusterObjectMapping clustering) {
		super();
		this.cluster = cluster;
		this.clustering = clustering;
	}

	/**
	 * @return the cluster
	 */
	public ICluster getCluster() {
		return this.cluster;
	}

	/**
	 * @return the clustering
	 */
	public IClusterObjectMapping getClustering() {
		return this.clustering;
	}
}
