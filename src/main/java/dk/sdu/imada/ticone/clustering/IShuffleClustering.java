/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.IShuffle;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public interface IShuffleClustering extends IShuffle {

	@Override
	ObjectType<IClusterObjectMapping> supportedObjectType();

	@Override
	IShuffleClustering copy();

	@Override
	IShuffleClustering newInstance();
}
