/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IPairList;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectSampleException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 14, 2019
 *
 */
public interface IClusterPairList extends IPairList<ICluster, ICluster, IClusterPair>, IClusterPairs {

	@Override
	IClusterPairList copy();

	@Override
	default IClusterPairList asList() {
		return this;
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type.equals(ObjectType.CLUSTER_PAIR))
			return (Collection<T>) this;
		return IClusterPairs.super.getObjectsOfType(type);
	}

	@Override
	default <T extends IObjectWithFeatures> List<T> sampleObjectsOfType(ObjectType<T> type, int sampleSize, long seed)
			throws IncompatibleObjectProviderException, ObjectSampleException {
		if (type == ObjectType.CLUSTER_PAIR) {
			final int[] indices = new Random(seed).ints(sampleSize, 0, this.size()).toArray();
			final List<IClusterPair> result = new ArrayList<>();
			for (int i : indices)
				result.add(this.get(i));
			return (List<T>) result;
		}
		throw new ObjectSampleException();
	}

}
