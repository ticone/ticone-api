/**
 * 
 */
package dk.sdu.imada.ticone.clustering.compare;

/**
 * @author Christian Wiwie
 * 
 * @since May 4, 2017
 *
 */
public class CompareException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1920379123009715188L;

	/**
	 * 
	 */
	public CompareException() {
		super();
	}

	/**
	 * @param message
	 */
	public CompareException(final String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public CompareException(final Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CompareException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CompareException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}