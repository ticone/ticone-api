/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.Arrays;
import java.util.Collection;

import dk.sdu.imada.ticone.clustering.pair.IClusterPairs;
import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 12, 2018
 *
 */
public interface IClusters extends Collection<ICluster>, IObjectProvider {
	@Override
	ICluster[] toArray();

	@Override
	default Collection<ObjectType<?>> providedTypesOfObjects() {
		return Arrays.asList(ObjectType.CLUSTER, ObjectType.OBJECT);
	}

	@Override
	default <T extends IObjectWithFeatures> int getNumberObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type == ObjectType.CLUSTER)
			return size();
		else if (type == ObjectType.CLUSTER_PAIR)
			return size() * size();
		else if (type == ObjectType.OBJECT)
			return this.stream().mapToInt(c -> c.size()).sum();
		return IObjectProvider.super.getNumberObjectsOfType(type);
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type == ObjectType.CLUSTER)
			return (Collection<T>) this.asList();
		else if (type == ObjectType.CLUSTER_PAIR)
			return (Collection<T>) getClusterPairs().asList();
		else if (type == ObjectType.OBJECT)
			return (Collection<T>) getObjects();

		return IObjectProvider.super.getObjectsOfType(type);
	}

	default IClusters getClusters() {
		return this;
	}

	IClusterPairs getClusterPairs();

	IClusterList asList();

	IClusterSet asSet();

	IClusters copy();

	default ITimeSeriesObjectList getObjects() {
		ITimeSeriesObjectList result = null;
		for (ICluster c : this)
			if (result == null)
				result = c.getObjects();
			else
				result.addAll(c.getObjects());
		return result;
	}
}
