/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 8, 2019
 *
 */
public class DuplicateMappingForObjectException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -297235983174529363L;

	public DuplicateMappingForObjectException() {
		super();
	}

	public DuplicateMappingForObjectException(String message) {
		super(message);
	}

	public DuplicateMappingForObjectException(Throwable cause) {
		super(cause);
	}

}
