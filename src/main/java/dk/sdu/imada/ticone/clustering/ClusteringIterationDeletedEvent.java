/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since May 11, 2017
 *
 */
public class ClusteringIterationDeletedEvent {
	protected ITiconeClusteringResult clustering;
	protected int iteration;

	/**
	 * 
	 */
	public ClusteringIterationDeletedEvent(final ITiconeClusteringResult clustering, final int iteration) {
		super();
		this.clustering = clustering;
		this.iteration = iteration;
	}

	/**
	 * @return the clustering
	 */
	public ITiconeClusteringResult getClustering() {
		return this.clustering;
	}

	/**
	 * @return the iteration
	 */
	public int getIteration() {
		return this.iteration;
	}
}
