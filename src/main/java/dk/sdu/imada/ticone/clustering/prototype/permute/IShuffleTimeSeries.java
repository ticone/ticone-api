package dk.sdu.imada.ticone.clustering.prototype.permute;

import dk.sdu.imada.ticone.data.ITimeSeries;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.IShuffle;

/**
 * 
 * Permutes the prototype of the given cluster and returns it wrapped in a new
 * cluster object.
 * 
 * @author Christian Wiwie
 * 
 *         created Apr 5, 2017
 *
 */
public interface IShuffleTimeSeries extends IShuffle {

	@Override
	ObjectType<ITimeSeries> supportedObjectType();

	@Override
	IShuffleTimeSeries copy();

	@Override
	IShuffleTimeSeries newInstance();
}
