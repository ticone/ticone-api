/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * @author Christian Wiwie
 * 
 * @since Jan 16, 2019
 *
 */
public interface IClusterSelectionListener {
	void clusterSelectionChanged(ClusterSelectionChangeEvent event);
}
