/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.List;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder.IPrototype;
import dk.sdu.imada.ticone.similarity.ISimilarityValue;
import dk.sdu.imada.ticone.util.IBuilder;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 19, 2018
 *
 */
public interface IClusterBuilder
		extends IBuilder<ICluster, ClusterFactoryException, CreateClusterInstanceFactoryException> {

	@Override
	IClusterBuilder copy();

	IClusterBuilder setPrototype(IPrototype prototype);

	IClusterBuilder setPrototypeBuilder(IPrototypeBuilder prototypeFactory);

	IClusterBuilder setIsKeep(boolean isKeep);

	IClusterBuilder setClusterNumber(int clusterNumber);

	IClusterBuilder setInternalClusterNumber(Long internalClusterNumber);

	IClusterBuilder setObjects(ITimeSeriesObjectList objects, List<? extends ISimilarityValue> similarities)
			throws ClusterFactoryException;

	IClusterBuilder setClustering(IClusterObjectMapping clustering);
}
