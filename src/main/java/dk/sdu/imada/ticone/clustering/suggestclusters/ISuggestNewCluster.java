package dk.sdu.imada.ticone.clustering.suggestclusters;

import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;

/**
 * Created by christian on 8/24/15.
 */
public interface ISuggestNewCluster {

	IClusterSuggestion suggestNewClusters(IClusterObjectMapping patternObjectMapping)
			throws SuggestClusterException, InterruptedException;

	IClusterObjectMapping applyNewClusters(IClusterSuggestion suggestPatternsContainer) throws SuggestClusterException;

}
