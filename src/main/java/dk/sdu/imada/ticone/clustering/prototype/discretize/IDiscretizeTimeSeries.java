package dk.sdu.imada.ticone.clustering.prototype.discretize;

import java.io.Serializable;

import dk.sdu.imada.ticone.data.ITimeSeries;

/**
 * @author Christian Wiwie
 * @author Christian Nørskov
 * @since 6/11/15
 */
public interface IDiscretizeTimeSeries extends Serializable {

	public static final IDiscretizeTimeSeries NONE = new IDiscretizeTimeSeries() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5674920371290206884L;

		@Override
		public double[] getDiscretizationValues() {
			return null;
		}

		@Override
		public ITimeSeries discretizeObjectTimeSeries(ITimeSeries timeSeries) {
			return timeSeries;
		}

		@Override
		public IDiscretizeTimeSeries copy() {
			return this;
		}
	};

	IDiscretizeTimeSeries copy();

	ITimeSeries discretizeObjectTimeSeries(ITimeSeries timeSeries);

	double[] getDiscretizationValues();
}
