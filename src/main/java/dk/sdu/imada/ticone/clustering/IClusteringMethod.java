package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;

/**
 * @author Christian Wiwie
 * 
 *         created Apr 6, 2017
 *
 */
public interface IClusteringMethod<C extends IClusterObjectMapping> extends Serializable {

	C findClusters(ITimeSeriesObjects timeSeriesDatas, int numberOfClusters, long seed)
			throws ClusterOperationException, InterruptedException;

	IClusteringMethod<C> copy();

	ISimilarityFunction getSimilarityFunction();

	IPrototypeBuilder getPrototypeBuilder();
}
