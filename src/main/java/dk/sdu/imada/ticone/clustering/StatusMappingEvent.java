/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;

import dk.sdu.imada.ticone.util.IStatusMapping;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 27, 2017
 *
 */
public class StatusMappingEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5249437288794235728L;
	protected IStatusMapping csm;

	public StatusMappingEvent(final IStatusMapping csm) {
		super();
		this.csm = csm;
	}

	/**
	 * @return the csm
	 */
	public IStatusMapping getClusterStatusMapping() {
		return this.csm;
	}
}
