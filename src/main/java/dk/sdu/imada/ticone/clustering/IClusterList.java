/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.List;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 2, 2018
 *
 */
public interface IClusterList extends IClusters, List<ICluster> {
	@Override
	IClusterList copy();

	@Override
	default IClusterList asList() {
		return this;
	}
}
