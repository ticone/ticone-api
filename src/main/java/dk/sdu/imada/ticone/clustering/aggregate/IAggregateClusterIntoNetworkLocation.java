package dk.sdu.imada.ticone.clustering.aggregate;

import java.util.Collection;

import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.network.ITiconeNetwork;
import dk.sdu.imada.ticone.network.ITiconeNetworkNode;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 15, 2019
 */
public interface IAggregateClusterIntoNetworkLocation
		extends IAggregateCluster<Collection<? extends INetworkMappedTimeSeriesObject>> {

	@Override
	IAggregateClusterIntoNetworkLocation copy();

	void setNetwork(final ITiconeNetwork<? extends ITiconeNetworkNode, ?> network);
}
