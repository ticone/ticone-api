/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.clustering.IClusterObjectMapping;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.permute.IPermutable;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;
import dk.sdu.imada.ticone.util.ObjectSampleException;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 30, 2017
 *
 */
public interface IClusterObjectMappingPair extends IObjectProvider, IObjectWithFeatures, IPermutable {

	@Override
	default ObjectType<? extends IObjectWithFeatures> getObjectType() {
		return ObjectType.CLUSTERING_PAIR;
	}

	IClusterObjectMapping getClustering2();

	IClusterObjectMapping getClustering1();

	IClusterPairs getClusterPairs();

	@Override
	default Collection<ObjectType<?>> providedTypesOfObjects() {
		return Arrays.asList(ObjectType.CLUSTER_PAIR);
	}

	@Override
	default <T extends IObjectWithFeatures> int getNumberObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type == ObjectType.CLUSTER_PAIR)
			return getClustering1().getClusters().size() * getClustering2().getClusters().size();
		return IObjectProvider.super.getNumberObjectsOfType(type);
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type == ObjectType.CLUSTER_PAIR)
			return (Collection<T>) getClusterPairs();
		return IObjectProvider.super.getObjectsOfType(type);
	}
}
