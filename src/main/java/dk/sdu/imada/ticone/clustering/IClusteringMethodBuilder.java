package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.util.IBuilder;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Mar 28, 2019
 *
 * @param <C>
 */
public interface IClusteringMethodBuilder<CM extends IClusteringMethod<? extends IClusterObjectMapping>>
		extends IBuilder<CM, ClusteringMethodFactoryException, CreateClusteringMethodInstanceFactoryException> {

	@Override
	IClusteringMethodBuilder<CM> copy();

	CM copy(CM clusteringMethod);

	ISimilarityFunction getSimilarityFunction();

	IClusteringMethodBuilder<CM> setSimilarityFunction(ISimilarityFunction similarityFunction);

	IPrototypeBuilder getPrototypeBuilder();

	IClusteringMethodBuilder<CM> setPrototypeBuilder(IPrototypeBuilder prototypeBuilder);
}
