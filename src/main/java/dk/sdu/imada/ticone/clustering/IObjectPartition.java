/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import java.util.HashSet;
import java.util.Set;

import dk.sdu.imada.ticone.data.ITimeSeriesObject;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 21, 2018
 *
 */
interface IObjectPartition extends Set<HashSet<ITimeSeriesObject>> {

}