/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import java.util.Collection;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IPairSet;
import dk.sdu.imada.ticone.util.IncompatibleObjectProviderException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Jan 23, 2019
 *
 */
public interface IClusterPairSet extends IPairSet<ICluster, ICluster, IClusterPair>, IClusterPairs {
	@Override
	IClusterPairSet copy();

	@Override
	default IClusterPairSet asSet() {
		return this;
	}

	@Override
	default <T extends IObjectWithFeatures> Collection<T> getObjectsOfType(ObjectType<T> type)
			throws IncompatibleObjectProviderException {
		if (type.equals(ObjectType.CLUSTER_PAIR))
			return (Collection<T>) this;
		return IClusterPairs.super.getObjectsOfType(type);
	}
}
