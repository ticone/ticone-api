package dk.sdu.imada.ticone.clustering;

import java.io.Serializable;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;

public abstract interface IInitialClusteringProvider<C extends IClusterObjectMapping> extends Serializable {

	C getInitialClustering(ITimeSeriesObjects timeSeriesDatas) throws ClusterOperationException, InterruptedException;

	int getInitialNumberOfClusters();
}
