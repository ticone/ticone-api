package dk.sdu.imada.ticone.clustering.aggregate;

import dk.sdu.imada.ticone.data.ITimeSeriesObjects;
import dk.sdu.imada.ticone.util.AggregationException;
import dk.sdu.imada.ticone.util.IAggregate;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Oct 18, 2018
 *
 */
public interface IAggregateCluster<A> extends IAggregate<ITimeSeriesObjects, A> {

	@Override
	IAggregateCluster<A> copy();

	@Override
	default A aggregate(final ITimeSeriesObjects objects)
			throws EmptyClusterAggregationException, AggregationException, InterruptedException {
		if (objects.size() == 0)
			throw new EmptyClusterAggregationException("Cannot aggregate an empty cluster");
		try {
			return doAggregate(objects);
		} catch (final AggregationException e) {
			throw e;
		} catch (final InterruptedException e) {
			throw e;
		} catch (final Exception e) {
			throw new AggregationException(e);
		}
	}

	A doAggregate(ITimeSeriesObjects objects) throws AggregationException, InterruptedException;
}
