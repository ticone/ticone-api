/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.io.ILoadDataMethod;
import dk.sdu.imada.ticone.preprocessing.IPreprocessingSummary;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;
import dk.sdu.imada.ticone.prototype.IPrototypeBuilder;
import dk.sdu.imada.ticone.similarity.ISimilarityFunction;
import dk.sdu.imada.ticone.util.CreateInstanceFactoryException;
import dk.sdu.imada.ticone.util.FactoryException;
import dk.sdu.imada.ticone.util.IFactory;
import dk.sdu.imada.ticone.util.IIdMapMethod;
import dk.sdu.imada.ticone.util.ITimePointWeighting;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 * @param <R>
 */
public interface ITiconeClusteringResultFactory<C extends IClusterObjectMapping, R extends ITiconeClusteringResult>
		extends IFactory<R, FactoryException, CreateInstanceFactoryException> {

	public R createInstance(long seed, ILoadDataMethod loadDataMethod,
			IInitialClusteringProvider<C> initialClusteringProvider, int numberOfTimePoints,
			ITimePointWeighting timePointWeighting, IIdMapMethod idMapMethod,
			IPreprocessingSummary<C> preprocessingSummary, ISimilarityFunction similarityFunction,
			IClusteringMethodBuilder<? extends IClusteringMethod<C>> clusteringMethodBuilder,
			ITimeSeriesPreprocessor timeSeriesPreprocessor, IPrototypeBuilder prototypeBuilder)
			throws FactoryException, CreateInstanceFactoryException, InterruptedException;

	@Override
	ITiconeClusteringResultFactory<C, R> copy();
}