/**
 * 
 */
package dk.sdu.imada.ticone.clustering.pair;

import java.util.Arrays;
import java.util.Collection;

import dk.sdu.imada.ticone.clustering.ICluster;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.util.IObjectProvider;
import dk.sdu.imada.ticone.util.IPairs;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 7, 2018
 *
 */
public interface IClusterPairs extends IPairs<ICluster, ICluster, IClusterPair>, IObjectProvider {

	@Override
	default Collection<ObjectType<?>> providedTypesOfObjects() {
		return Arrays.asList(ObjectType.CLUSTER_PAIR);
	}

	default IClusterPairs getClusterPairs() {
		return this;
	}

	@Override
	IClusterPairs copy();

	@Override
	IClusterPairList asList();

	@Override
	IClusterPairSet asSet();
}
