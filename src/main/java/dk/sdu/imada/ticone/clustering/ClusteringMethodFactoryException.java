/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

import dk.sdu.imada.ticone.util.FactoryException;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Mar 28, 2019
 *
 */
public class ClusteringMethodFactoryException extends FactoryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8073631659765931794L;

	public ClusteringMethodFactoryException() {
		super();
	}

	public ClusteringMethodFactoryException(final String message) {
		super(message);
	}

	public ClusteringMethodFactoryException(final Throwable cause) {
		super(cause);
	}

}
