/**
 * 
 */
package dk.sdu.imada.ticone.clustering;

/**
 * 
 * @author Christian Wiwie
 * 
 * @since Feb 6, 2019
 *
 */
public class ClusteringIterationAddedEvent {
	protected final ITiconeClusteringResult clustering;
	protected final int iteration;

	/**
	 * 
	 */
	public ClusteringIterationAddedEvent(final ITiconeClusteringResult clustering, final int iteration) {
		super();
		this.clustering = clustering;
		this.iteration = iteration;
	}

	/**
	 * @return the clustering
	 */
	public ITiconeClusteringResult getClustering() {
		return this.clustering;
	}

	/**
	 * @return the iteration
	 */
	public int getIteration() {
		return this.iteration;
	}
}
