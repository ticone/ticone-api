/**
 * 
 */
package dk.sdu.imada.ticone.network;

public enum TiconeEdgeType {
	ANY, OUTGOING, INCOMING, SELF
}