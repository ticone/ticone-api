/**
 * 
 */
package dk.sdu.imada.ticone.network;

/**
 * @author Christian Wiwie
 * 
 * @since Oct 15, 2018
 *
 */
public class ObjectNotInNetworkException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 171407603707819018L;

	protected String objectId;

	protected ITiconeNetwork<?, ?> network;

	public ObjectNotInNetworkException(final String objectId, final ITiconeNetwork<?, ?> network) {
		super();
		this.objectId = objectId;
		this.network = network;
	}

	/**
	 * @return the objectId
	 */
	public String getObjectId() {
		return this.objectId;
	}

	/**
	 * @return the network
	 */
	public ITiconeNetwork<?, ?> getNetwork() {
		return this.network;
	}
}
