/**
 * 
 */
package dk.sdu.imada.ticone.network;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import de.wiwie.wiutils.utils.Triple;
import dk.sdu.imada.ticone.feature.IObjectWithFeatures;
import dk.sdu.imada.ticone.permute.IPermutable;

/**
 * @author Christian Wiwie
 * 
 * @since May 2, 2017
 *
 * @param <TICONE_NODE>
 * @param <TICONE_EDGE>
 */
public interface ITiconeNetwork<TICONE_NODE extends ITiconeNetworkNode, TICONE_EDGE extends ITiconeNetworkEdge<TICONE_NODE>>
		extends IObjectWithFeatures, IPermutable {

	@Override
	default ObjectType<ITiconeNetwork> getObjectType() {
		return ObjectType.NETWORK;
	}

	long getUID();

	void setName(String name);

	TICONE_NODE addNode(String name);

	TICONE_NODE[] addNodes(String[] names);

	TICONE_NODE getNode(String name);

	TICONE_NODE removeNode(String name);

	TICONE_NODE[] removeNodesByIds(String[] names);

	<N extends TICONE_NODE> boolean removeNode(TICONE_NODE node);

	<N extends TICONE_NODE> boolean removeNodes(TICONE_NODE[] nodes);

	TICONE_EDGE addEdge(String sourceNodeId, String targetNodeId, boolean isDirected);

	<N extends TICONE_NODE> TICONE_EDGE addEdge(N source, N target, boolean isDirected);

	int getEdgeCount();

	int getNodeCount();

	List<TICONE_EDGE> getEdgeList();

	Set<TICONE_EDGE> getEdgeSet();

	List<TICONE_NODE> getNodeList();

	Set<TICONE_NODE> getNodeSet();

	boolean containsNode(String nodeId);

	<T> T getValue(TICONE_NODE node, String col, Class<T> c);

	<T> T getValue(TICONE_EDGE e, String col, Class<T> c);

	boolean hasNetworkAttribute(String attribute);

	Set<Triple<String, Class, Boolean>> getNetworkAttributes();

	void createNetworkAttribute(String attribute, Class c, boolean isImmutable);

	void setNetworkAttribute(String attribute, Object value);

	boolean hasNodeAttribute(String attribute);

	Set<Triple<String, Class, Boolean>> getNodeAttributes();

	void createNodeAttribute(String attribute, Class c, boolean isImmutable);

	<N extends TICONE_NODE> void setNodeAttribute(N node, String attribute, Object value);

	void setEdgeAttribute(TICONE_EDGE edge, String attribute, Object value);

	boolean hasEdgeAttribute(String attribute);

	Set<Triple<String, Class, Boolean>> getEdgeAttributes();

	void createEdgeAttribute(String attribute, Class c, boolean isImmutable);

	<N extends TICONE_NODE> List<TICONE_EDGE> getAdjacentEdgeList(N node, TiconeEdgeType types);

	<N extends TICONE_NODE> Collection<TICONE_EDGE> getEdges(N n1, N n2, TiconeEdgeType types);

	<N extends TICONE_NODE> Collection<TICONE_EDGE> getEdges(Set<N> nodes1, Set<N> nodes2, TiconeEdgeType types);

	<N extends TICONE_NODE> Collection<TICONE_EDGE> getEdgesByNodeIds(Set<String> nodeIds1, Set<String> nodeIds2,
			TiconeEdgeType types);

	void performEdgeCrossovers(double factorEdgeSwaps, boolean isDirected, final long seed) throws InterruptedException;

	int getAdjacentEdgeCount(TICONE_NODE node, TiconeEdgeType types);

	ITiconeNetwork<TICONE_NODE, TICONE_EDGE> copy();

	Set<String> getNodeIds();

}