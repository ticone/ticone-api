/**
 * 
 */
package dk.sdu.imada.ticone.network;

import java.util.Collection;

import dk.sdu.imada.ticone.data.INetworkMappedTimeSeriesObject;
import dk.sdu.imada.ticone.network.INetworkLocationPrototypeComponentBuilder.INetworkLocationPrototypeComponent;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder;
import dk.sdu.imada.ticone.prototype.IPrototypeComponentBuilder.IPrototypeComponent;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 1, 2018
 *
 */
public interface INetworkLocationPrototypeComponentBuilder extends
		IPrototypeComponentBuilder<INetworkLocationPrototypeComponent, Collection<? extends INetworkMappedTimeSeriesObject>> {

	/**
	 * @author Christian Wiwie
	 * 
	 * @since Oct 31, 2018
	 *
	 */
	public interface INetworkLocationPrototypeComponent
			extends IPrototypeComponent<Collection<? extends INetworkMappedTimeSeriesObject>> {
		Collection<? extends INetworkMappedTimeSeriesObject> getNetworkLocation();
	}

	INetworkLocationPrototypeComponentBuilder setNetworkLocation(
			Collection<? extends INetworkMappedTimeSeriesObject> location);

	Collection<? extends INetworkMappedTimeSeriesObject> getNetworkLocation();

	@Override
	INetworkLocationPrototypeComponentBuilder copy();

}
