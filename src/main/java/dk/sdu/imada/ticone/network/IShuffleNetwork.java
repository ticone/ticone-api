/**
 * 
 */
package dk.sdu.imada.ticone.network;

import dk.sdu.imada.ticone.feature.IObjectWithFeatures.ObjectType;
import dk.sdu.imada.ticone.permute.IShuffle;

/**
 * @author Christian Wiwie
 * 
 * @since May 2, 2017
 *
 */
public interface IShuffleNetwork extends IShuffle {

	@Override
	ObjectType<ITiconeNetwork> supportedObjectType();

	@Override
	IShuffleNetwork copy();

	@Override
	IShuffleNetwork newInstance();
}
