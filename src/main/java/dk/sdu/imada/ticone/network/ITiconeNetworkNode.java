/**
 * 
 */
package dk.sdu.imada.ticone.network;

import it.unimi.dsi.fastutil.objects.ObjectList;

/**
 * @author Christian Wiwie
 * 
 * @since May 2, 2017
 *
 */
public interface ITiconeNetworkNode {

	String getName();

	boolean hasAlternativeName();

	String getAlternativeName();

	void setAlternativeName(String alternativeName);

	long getUID();

	IConnectedComponent<? extends ITiconeNetworkNode> getConnectedComponent() throws InterruptedException;

	void setConnectedComponent(IConnectedComponent<? extends ITiconeNetworkNode> component);

	void resetConnectedComponent();

	int getComponentWideId();

	void setComponentWideId(int componentWideId);

	ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge<?>> getNetwork();

	ObjectList<? extends ITiconeNetworkNode> getNeighbors(TiconeEdgeType types);
}