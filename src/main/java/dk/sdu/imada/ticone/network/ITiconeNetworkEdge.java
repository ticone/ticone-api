/**
 * 
 */
package dk.sdu.imada.ticone.network;

/**
 * @author Christian Wiwie
 * 
 * @since May 2, 2017
 *
 * @param <TICONE_NODE>
 */
public interface ITiconeNetworkEdge<TICONE_NODE extends ITiconeNetworkNode> {

	long getUID();

	TICONE_NODE getSource();

	TICONE_NODE getTarget();

	boolean isDirected();

}