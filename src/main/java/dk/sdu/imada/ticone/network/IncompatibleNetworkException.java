/**
 * 
 */
package dk.sdu.imada.ticone.network;

/**
 * @author Christian Wiwie
 * 
 * @since Mar 30, 2019
 *
 */
public class IncompatibleNetworkException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4278914565742835503L;

	public IncompatibleNetworkException() {
		super();
	}

	public IncompatibleNetworkException(String message) {
		super(message);
	}

	public IncompatibleNetworkException(Throwable cause) {
		super(cause);
	}

}
