/**
 * 
 */
package dk.sdu.imada.ticone.network;

import java.util.List;

/**
 * @author Christian Wiwie
 * 
 * @since Nov 21, 2018
 *
 */
public interface IConnectedComponent<TICONE_NODE extends ITiconeNetworkNode> {

	void addNode(final TICONE_NODE node);

	ITiconeNetwork<TICONE_NODE, ?> getNetwork();

	int getNetworkWideId();

	/**
	 * @return the nodes
	 */
	List<TICONE_NODE> getNodes();

	/**
	 * @return the networkWideId
	 */
	long getUID();

}
