/**
 * 
 */
package dk.sdu.imada.ticone.network;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public interface ITiconeNetworkImplFactory {

	/**
	 * @param reuseInstanceIfExists the reuseInstanceIfExists to set
	 */
	void setReuseInstanceIfExists(boolean reuseInstanceIfExists);

	/**
	 * @return the reuseInstanceIfExists
	 */
	boolean isReuseInstanceIfExists();

	/**
	 * @return the isMultiGraph
	 */
	boolean isMultiGraph();

	/**
	 * @return the cloneAttributes
	 */
	boolean isCloneAttributes();

	/**
	 * @param cloneAttributes the cloneAttributes to set
	 */
	void setCloneAttributes(boolean cloneAttributes);

	ITiconeNetwork<?, ?> getInstance();

	ITiconeNetwork<?, ?> getInstance(boolean registerNetwork);

	ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge<?>> getInstance(
			ITiconeNetwork<? extends ITiconeNetworkNode, ? extends ITiconeNetworkEdge<?>> network);

}