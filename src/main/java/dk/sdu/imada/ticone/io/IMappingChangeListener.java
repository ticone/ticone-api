/**
 * 
 */
package dk.sdu.imada.ticone.io;

/**
 * @author Christian Wiwie
 * 
 * @since Apr 13, 2019
 *
 */
public interface IMappingChangeListener {
	void mappingChanged(MappingChangedEvent event);
}
