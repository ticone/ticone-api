/**
 * 
 */
package dk.sdu.imada.ticone.io;

import dk.sdu.imada.ticone.data.ITimeSeriesObjectList;
import dk.sdu.imada.ticone.preprocessing.ITimeSeriesPreprocessor;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public interface ILoadDataMethod {

	ILoadDataMethod copy();

	/**
	 * Parses or retrieves a {@link ITimeSeriesObjectList} and eventually passes it
	 * to {@link ITimeSeriesPreprocessor#initializeObjects(ITimeSeriesObjectList)}.
	 * 
	 * @param processBuilder
	 * @throws LoadDataException
	 */
	void loadData(ITimeSeriesPreprocessor preprocessor) throws LoadDataException;

	boolean isDataLoaded();

	/**
	 * Used to represent this {@link ILoadDataMethod} by its source in a
	 * human-readable format.
	 * 
	 * @return
	 */
	String sourceAsString();

}