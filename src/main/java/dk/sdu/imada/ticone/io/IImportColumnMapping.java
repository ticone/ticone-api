/**
 * 
 */
package dk.sdu.imada.ticone.io;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public interface IImportColumnMapping {

	IImportColumnMapping copy();

	void setObjectIdColumn(int objectIdColumnIndex, String objectIdColumnName);

	void setReplicateColumn(int replicateColumnIndex, String replicateColumnName);

	void setTimePointsColumns(int[] timePointsColumnIndices, String[] timePointsColumnNames);

	int getObjectIdColumnIndex();

	String getObjectIdColumnName();

	int getReplicateColumnIndex();

	String getReplicateColumnName();

	int[] getTimePointsColumnIndices();

	String[] getTimePointsColumnNames();

	boolean addMappingChangeListener(IMappingChangeListener listener);

	boolean removeMappingChangeListener(IMappingChangeListener listener);
}