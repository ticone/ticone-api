package dk.sdu.imada.ticone.io;

public class LoadDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3989015452494216405L;

	public LoadDataException(final Throwable cause) {
		super(cause);
	}

	public LoadDataException(final String text) {
		super(text);
	}

	@Override
	public String getMessage() {
		if (this.getCause() != null)
			return this.getCause().getMessage();
		return super.getMessage();
	}
}
