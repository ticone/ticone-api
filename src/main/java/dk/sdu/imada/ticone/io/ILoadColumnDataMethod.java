/**
 * 
 */
package dk.sdu.imada.ticone.io;

/**
 * @author Christian Wiwie
 * 
 * @since Feb 8, 2019
 *
 */
public interface ILoadColumnDataMethod extends ILoadDataMethod {

	@Override
	ILoadColumnDataMethod copy();

	IImportColumnMapping getColumnMapping();

}